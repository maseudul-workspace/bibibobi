package n.webinfotech.bibibobi.domain.interactors.impl;

import java.util.ArrayList;

import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.GetProductListWithoutFilterInteractor;
import n.webinfotech.bibibobi.domain.interactors.base.AbstractInteractor;
import n.webinfotech.bibibobi.domain.models.Product;
import n.webinfotech.bibibobi.domain.models.ProductListWithoutFilterWrapper;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class GetProductListWithoutFilterInteractorImpl extends AbstractInteractor implements GetProductListWithoutFilterInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int secondCategory;
    ArrayList<Integer> designerIds;
    ArrayList<Integer> sizeIds;
    ArrayList<Integer> colorIds;
    int priceFrom;
    int priceTo;
    int sortBy;
    int pageNo;

    public GetProductListWithoutFilterInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int secondCategory, ArrayList<Integer> designerIds, ArrayList<Integer> sizeIds, ArrayList<Integer> colorIds, int priceFrom, int priceTo, int sortBy, int pageNo) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.secondCategory = secondCategory;
        this.designerIds = designerIds;
        this.sizeIds = sizeIds;
        this.colorIds = colorIds;
        this.priceFrom = priceFrom;
        this.priceTo = priceTo;
        this.sortBy = sortBy;
        this.pageNo = pageNo;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductListWithoutFilterFail(errorMsg);
            }
        });
    }

    private void postMessage(Product[] products, int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductListWithoutFilterSuccess(products, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final ProductListWithoutFilterWrapper productListWithoutFilterWrapper = mRepository.getProductListWithoutFilter(secondCategory, designerIds, sizeIds, colorIds, priceFrom, priceTo, sortBy, pageNo);
        if (productListWithoutFilterWrapper == null) {
            notifyError("Something went wrong");
        } else if (!productListWithoutFilterWrapper.status) {
            notifyError(productListWithoutFilterWrapper.message);
        } else {
            postMessage(productListWithoutFilterWrapper.products, productListWithoutFilterWrapper.totalPage);
        }
    }
}
