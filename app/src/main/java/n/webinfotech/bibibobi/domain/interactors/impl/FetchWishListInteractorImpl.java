package n.webinfotech.bibibobi.domain.interactors.impl;

import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.FetchWishListInteractor;
import n.webinfotech.bibibobi.domain.interactors.base.AbstractInteractor;
import n.webinfotech.bibibobi.domain.models.WishListWrapper;
import n.webinfotech.bibibobi.domain.models.WishlistProduct;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class FetchWishListInteractorImpl extends AbstractInteractor implements FetchWishListInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userID;

    public FetchWishListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userID) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userID = userID;
    }

    private void notifyError(String erroMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onWishListFetchFail(erroMsg, loginError);
            }
        });
    }

    private void postMessage(WishlistProduct[] wishlistProducts){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onWishlistFetchSuccess(wishlistProducts);
            }
        });
    }

    @Override
    public void run() {
        WishListWrapper wishListWrapper = mRepository.fetchWishList(apiToken, userID);
        if (wishListWrapper == null) {
            notifyError("Something went wrong", wishListWrapper.login_error);
        } else if (!wishListWrapper.status) {
            notifyError(wishListWrapper.message, wishListWrapper.login_error);
        } else {
            postMessage(wishListWrapper.products);
        }
    }
}
