package n.webinfotech.bibibobi.domain.interactors;

public interface SetTransactionIdInteractor {
    interface Callback {
        void onSetTransactionIdSuccess();
        void onSetTransactionIdFail(String errorMsg, int loginError);
    }
}
