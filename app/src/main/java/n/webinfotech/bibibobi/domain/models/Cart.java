package n.webinfotech.bibibobi.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Cart {
    @SerializedName("id")
    @Expose
    public int cartId;

    @SerializedName("product_id")
    @Expose
    public int productId;

    @SerializedName("color_id")
    @Expose
    public int colorId;

    @SerializedName("quantity")
    @Expose
    public int quantity;

    @SerializedName("p_name")
    @Expose
    public String productName;

    @SerializedName("price")
    @Expose
    public double productPrice;

    @SerializedName("mrp")
    @Expose
    public double productMRP;

    @SerializedName("tag_name")
    @Expose
    public String productTagName;

    @SerializedName("main_image")
    @Expose
    public String productImage;

    @SerializedName("color_name")
    @Expose
    public String colorName;

    @SerializedName("color_value")
    @Expose
    public String colorValue;

    @SerializedName("size_name")
    @Expose
    public String sizeName;

    @SerializedName("designer")
    @Expose
    public String designer;

    @SerializedName("stock")
    @Expose
    public int stock;

}
