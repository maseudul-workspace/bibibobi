package n.webinfotech.bibibobi.domain.interactors;

import n.webinfotech.bibibobi.domain.models.MainData;

public interface GetMainDataInteractor {
    interface Callback {
        void onGettingMainDataSuccess(MainData mainData);
        void onGettingMainDataFail();
    }
}
