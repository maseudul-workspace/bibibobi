package n.webinfotech.bibibobi.domain.models;

public class MainCategory {

    public int categoryId;
    public String categoryName;
    public String categoryImage;
    public boolean isTraditional;

    public MainCategory(int categoryId, String categoryName, String categoryImage, boolean isTraditional) {
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.categoryImage = categoryImage;
        this.isTraditional = isTraditional;
    }
}
