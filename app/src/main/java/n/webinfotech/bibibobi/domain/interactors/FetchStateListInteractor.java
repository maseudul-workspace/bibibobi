package n.webinfotech.bibibobi.domain.interactors;

import n.webinfotech.bibibobi.domain.models.StateList;

public interface FetchStateListInteractor {
    interface Callback {
        void onGettingStateListSuccess(StateList[] states);
        void onGettingStateListFail();
    }
}
