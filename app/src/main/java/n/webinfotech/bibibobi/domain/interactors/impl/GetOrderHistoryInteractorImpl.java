package n.webinfotech.bibibobi.domain.interactors.impl;

import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.GetOrderHistoryInteractor;
import n.webinfotech.bibibobi.domain.interactors.base.AbstractInteractor;
import n.webinfotech.bibibobi.domain.models.Order;
import n.webinfotech.bibibobi.domain.models.OrderListWrapper;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class GetOrderHistoryInteractorImpl extends AbstractInteractor implements GetOrderHistoryInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;

    public GetOrderHistoryInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingOrderHistoryFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(final Order[] orders){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingOrderHistorySuccess(orders);
            }
        });
    }

    @Override
    public void run() {
        final OrderListWrapper orderListWrapper = mRepository.getOrderHistory(apiToken, userId);
        if (orderListWrapper == null) {
            notifyError(orderListWrapper.message, 0);
        } else if (!orderListWrapper.status) {
            notifyError(orderListWrapper.message, orderListWrapper.login_error);
        } else {
            postMessage(orderListWrapper.orders);
        }
    }
}
