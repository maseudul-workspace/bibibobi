package n.webinfotech.bibibobi.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PinData {

    @SerializedName("pre_paid")
    @Expose
    public String prePaid;

    @SerializedName("cod")
    @Expose
    public String cod;

    @SerializedName("pickup")
    @Expose
    public String pickup;

}
