package n.webinfotech.bibibobi.domain.interactors;

public interface DeleteItemFromCartInteractor {
    interface Callback {
        void onCartItemDeleteSuccess();
        void onCartItemDeleteFail(String errorMsg, int loginError);
    }
}
