package n.webinfotech.bibibobi.domain.interactors;
import n.webinfotech.bibibobi.domain.models.User;

public interface CheckLoginInteractor {
    interface Callback {
        void onCheckLoginSuccess(User user);
        void onCheckLoginFail(String errorMsg);
    }
}
