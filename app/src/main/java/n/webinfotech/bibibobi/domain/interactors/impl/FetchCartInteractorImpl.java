package n.webinfotech.bibibobi.domain.interactors.impl;

import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.FetchCartInteractor;
import n.webinfotech.bibibobi.domain.interactors.base.AbstractInteractor;
import n.webinfotech.bibibobi.domain.models.Cart;
import n.webinfotech.bibibobi.domain.models.CartWrapper;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class FetchCartInteractorImpl extends AbstractInteractor implements FetchCartInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiKey;
    int userID;

    public FetchCartInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiKey, int userID) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiKey = apiKey;
        this.userID = userID;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchCartFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(Cart[] carts){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchCartSuccess(carts);
            }
        });
    }

    @Override
    public void run() {
        final CartWrapper cartWrapper = mRepository.fetchCartList(apiKey, userID);
        if (cartWrapper == null) {
            notifyError("Something went wrong",  cartWrapper.loginError);
        } else if (!cartWrapper.status) {
            notifyError(cartWrapper.message, cartWrapper.loginError);
        } else {
            postMessage(cartWrapper.carts);
        }
    }
}
