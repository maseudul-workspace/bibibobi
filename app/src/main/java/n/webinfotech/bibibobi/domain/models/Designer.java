package n.webinfotech.bibibobi.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Designer {

    @SerializedName("id")
    @Expose
    public int designerId;

    @SerializedName("name")
    @Expose
    public String designerName;

}
