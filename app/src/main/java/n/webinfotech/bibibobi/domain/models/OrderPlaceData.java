package n.webinfotech.bibibobi.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderPlaceData {

    @SerializedName("order_id")
    @Expose
    public int orderId;

    @SerializedName("amount")
    @Expose
    public Double amount;

}