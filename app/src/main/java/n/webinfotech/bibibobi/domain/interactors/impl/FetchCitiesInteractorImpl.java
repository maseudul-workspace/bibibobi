package n.webinfotech.bibibobi.domain.interactors.impl;


import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.FetchCitiesInteractor;
import n.webinfotech.bibibobi.domain.interactors.base.AbstractInteractor;
import n.webinfotech.bibibobi.domain.models.Cities;
import n.webinfotech.bibibobi.domain.models.CitiesWrapper;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class FetchCitiesInteractorImpl extends AbstractInteractor implements FetchCitiesInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int stateId;

    public FetchCitiesInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int stateId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.stateId = stateId;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCitiesFail();
            }
        });
    }

    private void postMessage(Cities[] cities){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCitiesSuccess(cities);
            }
        });
    }

    @Override
    public void run() {
        final CitiesWrapper citiesWrapper = mRepository.fetchCities(stateId);
        if (citiesWrapper == null) {
            notifyError();
        } else if (!citiesWrapper.status) {
            notifyError();
        } else {
            postMessage(citiesWrapper.cities);
        }
    }
}
