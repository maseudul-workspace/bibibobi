package n.webinfotech.bibibobi.domain.interactors.impl;

import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.AddAddressInteractor;
import n.webinfotech.bibibobi.domain.interactors.base.AbstractInteractor;
import n.webinfotech.bibibobi.domain.models.CommonResponse;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class AddAddressInteractorImpl extends AbstractInteractor implements AddAddressInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String authorization;
    int userId;
    String email;
    String mobile;
    int state;
    int city;
    String pin;
    String address;

    public AddAddressInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String authorization, int userId, String email, String mobile, int state, int city, String pin, String address) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.userId = userId;
        this.email = email;
        this.mobile = mobile;
        this.state = state;
        this.city = city;
        this.pin = pin;
        this.address = address;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddAddressFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddAddressSuccess();
            }
        });
    }

    @Override
    public void run() {
        CommonResponse commonResponse = mRepository.addShippingAddress(authorization, userId, email, mobile, state, city, pin, address);
        if (commonResponse == null) {
            notifyError("Something went wrong", 0);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.loginError);
        } else {
            postMessage();
        }
    }
}
