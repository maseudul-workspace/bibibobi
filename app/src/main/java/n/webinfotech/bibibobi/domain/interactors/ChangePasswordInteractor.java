package n.webinfotech.bibibobi.domain.interactors;

public interface ChangePasswordInteractor {
    interface Callback {
        void onChangePasswordSuccess();
        void onChangePasswordFail(String errorMsg, int loginError);
    }
}
