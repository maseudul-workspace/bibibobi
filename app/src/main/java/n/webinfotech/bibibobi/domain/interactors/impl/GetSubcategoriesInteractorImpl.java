package n.webinfotech.bibibobi.domain.interactors.impl;

import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.GetSubcategoriesInteractor;
import n.webinfotech.bibibobi.domain.interactors.base.AbstractInteractor;
import n.webinfotech.bibibobi.domain.models.CategoryWrapper;
import n.webinfotech.bibibobi.domain.models.FirstCategory;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class GetSubcategoriesInteractorImpl extends AbstractInteractor implements GetSubcategoriesInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int mainCatId;

    public GetSubcategoriesInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int mainCatId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.mainCatId = mainCatId;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingSubcategoriesFail(errorMsg);
            }
        });
    }

    private void postMessage(FirstCategory[] firstCategories){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingSubcategoriesSuccess(firstCategories);
            }
        });
    }

    @Override
    public void run() {
        final CategoryWrapper categoryWrapper = mRepository.getMainSubcategories(mainCatId);
        if (categoryWrapper == null) {
            notifyError("Something went wrong");
        } else if (!categoryWrapper.status) {
            notifyError(categoryWrapper.message);
        } else {
            postMessage(categoryWrapper.firstCategories);
        }
    }
}
