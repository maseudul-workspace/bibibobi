package n.webinfotech.bibibobi.domain.interactors;

public interface UpdateTransactionIdInteractor {
    interface Callback {
        void onUpdateTransactionIdSuccess();
        void onUpdateTransactionIdFail(String errorMsg, int loginError);
    }
}
