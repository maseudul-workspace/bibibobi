package n.webinfotech.bibibobi.domain.interactors;

public interface UpdateCartInteractor {
    interface Callback {
        void onUpdateCartSuccess();
        void onUpdateCartFail(String errorMsg, int loginError);
    }
}
