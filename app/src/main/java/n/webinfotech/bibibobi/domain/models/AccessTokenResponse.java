package n.webinfotech.bibibobi.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AccessTokenResponse {

    @SerializedName("access_token")
    @Expose
    public String accessToken;

    @SerializedName("error")
    @Expose
    public String error = "";

}
