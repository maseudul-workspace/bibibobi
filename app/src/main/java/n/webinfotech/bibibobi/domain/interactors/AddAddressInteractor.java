package n.webinfotech.bibibobi.domain.interactors;

public interface AddAddressInteractor {
    interface Callback {
        void onAddAddressSuccess();
        void onAddAddressFail(String errorMsg, int loginError);
    }
}
