package n.webinfotech.bibibobi.domain.interactors;


import n.webinfotech.bibibobi.domain.models.PaymentRequestResponse;

public interface RequestPaymentInteractor {
    interface Callback {
        void onRequestPaymentSuccess(PaymentRequestResponse paymentRequestResponse);
        void onRequestPaymentFail(String errorMsg);
    }
}
