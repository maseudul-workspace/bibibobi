package n.webinfotech.bibibobi.domain.interactors;

import n.webinfotech.bibibobi.domain.models.ProductListData;

public interface GetProductListWithFilterInteractor {
    interface Callback {
        void onGettingProductListWithFiltersSuccess(ProductListData productListData, int totalPage);
        void onGettingProductListWithFiltersFail(String errorMsg);
    }
}
