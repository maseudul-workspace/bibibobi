package n.webinfotech.bibibobi.domain.interactors.impl;

import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.CheckLoginInteractor;
import n.webinfotech.bibibobi.domain.interactors.base.AbstractInteractor;
import n.webinfotech.bibibobi.domain.models.User;
import n.webinfotech.bibibobi.domain.models.UserWrapper;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class CheckLoginInteractorImpl extends AbstractInteractor implements CheckLoginInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String email;
    String password;

    public CheckLoginInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String email, String password) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.email = email;
        this.password = password;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCheckLoginFail(errorMsg);
            }
        });
    }

    private void postMessage(User user){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCheckLoginSuccess(user);
            }
        });
    }

    @Override
    public void run() {
        final UserWrapper userWrapper = mRepository.checkLogin(email, password);
        if (userWrapper == null) {
            notifyError("Something went wrong");
        } else if (!userWrapper.status) {
            notifyError(userWrapper.message);
        } else {
            postMessage(userWrapper.user);
        }
    }
}
