package n.webinfotech.bibibobi.domain.interactors.impl;


import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.GetAddressDetailsInteractor;
import n.webinfotech.bibibobi.domain.interactors.base.AbstractInteractor;
import n.webinfotech.bibibobi.domain.models.Address;
import n.webinfotech.bibibobi.domain.models.AddressDetailsWrapper;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class GetAddressDetailsInteractorImpl extends AbstractInteractor implements GetAddressDetailsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;
    int addressId;

    public GetAddressDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId, int addressId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
        this.addressId = addressId;
    }

    private void notifyError(String errorMsg, int isLoginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingAddressDetailsFail(errorMsg, isLoginError);
            }
        });
    }

    private void postMessage(Address addresses){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingAddressDetailsSuccess(addresses);
            }
        });
    }

    @Override
    public void run() {
        AddressDetailsWrapper addressDetailsWrapper = mRepository.getShippingAddressDetails(apiToken, userId, addressId);
        if (addressDetailsWrapper == null) {
            notifyError("Something went wrong", 0);
        } else if (!addressDetailsWrapper.status) {
            notifyError(addressDetailsWrapper.message, addressDetailsWrapper.login_error);
        } else {
            postMessage(addressDetailsWrapper.addresses);
        }
    }
}
