package n.webinfotech.bibibobi.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserDetails {

    @SerializedName("user")
    @Expose
    public User user;

    @SerializedName("user_details")
    @Expose
    public UserProfile userProfile;

}
