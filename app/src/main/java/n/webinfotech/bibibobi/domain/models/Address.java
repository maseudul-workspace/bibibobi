package n.webinfotech.bibibobi.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Address {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("mobile")
    @Expose
    public String mobile;

    @SerializedName("address")
    @Expose
    public String address;

    @SerializedName("pin")
    @Expose
    public String pin;

    @SerializedName("s_name")
    @Expose
    public String stateName;

    @SerializedName("c_name")
    @Expose
    public String cityName;

    @SerializedName("alternative_mobile")
    @Expose
    public String alternativeMobile;

    @SerializedName("landmark")
    @Expose
    public String landmark;

    @SerializedName("state_id")
    @Expose
    public int stateId;

    @SerializedName("city_id")
    @Expose
    public int cityId;

    public boolean isSelected = false;


}
