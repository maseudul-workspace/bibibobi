package n.webinfotech.bibibobi.domain.interactors.impl;

import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.FetchUserInfoInteractor;
import n.webinfotech.bibibobi.domain.interactors.base.AbstractInteractor;
import n.webinfotech.bibibobi.domain.models.UserDetails;
import n.webinfotech.bibibobi.domain.models.UserDetailsWrapper;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class FetchUserInfoInteractorImpl extends AbstractInteractor implements FetchUserInfoInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiToken;

    public FetchUserInfoInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int userId, String apiToken) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiToken = apiToken;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingUserInfoFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(UserDetails user){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingUserInfoSuccess(user);
            }
        });
    }

    @Override
    public void run() {
        final UserDetailsWrapper userWrapper = mRepository.fetchUserInfo(userId, apiToken);
        if (userWrapper == null) {
            notifyError("Something went wrong", 0);
        } else if (!userWrapper.status) {
            notifyError(userWrapper.message, userWrapper.login_error);
        } else {
            postMessage(userWrapper.userDetails);
        }
    }
}
