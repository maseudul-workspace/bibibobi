package n.webinfotech.bibibobi.domain.interactors;

import n.webinfotech.bibibobi.domain.models.Cities;

public interface FetchCitiesInteractor {
    interface Callback {
        void onGettingCitiesSuccess(Cities[] cities);
        void onGettingCitiesFail();
    }
}
