package n.webinfotech.bibibobi.domain.interactors.impl;

import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.PlaceOrderInteractor;
import n.webinfotech.bibibobi.domain.interactors.base.AbstractInteractor;
import n.webinfotech.bibibobi.domain.models.OrderPlaceData;
import n.webinfotech.bibibobi.domain.models.OrderPlaceDataResponse;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class PlaceOrderInteractorImpl extends AbstractInteractor implements PlaceOrderInteractor {

    AppRepositoryImpl mRespository;
    Callback mCallback;
    String apitoken;
    int userId;
    int paymentMethod;
    int addressId;

    public PlaceOrderInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRespository, Callback mCallback, String apitoken, int userId, int paymentMethod, int addressId) {
        super(threadExecutor, mainThread);
        this.mRespository = mRespository;
        this.mCallback = mCallback;
        this.apitoken = apitoken;
        this.userId = userId;
        this.paymentMethod = paymentMethod;
        this.addressId = addressId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onPlaceOrderFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(OrderPlaceData orderPlaceData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onPlaceOrderSuccess(orderPlaceData);
            }
        });
    }

    @Override
    public void run() {
        final OrderPlaceDataResponse orderPlaceDataResponse = mRespository.placeOrder(apitoken, userId, paymentMethod, addressId);
        if (orderPlaceDataResponse == null) {
            notifyError("Something went wrong", 0);
        } else if (!orderPlaceDataResponse.status) {
            notifyError(orderPlaceDataResponse.message, orderPlaceDataResponse.login_error);
        } else {
            postMessage(orderPlaceDataResponse.orderPlaceData);
        }
    }
}
