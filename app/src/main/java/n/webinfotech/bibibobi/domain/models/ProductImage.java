package n.webinfotech.bibibobi.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductImage {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("image")
    @Expose
    public String image;

}
