package n.webinfotech.bibibobi.domain.interactors;

public interface UpdateUserProfileInteractor {
    interface Callback {
        void onUserProfileUpdateSuccess();
        void onUserProfileUpdateFail(String errorMsg, int loginError);
    }
}
