package n.webinfotech.bibibobi.domain.interactors.impl;

import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.CheckPinAvailabilityInteractor;
import n.webinfotech.bibibobi.domain.interactors.base.AbstractInteractor;
import n.webinfotech.bibibobi.domain.models.PinData;
import n.webinfotech.bibibobi.domain.models.PinDataWrapper;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class CheckPinAvailabilityInteractorImpl extends AbstractInteractor implements CheckPinAvailabilityInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String pin;

    public CheckPinAvailabilityInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String pin) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.pin = pin;
    }

    private void notifyError(final String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onPinAvailabilityCheckFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(PinData pinData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onPinAvailabilityCheckSuccess(pinData);
            }
        });
    }

    @Override
    public void run() {
        final PinDataWrapper pinDataWrapper = mRepository.checkPinAvailability(pin);
        if (pinDataWrapper == null) {
            notifyError("Something went wrong", 0);
        } else if (!pinDataWrapper.status) {
            notifyError(pinDataWrapper.message, pinDataWrapper.login_error);
        } else {
            postMessage(pinDataWrapper.pinData);
        }
    }
}
