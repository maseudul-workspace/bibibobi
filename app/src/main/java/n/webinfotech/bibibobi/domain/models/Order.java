package n.webinfotech.bibibobi.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Order {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("order_id")
    @Expose
    public int orderId;

    @SerializedName("quantity")
    @Expose
    public int quantity;

    @SerializedName("rate")
    @Expose
    public double price;

    @SerializedName("total")
    @Expose
    public double total;

    @SerializedName("created_at")
    @Expose
    public String orderDate;

    @SerializedName("p_name")
    @Expose
    public String productName;

    @SerializedName("p_image")
    @Expose
    public String image;

    @SerializedName("payment_method")
    @Expose
    public int paymentMethod;

    @SerializedName("payment_status")
    @Expose
    public int paymentStatus;

    @SerializedName("brand_name")
    @Expose
    public String brandName;

    @SerializedName("designer")
    @Expose
    public String designer;

    @SerializedName("c_value")
    @Expose
    public String color;

    @SerializedName("size")
    @Expose
    public String size;

    @SerializedName("order_status")
    @Expose
    public int status;

}
