package n.webinfotech.bibibobi.domain.models;

/**
 * Created by Raj on 23-09-2019.
 */

public class Products {
    public String productTitle;
    public String productImage;
    public String price;
    public String sellingPrice;

    public Products(String productTitle, String productImage, String price,  String sellingPrice) {
        this.productTitle = productTitle;
        this.productImage = productImage;
        this.price = price;
        this.sellingPrice = sellingPrice;
    }
}
