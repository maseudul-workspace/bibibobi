package n.webinfotech.bibibobi.domain.interactors;

import n.webinfotech.bibibobi.domain.models.Address;

public interface FetchShippingAddressListInteractor {
    interface Callback {
        void onGettingShippingAddressSuccess(Address[] addresses);
        void onGettingShippingAddressFail(String errorMsg, int isLoginError);
    }
}
