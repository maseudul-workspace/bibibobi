package n.webinfotech.bibibobi.domain.interactors;

public interface AddToCartWithoutFilterInteractor {
    interface Callback {
        void onAddToCartSuccess();
        void onAddToCartFail(String errorMsg, int loginError);
    }
}
