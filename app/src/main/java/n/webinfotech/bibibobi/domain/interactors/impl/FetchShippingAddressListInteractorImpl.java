package n.webinfotech.bibibobi.domain.interactors.impl;


import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.FetchShippingAddressListInteractor;
import n.webinfotech.bibibobi.domain.interactors.base.AbstractInteractor;
import n.webinfotech.bibibobi.domain.models.Address;
import n.webinfotech.bibibobi.domain.models.AddressListWrapper;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class FetchShippingAddressListInteractorImpl extends AbstractInteractor implements FetchShippingAddressListInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiToken;

    public FetchShippingAddressListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int userId, String apiToken) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiToken = apiToken;
    }

    private void notifyError(String errorMsg, int isLoginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingShippingAddressFail(errorMsg, isLoginError);
            }
        });
    }

    private void postMessage(Address[] addresses){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingShippingAddressSuccess(addresses);
            }
        });
    }

    @Override
    public void run() {
        final AddressListWrapper addressListWrapper = mRepository.fetchShippingAddressList(userId, apiToken);
        if (addressListWrapper == null) {
            notifyError("Something went wrong", 0);
        } else if (!addressListWrapper.status) {
            notifyError(addressListWrapper.message, addressListWrapper.login_error);
        } else {
            postMessage(addressListWrapper.addresses);
        }
    }
}
