package n.webinfotech.bibibobi.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductDetailsData {

    @SerializedName("product")
    @Expose
    public Product product;

    @SerializedName("colors")
    @Expose
    public ProductDetailsColor[] colors;

    @SerializedName("sizes")
    @Expose
    public ProductDetailsSize[] sizes;

    @SerializedName("images")
    @Expose
    public ProductImage[] productImages;

    @SerializedName("related_products")
    @Expose
    public Product[] relatedProducts;

}
