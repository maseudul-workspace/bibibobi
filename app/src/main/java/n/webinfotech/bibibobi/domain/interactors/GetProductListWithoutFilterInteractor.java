package n.webinfotech.bibibobi.domain.interactors;

import n.webinfotech.bibibobi.domain.models.Product;

public interface GetProductListWithoutFilterInteractor {
    interface Callback {
        void onGettingProductListWithoutFilterSuccess(Product[] products, int totalPage);
        void onGettingProductListWithoutFilterFail(String errorMsg);
    }
}
