package n.webinfotech.bibibobi.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductListData {

    @SerializedName("products")
    @Expose
    public Product[] products;

    @SerializedName("second_category")
    @Expose
    public SecondCategory[] secondCategories;

    @SerializedName("color")
    @Expose
    public Colors[] colors;

    @SerializedName("designers")
    @Expose
    public Designer[] designers;

    @SerializedName("sizes")
    @Expose
    public Size[] sizes;

}
