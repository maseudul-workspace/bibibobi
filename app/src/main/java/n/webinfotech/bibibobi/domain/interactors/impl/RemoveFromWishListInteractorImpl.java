package n.webinfotech.bibibobi.domain.interactors.impl;

import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.RemoveFromWishlistInteractor;
import n.webinfotech.bibibobi.domain.interactors.base.AbstractInteractor;
import n.webinfotech.bibibobi.domain.models.CommonResponse;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class RemoveFromWishListInteractorImpl extends AbstractInteractor implements RemoveFromWishlistInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;
    int wishlistId;

    public RemoveFromWishListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId, int wishlistId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
        this.wishlistId = wishlistId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onWishListRemovedFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onWishlistRemovedSuccess();
            }
        });
    }

    @Override
    public void run() {
        CommonResponse commonResponse = mRepository.removeFromWishList(apiToken, userId, wishlistId);
        if (commonResponse == null) {
            notifyError("Something went wrong", commonResponse.loginError);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.loginError);
        } else {
            postMessage();
        }
    }
}
