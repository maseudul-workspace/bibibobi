package n.webinfotech.bibibobi.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WishlistProduct {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("product_id")
    @Expose
    public int productId;

    @SerializedName("p_name")
    @Expose
    public String name;

    @SerializedName("tag_name")
    @Expose
    public String tagNmae;

    @SerializedName("brand_id")
    @Expose
    public int brandId;

    @SerializedName("category")
    @Expose
    public int category;

    @SerializedName("first_category")
    @Expose
    public int firstCategory;

    @SerializedName("second_category")
    @Expose
    public int secondCategory;

    @SerializedName("main_image")
    @Expose
    public String mainImage;

    @SerializedName("mrp")
    @Expose
    public String mrp;

    @SerializedName("price")
    @Expose
    public String price;
}
