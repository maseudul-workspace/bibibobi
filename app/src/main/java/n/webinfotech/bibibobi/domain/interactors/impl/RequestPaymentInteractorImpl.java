package n.webinfotech.bibibobi.domain.interactors.impl;

import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.RequestPaymentInteractor;
import n.webinfotech.bibibobi.domain.interactors.base.AbstractInteractor;
import n.webinfotech.bibibobi.domain.models.PaymentRequestResponse;
import n.webinfotech.bibibobi.repository.PaymentRepositoryImpl;

public class RequestPaymentInteractorImpl extends AbstractInteractor implements RequestPaymentInteractor {

    PaymentRepositoryImpl mRepository;
    Callback mCallback;
    String authorization;
    String id;

    public RequestPaymentInteractorImpl(Executor threadExecutor, MainThread mainThread, PaymentRepositoryImpl mRepository, Callback mCallback, String authorization, String id) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.id = id;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRequestPaymentFail(errorMsg);
            }
        });
    }

    private void postMessage(PaymentRequestResponse paymentRequestResponse){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRequestPaymentSuccess(paymentRequestResponse);
            }
        });
    }

    @Override
    public void run() {
        final PaymentRequestResponse paymentRequestResponse = mRepository.requestPayment(authorization, id);
        if (paymentRequestResponse == null) {
            notifyError("Something went wrong");
        } else {
            postMessage(paymentRequestResponse);
        }
    }
}
