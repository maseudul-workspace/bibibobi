package n.webinfotech.bibibobi.domain.interactors;


import n.webinfotech.bibibobi.domain.models.GatewayOrder;

public interface InitiatePaymentInteractor {
    interface Callback {
        void onInitiatePaymentSuccess(GatewayOrder gatewayOrder);
        void onInitiatePaymentFail(String errorMsg);
    }
}
