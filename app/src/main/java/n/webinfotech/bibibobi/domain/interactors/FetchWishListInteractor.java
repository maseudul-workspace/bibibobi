package n.webinfotech.bibibobi.domain.interactors;

import n.webinfotech.bibibobi.domain.models.WishlistProduct;

public interface FetchWishListInteractor {
    interface Callback {
        void onWishlistFetchSuccess(WishlistProduct[] wishlistProducts);
        void onWishListFetchFail(String errorMsg, int loginError);
    }
}
