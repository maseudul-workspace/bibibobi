package n.webinfotech.bibibobi.domain.interactors;

public interface GetAccessTokenInteractor {
    interface Callback {
        void onGettingAccessTokenSuccess(String accessToken);
        void onGettingAccessTokenFail(String errorMsg);
    }
}
