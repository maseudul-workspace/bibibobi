package n.webinfotech.bibibobi.domain.interactors.impl;

import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.GetProductDetailsInteractor;
import n.webinfotech.bibibobi.domain.interactors.base.AbstractInteractor;
import n.webinfotech.bibibobi.domain.models.ProductDetailsData;
import n.webinfotech.bibibobi.domain.models.ProductDetailsDataWrapper;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class GetProductDetailsInteractorImpl extends AbstractInteractor implements GetProductDetailsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int productId;

    public GetProductDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int productId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.productId = productId;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onProductDetailsFail(errorMsg);
            }
        });
    }

    private void postMessage(ProductDetailsData productDetailsData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onProductDetailsSuccess(productDetailsData);
            }
        });
    }

    @Override
    public void run() {
        final ProductDetailsDataWrapper productDetailsDataWrapper = mRepository.getProductDetails(productId);
        if (productDetailsDataWrapper == null) {
            notifyError("Something went wrong");
        } else if (!productDetailsDataWrapper.status) {
            notifyError(productDetailsDataWrapper.message);
        } else {
            postMessage(productDetailsDataWrapper.productDetailsData);
        }
    }
}
