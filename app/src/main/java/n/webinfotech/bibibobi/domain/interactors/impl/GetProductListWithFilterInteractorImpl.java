package n.webinfotech.bibibobi.domain.interactors.impl;

import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.GetProductListWithFilterInteractor;
import n.webinfotech.bibibobi.domain.interactors.base.AbstractInteractor;
import n.webinfotech.bibibobi.domain.models.ProductListData;
import n.webinfotech.bibibobi.domain.models.ProductListDataWrapper;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class GetProductListWithFilterInteractorImpl extends AbstractInteractor implements GetProductListWithFilterInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int categoryId;
    int pageNo;

    public GetProductListWithFilterInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int categoryId, int pageNo) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.categoryId = categoryId;
        this.pageNo = pageNo;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductListWithFiltersFail(errorMsg);
            }
        });
    }

    private void postMessage(ProductListData productListData, int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductListWithFiltersSuccess(productListData, totalPage);
            }
        });
    }


    @Override
    public void run() {
        final ProductListDataWrapper productListDataWrapper = mRepository.getProductListWithFilters(categoryId, pageNo);
        if (productListDataWrapper == null) {
            notifyError("Something went wrong");
        } else if (!productListDataWrapper.status) {
            notifyError(productListDataWrapper.message);
        } else {
            postMessage(productListDataWrapper.productListData, productListDataWrapper.totalPage);
        }
    }
}
