package n.webinfotech.bibibobi.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductDetailsColor {
    @SerializedName("color_id")
    @Expose
    public int colorId;

    @SerializedName("color_name")
    @Expose
    public String colorName;

    @SerializedName("color_value")
    @Expose
    public String colorValue;

    public boolean isSelected = false;
}
