package n.webinfotech.bibibobi.domain.interactors;

import n.webinfotech.bibibobi.domain.models.PinData;

public interface CheckPinAvailabilityInteractor {
    interface Callback {
        void onPinAvailabilityCheckSuccess(PinData pinData);
        void onPinAvailabilityCheckFail(String errorMsg, int loginError);
    }
}
