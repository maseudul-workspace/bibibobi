package n.webinfotech.bibibobi.domain.interactors.impl;

import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.AddToCartWithoutFilterInteractor;
import n.webinfotech.bibibobi.domain.interactors.base.AbstractInteractor;
import n.webinfotech.bibibobi.domain.models.CommonResponse;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class AddToCartWithoutFilterInteractorImpl extends AbstractInteractor implements AddToCartWithoutFilterInteractor {

    AppRepositoryImpl appRepository;
    Callback mCallback;
    String authorization;
    int userId;
    int productId;

    public AddToCartWithoutFilterInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl appRepository, Callback mCallback, String authorization, int userId, int productId) {
        super(threadExecutor, mainThread);
        this.appRepository = appRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.userId = userId;
        this.productId = productId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddToCartFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddToCartSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = appRepository.addToCartWithoutFilters(authorization, userId, productId);
        if (commonResponse == null) {
            notifyError("Something went wrong", commonResponse.loginError);
        } else if(!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.loginError);
        } else {
            postMessage();
        }
    }
}
