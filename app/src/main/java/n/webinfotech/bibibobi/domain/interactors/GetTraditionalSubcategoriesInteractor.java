package n.webinfotech.bibibobi.domain.interactors;

import n.webinfotech.bibibobi.domain.models.FirstCategory;

public interface GetTraditionalSubcategoriesInteractor {
    interface Callback {
        void onGettingTraditionalSubcategoriesSuccess(FirstCategory[] firstCategories);
        void onGettingTraditionaSubcategoriesFail(String errorMsg);
    }
}
