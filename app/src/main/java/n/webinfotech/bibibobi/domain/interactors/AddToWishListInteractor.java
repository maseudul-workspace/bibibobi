package n.webinfotech.bibibobi.domain.interactors;

public interface AddToWishListInteractor {
    interface Callback {
        void onAddingWishListSuccess();
        void onAddingWishListFail(String errorMsg, int loginError);
    }
}
