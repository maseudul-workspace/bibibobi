package n.webinfotech.bibibobi.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductDetailsSize {
    @SerializedName("size_id")
    @Expose
    public int sizeId;

    @SerializedName("size_name")
    @Expose
    public String sizeName;

    public boolean isSelected = false;
}
