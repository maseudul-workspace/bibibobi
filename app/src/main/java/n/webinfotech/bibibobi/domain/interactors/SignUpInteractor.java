package n.webinfotech.bibibobi.domain.interactors;

import n.webinfotech.bibibobi.domain.models.ErrorMesage;

public interface SignUpInteractor {
    interface Callback {
        void onSignUpSuccess();
        void onSignUpFailed(String errorMsg, boolean errorCode, ErrorMesage errorMesage);
    }
}
