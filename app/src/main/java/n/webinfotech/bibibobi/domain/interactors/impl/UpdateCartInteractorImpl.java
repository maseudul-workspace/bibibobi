package n.webinfotech.bibibobi.domain.interactors.impl;


import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.UpdateCartInteractor;
import n.webinfotech.bibibobi.domain.interactors.base.AbstractInteractor;
import n.webinfotech.bibibobi.domain.models.CommonResponse;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class UpdateCartInteractorImpl extends AbstractInteractor implements UpdateCartInteractor {

    AppRepositoryImpl appRepository;
    Callback mCallback;
    String apiToken;
    int quantity;
    int cartId;
    int userId;

    public UpdateCartInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl appRepository, Callback mCallback, String apiToken, int userId, int quantity, int cartId) {
        super(threadExecutor, mainThread);
        this.appRepository = appRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.quantity = quantity;
        this.cartId = cartId;
        this.apiToken = apiToken;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUpdateCartFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUpdateCartSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = appRepository.updateCart(apiToken, userId, quantity, cartId);
        if (commonResponse == null) {
            notifyError("Something went wrong", commonResponse.loginError);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.loginError);
        } else {
           postMessage();
        }
    }
}
