package n.webinfotech.bibibobi.domain.interactors;

import n.webinfotech.bibibobi.domain.models.Product;

public interface GetProductSearchListInteractor {
    interface Callback {
        void onGettingProductListSuccess(Product[] products, String searchKey);
        void onGettingProductListFail(String errorMsg);
    }
}
