package n.webinfotech.bibibobi.domain.interactors;


import n.webinfotech.bibibobi.domain.models.Order;

public interface GetOrderHistoryInteractor {
    interface Callback {
        void onGettingOrderHistorySuccess(Order[] orders);
        void onGettingOrderHistoryFail(String errorMsg, int loginError);
    }
}
