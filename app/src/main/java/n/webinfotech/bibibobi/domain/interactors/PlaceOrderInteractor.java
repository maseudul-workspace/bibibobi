package n.webinfotech.bibibobi.domain.interactors;

import n.webinfotech.bibibobi.domain.models.OrderPlaceData;

public interface PlaceOrderInteractor {
    interface Callback {
        void onPlaceOrderSuccess(OrderPlaceData orderPlaceData);
        void onPlaceOrderFail(String errorMsg, int loginError);
    }
}
