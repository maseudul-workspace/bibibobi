package n.webinfotech.bibibobi.domain.interactors.impl;


import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.DeleteItemFromCartInteractor;
import n.webinfotech.bibibobi.domain.interactors.base.AbstractInteractor;
import n.webinfotech.bibibobi.domain.models.CommonResponse;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class DeleteItemFromCartInteractorImpl extends AbstractInteractor implements DeleteItemFromCartInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int cartId;

    public DeleteItemFromCartInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int cartId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.cartId = cartId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCartItemDeleteFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCartItemDeleteSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.deleteItemFromCart(apiToken, cartId);
        if (commonResponse == null) {
            notifyError("Something went wrong", commonResponse.loginError);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.loginError);
        } else {
            postMessage();
        }
    }
}
