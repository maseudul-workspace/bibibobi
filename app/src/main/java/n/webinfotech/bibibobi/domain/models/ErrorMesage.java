package n.webinfotech.bibibobi.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ErrorMesage {
    @SerializedName("email")
    @Expose
    public String[] email = null;

    @SerializedName("mobile")
    @Expose
    public String[] mobile = null;
}
