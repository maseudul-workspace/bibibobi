package n.webinfotech.bibibobi.domain.interactors.impl;


import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.FetchStateListInteractor;
import n.webinfotech.bibibobi.domain.interactors.base.AbstractInteractor;
import n.webinfotech.bibibobi.domain.models.StateList;
import n.webinfotech.bibibobi.domain.models.StateListWrapper;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class FetchStateListInteractorImpl extends AbstractInteractor implements FetchStateListInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;

    public FetchStateListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingStateListFail();
            }
        });
    }

    private void postMessage(StateList[] states){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingStateListSuccess(states);
            }
        });
    }

    @Override
    public void run() {
        StateListWrapper stateListWrapper = mRepository.fetchStateList();
        if (stateListWrapper == null) {
            notifyError();
        } else if (!stateListWrapper.status) {
            notifyError();
        } else {
            postMessage(stateListWrapper.stateLists);
        }
    }
}
