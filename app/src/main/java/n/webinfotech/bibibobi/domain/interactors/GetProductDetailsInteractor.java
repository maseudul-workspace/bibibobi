package n.webinfotech.bibibobi.domain.interactors;

import n.webinfotech.bibibobi.domain.models.ProductDetailsData;

public interface GetProductDetailsInteractor {
    interface Callback {
        void onProductDetailsSuccess(ProductDetailsData productDetailsData);
        void onProductDetailsFail(String errorMsg);
    }
}
