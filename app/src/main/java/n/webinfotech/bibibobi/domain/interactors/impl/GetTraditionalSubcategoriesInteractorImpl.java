package n.webinfotech.bibibobi.domain.interactors.impl;

import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.GetTraditionalSubcategoriesInteractor;
import n.webinfotech.bibibobi.domain.interactors.base.AbstractInteractor;
import n.webinfotech.bibibobi.domain.models.CategoryWrapper;
import n.webinfotech.bibibobi.domain.models.FirstCategory;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class GetTraditionalSubcategoriesInteractorImpl extends AbstractInteractor implements GetTraditionalSubcategoriesInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int traditionalType;

    public GetTraditionalSubcategoriesInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int traditionalType) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.traditionalType = traditionalType;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingTraditionaSubcategoriesFail(errorMsg);
            }
        });
    }

    private void postMessage(FirstCategory[] firstCategories){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingTraditionalSubcategoriesSuccess(firstCategories);
            }
        });
    }


    @Override
    public void run() {
        final CategoryWrapper categoryWrapper = mRepository.getTraditionalSubcategories(traditionalType);
        if (categoryWrapper == null) {
            notifyError("Something went wrong");
        } else if (!categoryWrapper.status) {
            notifyError(categoryWrapper.message);
        } else {
            postMessage(categoryWrapper.firstCategories);
        }
    }
}
