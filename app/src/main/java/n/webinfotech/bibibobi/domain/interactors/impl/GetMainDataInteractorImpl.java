package n.webinfotech.bibibobi.domain.interactors.impl;

import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.GetMainDataInteractor;
import n.webinfotech.bibibobi.domain.interactors.base.AbstractInteractor;
import n.webinfotech.bibibobi.domain.models.MainData;
import n.webinfotech.bibibobi.domain.models.MainDataWrapper;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class GetMainDataInteractorImpl extends AbstractInteractor implements GetMainDataInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;

    public GetMainDataInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingMainDataFail();
            }
        });
    }

    private void postMessage(MainData mainData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingMainDataSuccess(mainData);
            }
        });
    }

    @Override
    public void run() {
        final MainDataWrapper mainDataWrapper = mRepository.getMainData();
        if (mainDataWrapper == null) {
            notifyError();
        } else if(!mainDataWrapper.status) {
            notifyError();
        } else {
            postMessage(mainDataWrapper.data);
        }
    }
}
