package n.webinfotech.bibibobi.domain.interactors;

import n.webinfotech.bibibobi.domain.models.Cart;

public interface FetchCartInteractor {
    interface Callback {
        void onFetchCartSuccess(Cart[] carts);
        void onFetchCartFail(String errorMsg, int loginError);
    }
}
