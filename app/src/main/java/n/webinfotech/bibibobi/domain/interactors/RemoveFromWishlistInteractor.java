package n.webinfotech.bibibobi.domain.interactors;

public interface RemoveFromWishlistInteractor {
    interface Callback {
        void onWishlistRemovedSuccess();
        void onWishListRemovedFail(String errorMsg, int loginError);
    }
}
