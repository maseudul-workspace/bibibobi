package n.webinfotech.bibibobi.domain.interactors;


import n.webinfotech.bibibobi.domain.models.Address;

public interface GetAddressDetailsInteractor {
    interface Callback {
        void onGettingAddressDetailsSuccess(Address address);
        void onGettingAddressDetailsFail(String errorMsg, int loginError);
    }
}
