package n.webinfotech.bibibobi.domain.interactors.impl;

import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.SignUpInteractor;
import n.webinfotech.bibibobi.domain.interactors.base.AbstractInteractor;
import n.webinfotech.bibibobi.domain.models.ErrorMesage;
import n.webinfotech.bibibobi.domain.models.UserInfoWrapper;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;


public class SignUpInteractorImpl extends AbstractInteractor implements SignUpInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    String name;
    String email;
    String password;
    String confirmPassword;
    String mobile;

    public SignUpInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository, String name, String email, String password, String confirmPassword, String mobile) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.name = name;
        this.email = email;
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.mobile = mobile;
    }

    private void notifyError(String errorMsg, boolean errorCode, ErrorMesage errorMesage) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSignUpFailed(errorMsg, errorCode, errorMesage);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSignUpSuccess();
            }
        });
    }

    @Override
    public void run() {
        final UserInfoWrapper userInfoWrapper = mRepository.signUp(name, email, password, confirmPassword, mobile);
        if (userInfoWrapper ==  null) {
            notifyError("Something went wrong", false, null);
        } else if (!userInfoWrapper.status) {
            notifyError(userInfoWrapper.message, userInfoWrapper.errorCode, userInfoWrapper.errorMessage);
        } else {
            postMessage();
        }
    }
}
