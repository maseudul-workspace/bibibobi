package n.webinfotech.bibibobi.domain.interactors;


import n.webinfotech.bibibobi.domain.models.UserDetails;

public interface FetchUserInfoInteractor {
    interface Callback {
        void onGettingUserInfoSuccess(UserDetails userDetails);
        void onGettingUserInfoFail(String errorMsg, int loginError);
    }
}
