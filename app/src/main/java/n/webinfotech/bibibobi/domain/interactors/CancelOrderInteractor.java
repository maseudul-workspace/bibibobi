package n.webinfotech.bibibobi.domain.interactors;

public interface CancelOrderInteractor {
    interface Callback {
        void onCancelOrderSuccess();
        void onCancelOrderFail(String errorMsg, int loginError);
    }
}
