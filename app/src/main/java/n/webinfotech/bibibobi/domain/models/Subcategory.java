package n.webinfotech.bibibobi.domain.models;

/**
 * Created by Raj on 26-09-2019.
 */

public class Subcategory {

    public String image;
    public String name;

    public Subcategory(String image, String name) {
        this.image = image;
        this.name = name;
    }

}
