package n.webinfotech.bibibobi.domain.interactors.impl;

import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.GetProductSearchListInteractor;
import n.webinfotech.bibibobi.domain.interactors.base.AbstractInteractor;
import n.webinfotech.bibibobi.domain.models.Product;
import n.webinfotech.bibibobi.domain.models.ProductSearchListWrapper;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class GetProductSearchListInteractorImpl extends AbstractInteractor implements GetProductSearchListInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String searchKey;

    public GetProductSearchListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String searchKey) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.searchKey = searchKey;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductListFail(errorMsg);
            }
        });
    }

    private void postMessage(Product[] products, String searchKey){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductListSuccess(products, searchKey);
            }
        });
    }

    @Override
    public void run() {
        final ProductSearchListWrapper productSearchListWrapper = mRepository.getProductsSearchList(searchKey);
        if (productSearchListWrapper == null) {
            notifyError("Something went wrong");
        } else if (!productSearchListWrapper.status) {
            notifyError(productSearchListWrapper.message);
        } else {
            postMessage(productSearchListWrapper.products, productSearchListWrapper.searchKey);
        }
    }
}
