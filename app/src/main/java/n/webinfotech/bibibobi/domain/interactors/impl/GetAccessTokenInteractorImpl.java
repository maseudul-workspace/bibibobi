package n.webinfotech.bibibobi.domain.interactors.impl;


import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.GetAccessTokenInteractor;
import n.webinfotech.bibibobi.domain.interactors.base.AbstractInteractor;
import n.webinfotech.bibibobi.domain.models.AccessTokenResponse;
import n.webinfotech.bibibobi.repository.PaymentRepositoryImpl;

public class GetAccessTokenInteractorImpl extends AbstractInteractor implements GetAccessTokenInteractor {

    PaymentRepositoryImpl mRepository;
    Callback mCallback;
    String clientId;
    String clientSecret;
    String grantType;

    public GetAccessTokenInteractorImpl(Executor threadExecutor, MainThread mainThread, PaymentRepositoryImpl mRepository, Callback mCallback, String clientId, String clientSecret, String grantType) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.grantType = grantType;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingAccessTokenFail(errorMsg);
            }
        });
    }

    private void postMessage(String accessToken){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingAccessTokenSuccess(accessToken);
            }
        });
    }

    @Override
    public void run() {
        AccessTokenResponse accessTokenResponse = mRepository.fetchAccessToken(clientId, clientSecret, grantType);
        if (accessTokenResponse == null) {
            notifyError("Something went wrong");
        } else if (!accessTokenResponse.error.isEmpty()) {
            notifyError(accessTokenResponse.error);
        } else {
            postMessage(accessTokenResponse.accessToken);
        }
    }
}
