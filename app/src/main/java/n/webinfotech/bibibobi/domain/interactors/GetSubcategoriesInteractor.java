package n.webinfotech.bibibobi.domain.interactors;

import n.webinfotech.bibibobi.domain.models.FirstCategory;

public interface GetSubcategoriesInteractor {
    interface Callback {
        void onGettingSubcategoriesSuccess(FirstCategory[] firstCategories);
        void onGettingSubcategoriesFail(String errorMsg);
    }
}
