package n.webinfotech.bibibobi.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserInfoWrapper {

    @SerializedName("status")
    @Expose
    public boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("error_code")
    @Expose
    public boolean errorCode = false;

    @SerializedName("error_message")
    @Expose
    public ErrorMesage errorMessage;

}
