package n.webinfotech.bibibobi.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GatewayOrder {

    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("transaction_id")
    @Expose
    public String transactionId;

    @SerializedName("status")
    @Expose
    public String status;

    @SerializedName("currency")
    @Expose
    public String currency;

    @SerializedName("amount")
    @Expose
    public String amount;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("phone")
    @Expose
    public String phone;

    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("redirect_url")
    @Expose
    public String redirect_url;

}
