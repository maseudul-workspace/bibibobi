package n.webinfotech.bibibobi.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Colors {
/**
 * Created by Raj on 30-08-2019.
 */

    @SerializedName("id")
    @Expose
    public int colorId;

    @SerializedName("name")
    @Expose
    public String colorName;

    @SerializedName("value")
    @Expose
    public String colorValue;

    public boolean isSelected = false;
}
