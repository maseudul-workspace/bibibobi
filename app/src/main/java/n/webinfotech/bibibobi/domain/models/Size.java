package n.webinfotech.bibibobi.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Size {
    @SerializedName("id")
    @Expose
    public int sizeId;

    @SerializedName("name")
    @Expose
    public String sizeName;

    public boolean isSelected = false;
}
