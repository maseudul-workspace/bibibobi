package n.webinfotech.bibibobi.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MainData {

    @SerializedName("sliders")
    @Expose
    public ProductImage[] productImages;

    @SerializedName("women_traditional_products")
    @Expose
    public Product[] menTraditionalProducts;

    @SerializedName("men_traditional_products")
    @Expose
    public Product[] womenTraditionalProducts;

    @SerializedName("special_products")
    @Expose
    public Product[] specialProducts;

    @SerializedName("random")
    @Expose
    public Product[] randomProducts;

    @SerializedName("promotion1_image")
    @Expose
    public ProductImage promotionImage1;

    @SerializedName("promotion2_image")
    @Expose
    public ProductImage promotionImage2;

}
