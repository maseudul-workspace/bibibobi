package n.webinfotech.bibibobi.domain.interactors.impl;

import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.ChangePasswordInteractor;
import n.webinfotech.bibibobi.domain.interactors.base.AbstractInteractor;
import n.webinfotech.bibibobi.domain.models.CommonResponse;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class ChangePasswordInteractorImpl extends AbstractInteractor implements ChangePasswordInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String authorization;
    String currentPassword;
    String newPassword;

    public ChangePasswordInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int userId, String authorization, String currentPassword, String newPassword) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.authorization = authorization;
        this.currentPassword = currentPassword;
        this.newPassword = newPassword;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onChangePasswordFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onChangePasswordSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.changePassword(authorization, userId, currentPassword, newPassword, newPassword);
        if (commonResponse == null) {
            notifyError("Something went wrong", 0);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.loginError);
        } else {
            postMessage();
        }
    }
}
