package n.webinfotech.bibibobi.repository;

import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;

import n.webinfotech.bibibobi.domain.models.AddressDetailsWrapper;
import n.webinfotech.bibibobi.domain.models.AddressListWrapper;
import n.webinfotech.bibibobi.domain.models.CartWrapper;
import n.webinfotech.bibibobi.domain.models.CategoryWrapper;
import n.webinfotech.bibibobi.domain.models.CitiesWrapper;
import n.webinfotech.bibibobi.domain.models.CommonResponse;
import n.webinfotech.bibibobi.domain.models.MainDataWrapper;
import n.webinfotech.bibibobi.domain.models.OrderListWrapper;
import n.webinfotech.bibibobi.domain.models.OrderPlaceDataResponse;
import n.webinfotech.bibibobi.domain.models.PinDataWrapper;
import n.webinfotech.bibibobi.domain.models.ProductDetailsDataWrapper;
import n.webinfotech.bibibobi.domain.models.ProductListDataWrapper;
import n.webinfotech.bibibobi.domain.models.ProductListWithoutFilterWrapper;
import n.webinfotech.bibibobi.domain.models.ProductSearchListWrapper;
import n.webinfotech.bibibobi.domain.models.StateListWrapper;
import n.webinfotech.bibibobi.domain.models.UserDetailsWrapper;
import n.webinfotech.bibibobi.domain.models.UserInfoWrapper;
import n.webinfotech.bibibobi.domain.models.UserWrapper;
import n.webinfotech.bibibobi.domain.models.WishListWrapper;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class AppRepositoryImpl {
    public AppRepository mRepository;

    public AppRepositoryImpl() {
        mRepository = APIclient.createService(AppRepository.class);
    }

    public CategoryWrapper getMainSubcategories(int mainCatId) {
        CategoryWrapper categoryWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.getMainSubCategories(mainCatId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    categoryWrapper = null;
                }else{
                    categoryWrapper = gson.fromJson(responseBody, CategoryWrapper.class);
                }
            } else {
                categoryWrapper = null;
            }
        }catch (Exception e){
            categoryWrapper = null;
        }
        return categoryWrapper;
    }

    public CategoryWrapper getTraditionalSubcategories(int traditionType) {
        CategoryWrapper categoryWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.getTraditionalSubcategories(traditionType);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    categoryWrapper = null;
                }else{
                    categoryWrapper = gson.fromJson(responseBody, CategoryWrapper.class);
                }
            } else {
                categoryWrapper = null;
            }
        }catch (Exception e){
            categoryWrapper = null;
        }
        return categoryWrapper;
    }

    public ProductListDataWrapper getProductListWithFilters(int categoryId, int page) {
        ProductListDataWrapper productListDataWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.getProductListWithFilters(categoryId, page);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    productListDataWrapper = null;
                }else{
                    productListDataWrapper = gson.fromJson(responseBody, ProductListDataWrapper.class);
                }
            } else {
                productListDataWrapper = null;
            }
        }catch (Exception e){
            productListDataWrapper = null;
        }
        return productListDataWrapper;
    }

    public ProductListWithoutFilterWrapper getProductListWithoutFilter(int secondCategory,
                                                                       ArrayList<Integer> designerIds,
                                                                       ArrayList<Integer> sizeIds,
                                                                       ArrayList<Integer> colorIds,
                                                                       int priceFrom,
                                                                       int priceTo,
                                                                       int sortBy,
                                                                       int pageNo) {
        ProductListWithoutFilterWrapper productListWithoutFilterWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.getProductsWithoutFilter(secondCategory, designerIds, sizeIds, colorIds, priceFrom, priceTo, sortBy, pageNo);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    productListWithoutFilterWrapper = null;
                }else{
                    productListWithoutFilterWrapper = gson.fromJson(responseBody, ProductListWithoutFilterWrapper.class);
                }
            } else {
                productListWithoutFilterWrapper = null;
            }
        }catch (Exception e){
            productListWithoutFilterWrapper = null;
        }
        return productListWithoutFilterWrapper;

    }

    public ProductSearchListWrapper getProductsSearchList(String searchKey) {
        ProductSearchListWrapper productSearchListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.getProductSearchResults(searchKey);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    productSearchListWrapper = null;
                }else{
                    productSearchListWrapper = gson.fromJson(responseBody, ProductSearchListWrapper.class);
                }
            } else {
                productSearchListWrapper = null;
            }
        }catch (Exception e){
            productSearchListWrapper = null;
        }
        return productSearchListWrapper;
    }

    public ProductDetailsDataWrapper getProductDetails(int productId) {
        ProductDetailsDataWrapper productDetailsDataWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.getProductDetails(productId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    productDetailsDataWrapper = null;
                }else{
                    productDetailsDataWrapper = gson.fromJson(responseBody, ProductDetailsDataWrapper.class);
                }
            } else {
                productDetailsDataWrapper = null;
            }
        }catch (Exception e){
            productDetailsDataWrapper = null;
        }
        return productDetailsDataWrapper;
    }

    public MainDataWrapper getMainData() {
        MainDataWrapper mainDataWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.getMainData();

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    mainDataWrapper = null;
                }else{
                    mainDataWrapper = gson.fromJson(responseBody, MainDataWrapper.class);
                }
            } else {
                mainDataWrapper = null;
            }
        }catch (Exception e){
            mainDataWrapper = null;
        }
        return mainDataWrapper;
    }

    public UserInfoWrapper signUp(String name,
                                  String email,
                                  String password,
                                  String confirmPassword,
                                  String mobile) {
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();


        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> register = mRepository.userSignUp(name, email, password, confirmPassword, mobile);

            Response<ResponseBody> response = register.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            userInfoWrapper = null;
        }
        return userInfoWrapper;
    }

    public UserWrapper checkLogin(String mobile, String password) {
        UserWrapper userWrapper;
        String responseBody = "";
        Gson gson = new Gson();


        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> check = mRepository.checkLogin(mobile, password);

            Response<ResponseBody> response = check.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userWrapper = null;
                }else{
                    userWrapper = gson.fromJson(responseBody, UserWrapper.class);
                }
            } else {
                userWrapper = null;
            }
        }catch (Exception e){
            userWrapper = null;
        }
        return userWrapper;
    }

    public CommonResponse addToCart(String authorization,
                                    int userId,
                                    int productId,
                                    int size,
                                    int colorId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addToCart("Bearer " + authorization, userId, productId, size, colorId);

            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CartWrapper fetchCartList(String authorization, int userId) {
        CartWrapper cartWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchCart("Bearer " + authorization, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    cartWrapper = null;
                }else{
                    cartWrapper = gson.fromJson(responseBody, CartWrapper.class);
                }
            } else {
                cartWrapper = null;
            }
        }catch (Exception e){
            cartWrapper = null;
        }
        return cartWrapper;
    }

    public CommonResponse deleteItemFromCart(String authorization,
                                             int shippingAddressId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.deleteCartItem("Bearer " + authorization, shippingAddressId);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse updateCart(String authorization,
                                     int userId,
                                     int quanitity,
                                     int cartId
    ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> add = mRepository.updateCart("Bearer " + authorization, userId, quanitity, cartId);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse addShippingAddress(String authorization,
                                             int userId,
                                             String email,
                                             String mobile,
                                             int state,
                                             int city,
                                             String pin,
                                             String address) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addShippingAddress("Bearer " + authorization, userId, email, mobile, state, city, pin, address);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public AddressListWrapper fetchShippingAddressList(int userId, String apiToken) {
        AddressListWrapper addressListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> fetch = mRepository.fetchShippingAddressList("Bearer " + apiToken, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    addressListWrapper = null;
                }else{
                    addressListWrapper = gson.fromJson(responseBody, AddressListWrapper.class);
                }
            } else {
                addressListWrapper = null;
            }
        }catch (Exception e){
            addressListWrapper = null;
        }
        return addressListWrapper;
    }

    public StateListWrapper fetchStateList() {
        StateListWrapper stateListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> fetch = mRepository.fetchStates();

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    stateListWrapper = null;
                }else{
                    stateListWrapper = gson.fromJson(responseBody, StateListWrapper.class);
                }
            } else {
                stateListWrapper = null;
            }
        }catch (Exception e){
            stateListWrapper = null;
        }
        return stateListWrapper;
    }

    public CitiesWrapper fetchCities(int stateId) {
        CitiesWrapper citiesWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> fetch = mRepository.fetchCities(stateId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    citiesWrapper = null;
                }else{
                    citiesWrapper = gson.fromJson(responseBody, CitiesWrapper.class);
                }
            } else {
                citiesWrapper = null;
            }
        }catch (Exception e){
            citiesWrapper = null;
        }
        return citiesWrapper;
    }

    public OrderPlaceDataResponse placeOrder(String authorization, int userId, int paymentMethod, int addressId) {
        OrderPlaceDataResponse orderPlaceDataResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> place = mRepository.placeOrder("Bearer " + authorization, userId, paymentMethod, addressId);
            Response<ResponseBody> response = place.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    orderPlaceDataResponse = null;
                }else{
                    orderPlaceDataResponse = gson.fromJson(responseBody, OrderPlaceDataResponse.class);
                }
            } else {
                orderPlaceDataResponse = null;
            }
        }catch (Exception e){
            orderPlaceDataResponse = null;
        }
        return orderPlaceDataResponse;
    }

    public CommonResponse setPaymentTransactionId(String apiToken, int orderId, String paymentRequestId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;


        try {
            Call<ResponseBody> set = mRepository.setPaymentTransactionId("Bearer " + apiToken, orderId, paymentRequestId);
            Response<ResponseBody> response = set.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse updateTransactionId(String apiToken, int orderId, String paymentRequestId, String paymentId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> update = mRepository.updatePaymentTransactionId("Bearer " + apiToken, orderId, paymentRequestId, paymentId);
            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse updateShippingAddress(String authorization,
                                                int userId,
                                                int shippingAddressId,
                                                String email,
                                                String mobile,
                                                int state,
                                                int city,
                                                String pin,
                                                String address) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.updateShippingAddress("Bearer " + authorization, userId, shippingAddressId, email, mobile, state, city, pin, address);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public AddressDetailsWrapper getShippingAddressDetails(String apiToken, int userId, int addressId) {
        AddressDetailsWrapper addressDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> get = mRepository.getShippingAddressDetails("Bearer " + apiToken, userId, addressId);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    addressDetailsWrapper = null;
                }else{
                    addressDetailsWrapper = gson.fromJson(responseBody, AddressDetailsWrapper.class);
                }
            } else {
                addressDetailsWrapper = null;
            }
        }catch (Exception e){
            addressDetailsWrapper = null;
        }
        return addressDetailsWrapper;
    }

    public OrderListWrapper getOrderHistory(String apiToken, int userId) {
        OrderListWrapper orderListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> get = mRepository.getOrderHistory("Bearer " + apiToken, userId);
            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    orderListWrapper = null;
                }else{
                    orderListWrapper = gson.fromJson(responseBody, OrderListWrapper.class);
                }
            } else {
                orderListWrapper = null;
            }
        }catch (Exception e){
            orderListWrapper = null;
        }
        return orderListWrapper;
    }

    public CommonResponse cancelOrder(String apiToken, int orderId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> cancel = mRepository.cancelOrder("Bearer " + apiToken, orderId);
            Response<ResponseBody> response = cancel.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public UserDetailsWrapper fetchUserInfo(int userId, String apiToken) {
        UserDetailsWrapper userWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> fetch = mRepository.fetchUserProfile("Bearer " + apiToken, userId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userWrapper = null;
                }else{
                    userWrapper = gson.fromJson(responseBody, UserDetailsWrapper.class);
                }
            } else {
                userWrapper = null;
            }
        }catch (Exception e){
            userWrapper = null;
        }
        return userWrapper;
    }

    public CommonResponse updateUserProfile(String authorization,
                                            int userId,
                                            String gender,
                                            String name,
                                            int state,
                                            int city,
                                            String pin,
                                            String dob) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> update = mRepository.updateUserProfile("Bearer " + authorization, userId, gender, name, state, city, pin, dob);
            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();

            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse addToWishList(String authorization,
                                        int userId,
                                        int productId
                                        ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> update = mRepository.addToWishList("Bearer " + authorization, userId, productId);
            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public WishListWrapper fetchWishList(String authorization, int userId) {
        WishListWrapper wishListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchWishList("Bearer " + authorization, userId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    wishListWrapper = null;
                }else{
                    wishListWrapper = gson.fromJson(responseBody, WishListWrapper.class);
                }
            } else {
                wishListWrapper = null;
            }
        }catch (Exception e){
            wishListWrapper = null;
        }
        return wishListWrapper;
    }

    public CommonResponse removeFromWishList(String authorization,
                                        int userId,
                                        int wishListId
    ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> update = mRepository.removeFromWishlist("Bearer " + authorization, userId, wishListId);
            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public PinDataWrapper checkPinAvailability(String pin) {
        PinDataWrapper pinDataWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> check = mRepository.checkPinAvailability(pin);
            Response<ResponseBody> response = check.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    pinDataWrapper = null;
                }else{
                    pinDataWrapper = gson.fromJson(responseBody, PinDataWrapper.class);
                }
            } else {
                pinDataWrapper = null;
            }
        }catch (Exception e){
            pinDataWrapper = null;
        }
        return pinDataWrapper;
    }

    public CommonResponse addToCartWithoutFilters(String authorization,
                                    int userId,
                                    int productId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addToCartWithoutFilters("Bearer " + authorization, userId, productId);

            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse changePassword(String authorization,
                                         int userId,
                                         String currentPswd,
                                         String newPswd,
                                         String confirmPswd
                                         ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> change = mRepository.changePassword("Bearer " + authorization, userId, currentPswd, newPswd, confirmPswd);

            Response<ResponseBody> response = change.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

}
