package n.webinfotech.bibibobi.repository;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface AppRepository {

    @GET("category/list/{main_cat_id}")
    Call<ResponseBody> getMainSubCategories(@Path("main_cat_id") int id);

    @GET("category/list/3/{traditional_type}")
    Call<ResponseBody> getTraditionalSubcategories(@Path("traditional_type") int traditionalType);

    @GET("product/list/second/category/{second_category}/{page}")
    Call<ResponseBody> getProductListWithFilters(@Path("second_category") int secondCategory,
                                                 @Path("page") int page);

    @POST("product/filter")
    @FormUrlEncoded
    Call<ResponseBody> getProductsWithoutFilter(@Field("second_category") int secondCategory,
                                                @Field("designer_id[]") ArrayList<Integer> designerIds,
                                                @Field("size_id[]") ArrayList<Integer> sizeIds,
                                                @Field("color_id[]") ArrayList<Integer> colorIds,
                                                @Field("price_from") int priceFrom,
                                                @Field("price_to") int priceTo,
                                                @Field("sort_by") int sortBy,
                                                @Field("page") int pageNo
    );

    @GET("product/search/{search_key}")
    Call<ResponseBody> getProductSearchResults(@Path("search_key") String searchKey);

    @GET("product/single/view/{product_id}")
    Call<ResponseBody> getProductDetails(@Path("product_id") int productId);

    @GET("app/load")
    Call<ResponseBody> getMainData();

    @POST("user/registration")
    @FormUrlEncoded
    Call<ResponseBody> userSignUp(@Field("name") String name,
                                  @Field("email") String email,
                                  @Field("password") String password,
                                  @Field("confirm_password") String confirmPassword,
                                  @Field("mobile") String mobile
    );

    @POST("user/login")
    @FormUrlEncoded
    Call<ResponseBody> checkLogin(@Field("mobile") String mobile,
                                  @Field("password") String password
    );

    @POST("user/add/to/cart")
    @FormUrlEncoded
    Call<ResponseBody> addToCart(@Header("Authorization") String authorization,
                                 @Field("user_id") int userId,
                                 @Field("product_id") int productId,
                                 @Field("size") int size,
                                 @Field("color") int colorId);

    @GET("user/cart/all/product/{user_id}")
    Call<ResponseBody> fetchCart(@Header("Authorization") String authorization,
                                 @Path("user_id") int userId);

    @GET("user/cart/remove/item/{cart_id}")
    Call<ResponseBody> deleteCartItem(@Header("Authorization") String authorization,
                                      @Path("cart_id") int cartId
    );

    @POST("user/cart/update/quantity")
    @FormUrlEncoded
    Call<ResponseBody> updateCart(@Header("Authorization") String authorization,
                                  @Field("user_id") int userId,
                                  @Field("quantity") int qty,
                                  @Field("cart_id") int cartId
    );

    @POST("user/shipping/add")
    @FormUrlEncoded
    Call<ResponseBody> addShippingAddress(@Header("Authorization") String authorization,
                                          @Field("user_id") int userId,
                                          @Field("email") String email,
                                          @Field("mobile") String mobile,
                                          @Field("state") int state,
                                          @Field("city") int city,
                                          @Field("pin") String pin,
                                          @Field("address") String address
    );

    @GET("user/shipping/list/{user_id}")
    Call<ResponseBody> fetchShippingAddressList(@Header("Authorization") String authorization,
                                                @Path("user_id") int userId
    );

    @GET("state_list")
    Call<ResponseBody> fetchStates();


    @GET("city/list/{state_id}")
    Call<ResponseBody> fetchCities(@Path("state_id") int stateId);


    @POST("user/place/order")
    @FormUrlEncoded
    Call<ResponseBody> placeOrder(@Header("Authorization") String authorization,
                                  @Field("user_id") int userId,
                                  @Field("pay_method") int paymentMethod,
                                  @Field("address") int addressId
    );

    @GET("user/update/payment/request/id/{order_id}/{payment_rqst_id}")
    Call<ResponseBody> setPaymentTransactionId(@Header("Authorization") String authorization,
                                               @Path("order_id") int orderId,
                                               @Path("payment_rqst_id") String paymentRequestId
    );

    @GET("user/update/payment/id/{order_id}/{payment_rqst_id}/{payment_id}")
    Call<ResponseBody> updatePaymentTransactionId(@Header("Authorization") String authorization,
                                                  @Path("order_id") int orderId,
                                                  @Path("payment_rqst_id") String paymentRequestId,
                                                  @Path("payment_id") String paymentId

    );

    @POST("user/shipping/update")
    @FormUrlEncoded
    Call<ResponseBody> updateShippingAddress(@Header("Authorization") String authorization,
                                             @Field("user_id") int userId,
                                             @Field("address_id") int shippingAddressId,
                                             @Field("email") String email,
                                             @Field("mobile") String mobile,
                                             @Field("state") int state,
                                             @Field("city") int city,
                                             @Field("pin") String pin,
                                             @Field("address") String address
    );

    @GET("user/shipping/single/{user_id}/{address_id}")
    Call<ResponseBody> getShippingAddressDetails(@Header("Authorization") String authorization,
                                                 @Path("user_id") int userId,
                                                 @Path("address_id") int addressId
    );

    @GET("user/order/history/{user_id}")
    Call<ResponseBody> getOrderHistory(@Header("Authorization") String authorization,
                                       @Path("user_id") int userId);

    @GET("user/order/cancel/{order_id}")
    Call<ResponseBody> cancelOrder(@Header("Authorization") String authorization,
                                   @Path("order_id") int orderDetailsId);

    @GET("user/profile/{user_id}")
    Call<ResponseBody> fetchUserProfile(@Header("Authorization") String authorization,
                                        @Path("user_id") int userId);

    @POST("user/profile/update")
    @FormUrlEncoded
    Call<ResponseBody> updateUserProfile(@Header("Authorization") String authorization,
                                         @Field("user_id") int userId,
                                         @Field("gender") String gender,
                                         @Field("name") String name,
                                         @Field("state") int state,
                                         @Field("city") int city,
                                         @Field("pin") String pin,
                                         @Field("dob") String dob
    );

    @GET("user/add/to/wish/list/{product_id}/{user_id}")
    Call<ResponseBody> addToWishList(@Header("Authorization") String authorization,
                                     @Path("user_id") int userId,
                                     @Path("product_id") int productId
    );

    @GET("user/wish/item/remove/{user_id}/{wish_list_id}")
    Call<ResponseBody> removeFromWishlist(@Header("Authorization") String authorization,
                                     @Path("user_id") int userId,
                                     @Path("wish_list_id") int wishListId
    );

    @GET("user/wish/list/items/{user_id}")
    Call<ResponseBody> fetchWishList(@Header("Authorization") String authorization,
                                  @Path("user_id") int userId
    );

    @GET("user/Shipping/pin/check/{pin}")
    Call<ResponseBody> checkPinAvailability(@Path("pin") String pin
    );

    @POST("user/add/to/cart")
    @FormUrlEncoded
    Call<ResponseBody> addToCartWithoutFilters(@Header("Authorization") String authorization,
                                 @Field("user_id") int userId,
                                 @Field("product_id") int productId
                                 );

    @POST("user/change/password")
    @FormUrlEncoded
    Call<ResponseBody> changePassword( @Header("Authorization") String authorization,
                                       @Field("user_id") int userId,
                                       @Field("current_pass") String currentPswd,
                                       @Field("new_password") String newPswd,
                                       @Field("confirm_password") String confirmPswd

    );

}
