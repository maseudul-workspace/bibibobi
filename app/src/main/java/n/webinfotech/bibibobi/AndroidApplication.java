package n.webinfotech.bibibobi;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import n.webinfotech.bibibobi.domain.models.User;
import n.webinfotech.bibibobi.domain.models.WishlistProduct;


/**
 * Created by Raj on 22-07-2019.
 */

public class AndroidApplication extends Application {

    User userInfo;
    int walletStatus;
    WishlistProduct[] wishlistProducts;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public void setUserInfo(Context context, User userInfo){
        this.userInfo = userInfo;
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.KEY_BIBIBOBI_APP_PREFERENCES), Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        if(userInfo != null) {
            editor.putString(getString(R.string.KEY_USER_DETAILS), new Gson().toJson(userInfo));
        } else {
            editor.putString(getString(R.string.KEY_USER_DETAILS), "");
        }

        editor.commit();
    }

    public User getUserInfo(Context context){
        User user;
        if(userInfo != null){
            user = userInfo;
        }else{
            SharedPreferences sharedPref = context.getSharedPreferences(
                    getString(R.string.KEY_BIBIBOBI_APP_PREFERENCES), Context.MODE_PRIVATE);
            String userInfoJson = sharedPref.getString(getString(R.string.KEY_USER_DETAILS),"");
            if(userInfoJson.isEmpty()){
                user = null;
            }else{
                try {
                    user = new Gson().fromJson(userInfoJson, User.class);
                    this.userInfo = user;
                }catch (Exception e){
                    user = null;
                }
            }
        }
        return user;
    }

    public void setWishList(Context context, WishlistProduct[] wishlistProducts){
        this.wishlistProducts = wishlistProducts;
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.KEY_BIBIBOBI_APP_PREFERENCES), Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        if(wishlistProducts != null) {
            editor.putString(getString(R.string.KEY_WISHLIST), new Gson().toJson(wishlistProducts));
        } else {
            editor.putString(getString(R.string.KEY_WISHLIST), "");
        }

        editor.commit();
    }

    public WishlistProduct[] getWishList(Context context){
        WishlistProduct[] wishlistProducts;
        if(this.wishlistProducts != null){
            wishlistProducts = this.wishlistProducts;
        }else{
            SharedPreferences sharedPref = context.getSharedPreferences(
                    getString(R.string.KEY_BIBIBOBI_APP_PREFERENCES), Context.MODE_PRIVATE);
            String wishListJson = sharedPref.getString(getString(R.string.KEY_WISHLIST),"");
            if(wishListJson.isEmpty()){
                wishlistProducts = null;
            }else{
                try {
                    wishlistProducts = new Gson().fromJson(wishListJson, WishlistProduct[].class);
                    this.wishlistProducts = wishlistProducts;
                }catch (Exception e){
                    wishlistProducts = null;
                }
            }
        }
        return wishlistProducts;
    }

}
