package n.webinfotech.bibibobi.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AlertDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.bibibobi.R;

/**
 * Created by Raj on 04-09-2019.
 */

public class SortByDialog {

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    @BindView(R.id.layout_newest_first)
    View layoutNewestArrivals;
    @BindView(R.id.layout_low_to_high)
    View layoutPriceLow;
    @BindView(R.id.layout_high_to_low)
    View layoutPriceHigh;
    @BindView(R.id.layout_AtoZ)
    View layoutAtoZ;
    @BindView(R.id.layout_ZtoA)
    View layoutZtoA;
    @BindView(R.id.img_view_new_selected)
    ImageView imgViewNewestSelected;
    @BindView(R.id.img_view_low_selected)
    ImageView imgViewPriceLow;
    @BindView(R.id.img_view_high_selected)
    ImageView imgViewPriceHigh;
    @BindView(R.id.img_view_A_to_Z_selected)
    ImageView imgViewAtoZselected;
    @BindView(R.id.img_view_Z_to_A_selected)
    ImageView imgViewZtoAselected;
    @BindView(R.id.img_view_cancel)
    ImageView imgViewCancel;
    int sort;
    @BindView(R.id.btn_apply)
    Button btnApply;

    public interface Callback{
        void onSortApplyBtnClicked(int sort);
    }

    Callback mCallback;

    public SortByDialog(Context mContext, Activity mActivity, Callback mCallback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = mCallback;
    }

    public void setUpDialogView(){

        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_sort_by_dialog, null);
        ButterKnife.bind(this, dialogContainer);
        layoutNewestArrivals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgViewNewestSelected.setVisibility(View.VISIBLE);
                imgViewAtoZselected.setVisibility(View.GONE);
                imgViewPriceHigh.setVisibility(View.GONE);
                imgViewPriceLow.setVisibility(View.GONE);
                imgViewZtoAselected.setVisibility(View.GONE);
                sort = 1;
            }
        });

        layoutPriceLow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgViewNewestSelected.setVisibility(View.GONE);
                imgViewPriceHigh.setVisibility(View.GONE);
                imgViewPriceLow.setVisibility(View.VISIBLE);
                imgViewAtoZselected.setVisibility(View.GONE);
                imgViewZtoAselected.setVisibility(View.GONE);
                sort = 2;
            }
        });

        layoutPriceHigh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgViewNewestSelected.setVisibility(View.GONE);
                imgViewPriceHigh.setVisibility(View.VISIBLE);
                imgViewPriceLow.setVisibility(View.GONE);
                imgViewAtoZselected.setVisibility(View.GONE);
                imgViewZtoAselected.setVisibility(View.GONE);
                sort = 3;
            }
        });

        layoutAtoZ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgViewNewestSelected.setVisibility(View.GONE);
                imgViewPriceHigh.setVisibility(View.GONE);
                imgViewPriceLow.setVisibility(View.GONE);
                imgViewAtoZselected.setVisibility(View.VISIBLE);
                imgViewZtoAselected.setVisibility(View.GONE);
                sort = 4;
            }
        });

        layoutZtoA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgViewNewestSelected.setVisibility(View.GONE);
                imgViewPriceHigh.setVisibility(View.GONE);
                imgViewPriceLow.setVisibility(View.GONE);
                imgViewAtoZselected.setVisibility(View.GONE);
                imgViewZtoAselected.setVisibility(View.VISIBLE);
                sort = 5;
            }
        });

        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                mCallback.onSortApplyBtnClicked(sort);
            }
        });

        imgViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    public void showDialog() {
        dialog.show();
    }

}
