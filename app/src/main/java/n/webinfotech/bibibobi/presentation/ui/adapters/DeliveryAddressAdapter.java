package n.webinfotech.bibibobi.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.bibibobi.R;
import n.webinfotech.bibibobi.domain.models.Address;

public class DeliveryAddressAdapter extends RecyclerView.Adapter<DeliveryAddressAdapter.ViewHolder> {

    public interface Callback {
        void onEditClicked(int id);
        void onAddressSelected(int id);
    }

    Context mContext;
    Address[] addresses;
    Callback mCallback;

    public DeliveryAddressAdapter(Context mContext, Address[] addresses, Callback mCallback) {
        this.mContext = mContext;
        this.addresses = addresses;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_delivery_address, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewUsername.setText(addresses[position].email);
        holder.txtViewAddress.setText(addresses[position].address + ", " + addresses[position].landmark);
        holder.txtViewCityStatePin.setText(addresses[position].stateName + ", " + addresses[position].cityName + " - " +addresses[position].pin);
        holder.txtViewMobile.setText("Mobile: " + addresses[position].mobile);
        holder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onEditClicked(addresses[position].id);
            }
        });
        if (addresses[position].isSelected) {
            holder.imgViewCheck.setVisibility(View.VISIBLE);
        } else {
            holder.imgViewCheck.setVisibility(View.INVISIBLE);
        }

        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onAddressSelected(addresses[position].id);
            }
        });

    }

    @Override
    public int getItemCount() {
        return addresses.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_user_name)
        TextView txtViewUsername;
        @BindView(R.id.txt_view_city_state_pin)
        TextView txtViewCityStatePin;
        @BindView(R.id.txt_view_address)
        TextView txtViewAddress;
        @BindView(R.id.txt_view_mobile)
        TextView txtViewMobile;
        @BindView(R.id.btn_edit)
        Button btnEdit;
        @BindView(R.id.img_view_check)
        ImageView imgViewCheck;
        @BindView(R.id.main_layout)
        View mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateDataset(Address[] addresses) {
        this.addresses = addresses;
        notifyDataSetChanged();
    }

}
