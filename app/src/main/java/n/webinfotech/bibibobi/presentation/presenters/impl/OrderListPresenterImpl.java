package n.webinfotech.bibibobi.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import n.webinfotech.bibibobi.AndroidApplication;
import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.CancelOrderInteractor;
import n.webinfotech.bibibobi.domain.interactors.GetOrderHistoryInteractor;
import n.webinfotech.bibibobi.domain.interactors.impl.CancelOrderInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.GetOrderHistoryInteractorImpl;
import n.webinfotech.bibibobi.domain.models.Order;
import n.webinfotech.bibibobi.domain.models.User;
import n.webinfotech.bibibobi.presentation.presenters.OrderListPresenter;
import n.webinfotech.bibibobi.presentation.presenters.base.AbstractPresenter;
import n.webinfotech.bibibobi.presentation.ui.adapters.OrderListAdapter;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class OrderListPresenterImpl extends AbstractPresenter implements OrderListPresenter, GetOrderHistoryInteractor.Callback, OrderListAdapter.Callback, CancelOrderInteractor.Callback {

    Context mContext;
    OrderListPresenter.View mView;
    GetOrderHistoryInteractorImpl getOrderHistoryInteractor;
    AndroidApplication androidApplication;
    CancelOrderInteractorImpl cancelOrderInteractor;

    public OrderListPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchOrdersList() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        getOrderHistoryInteractor = new GetOrderHistoryInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId);
        getOrderHistoryInteractor.execute();
    }

    @Override
    public void onGettingOrderHistorySuccess(Order[] orders) {
        OrderListAdapter orderListAdapter = new OrderListAdapter(mContext, orders, this);
        mView.loadAdapter(orderListAdapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingOrderHistoryFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.goToLoginActivity();
        } else {
            Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCancelClicked(int id) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        cancelOrderInteractor = new CancelOrderInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, id);
        cancelOrderInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onCancelOrderSuccess() {
        fetchOrdersList();
        Toast.makeText(mContext, "Cancelled Successfully", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCancelOrderFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.goToLoginActivity();
        } else {
            Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        }
    }
}
