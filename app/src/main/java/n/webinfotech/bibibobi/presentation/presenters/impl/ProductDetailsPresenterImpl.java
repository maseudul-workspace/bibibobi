package n.webinfotech.bibibobi.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import n.webinfotech.bibibobi.AndroidApplication;
import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.AddToCartInteractor;
import n.webinfotech.bibibobi.domain.interactors.AddToWishListInteractor;
import n.webinfotech.bibibobi.domain.interactors.CheckPinAvailabilityInteractor;
import n.webinfotech.bibibobi.domain.interactors.FetchCartInteractor;
import n.webinfotech.bibibobi.domain.interactors.FetchWishListInteractor;
import n.webinfotech.bibibobi.domain.interactors.GetProductDetailsInteractor;
import n.webinfotech.bibibobi.domain.interactors.RemoveFromWishlistInteractor;
import n.webinfotech.bibibobi.domain.interactors.impl.AddToCartInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.AddToWishListInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.CheckPinAvailabilityInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.FetchCartInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.FetchWishListInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.GetProductDetailsInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.RemoveFromWishListInteractorImpl;
import n.webinfotech.bibibobi.domain.models.Cart;
import n.webinfotech.bibibobi.domain.models.PinData;
import n.webinfotech.bibibobi.domain.models.ProductDetailsColor;
import n.webinfotech.bibibobi.domain.models.ProductDetailsData;
import n.webinfotech.bibibobi.domain.models.ProductDetailsSize;
import n.webinfotech.bibibobi.domain.models.User;
import n.webinfotech.bibibobi.domain.models.WishlistProduct;
import n.webinfotech.bibibobi.presentation.presenters.ProductDetailsPresenter;
import n.webinfotech.bibibobi.presentation.presenters.base.AbstractPresenter;
import n.webinfotech.bibibobi.presentation.ui.adapters.ColorsHorizontalAdapter;
import n.webinfotech.bibibobi.presentation.ui.adapters.ProductDetailsSizeAdapter;
import n.webinfotech.bibibobi.presentation.ui.adapters.ProductsHorizontalAdapter;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class ProductDetailsPresenterImpl extends AbstractPresenter implements   ProductDetailsPresenter,
                                                                                GetProductDetailsInteractor.Callback,
                                                                                ProductDetailsSizeAdapter.Callback,
                                                                                ColorsHorizontalAdapter.Callback,
                                                                                ProductsHorizontalAdapter.Callback,
                                                                                AddToCartInteractor.Callback,
                                                                                FetchCartInteractor.Callback,
                                                                                AddToWishListInteractor.Callback,
                                                                                FetchWishListInteractor.Callback,
                                                                                RemoveFromWishlistInteractor.Callback,
                                                                                CheckPinAvailabilityInteractor.Callback
{

    Context mContext;
    ProductDetailsPresenter.View mView;
    GetProductDetailsInteractorImpl  getProductDetailsInteractor;
    ProductDetailsSizeAdapter productDetailsSizeAdapter;
    ColorsHorizontalAdapter colorsHorizontalAdapter;
    ProductsHorizontalAdapter productsHorizontalAdapter;
    ProductDetailsColor[] productDetailsColors;
    ProductDetailsSize[] productDetailsSizes;
    AndroidApplication androidApplication;
    AddToCartInteractorImpl addToCartInteractor;
    int sizeId;
    int colorId;
    FetchCartInteractorImpl fetchCartInteractor;
    AddToWishListInteractorImpl addToWishListInteractor;
    RemoveFromWishListInteractorImpl removeFromWishListInteractor;
    FetchWishListInteractorImpl fetchWishListInteractor;
    CheckPinAvailabilityInteractorImpl checkPinAvailabilityInteractor;

    public ProductDetailsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void getProductDetails(int productId) {
        getProductDetailsInteractor = new GetProductDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, productId);
        getProductDetailsInteractor.execute();
    }

    @Override
    public void addToCart(int productId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        if (user != null) {
            mView.showLoader();
            addToCartInteractor = new AddToCartInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, productId, sizeId, colorId);
            addToCartInteractor.execute();
        }
    }

    @Override
    public void fetchCartCount() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        if (user != null) {
            fetchCartInteractor = new FetchCartInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId);
            fetchCartInteractor.execute();
        }
    }

    @Override
    public void fetchWishlist() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        if (user != null) {
            fetchWishListInteractor = new FetchWishListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId);
            fetchWishListInteractor.execute();
        }
    }

    @Override
    public void addToWishList(int productId) {
        mView.showLoader();
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        if (user != null) {
            addToWishListInteractor = new AddToWishListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(),  this, user.apiToken, user.userId, productId);
            addToWishListInteractor.execute();
        }
    }

    @Override
    public void removeFromWishlist(int productId) {
        mView.showLoader();
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        WishlistProduct[] wishlistProducts = androidApplication.getWishList(mContext);
        User user = androidApplication.getUserInfo(mContext);
        int wishListId = 0;
        for (int i = 0; i < wishlistProducts.length; i++) {
            if (wishlistProducts[i].productId == productId) {
                wishListId = wishlistProducts[i].id;
                break;
            }
        }
        removeFromWishListInteractor = new RemoveFromWishListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, wishListId);
        removeFromWishListInteractor.execute();
    }

    @Override
    public void checkPin(String pin) {
        checkPinAvailabilityInteractor = new CheckPinAvailabilityInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, pin);
        checkPinAvailabilityInteractor.execute();
    }

    @Override
    public void onProductDetailsSuccess(ProductDetailsData productDetailsData) {

        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        WishlistProduct[] wishlistProducts = androidApplication.getWishList(mContext);
        if (user != null && wishlistProducts != null && wishlistProducts.length != 0) {
            for (int i = 0; i < wishlistProducts.length; i++) {
                if (productDetailsData.product.id == wishlistProducts[i].productId) {
                    productDetailsData.product.isWishListPresent = true;
                    break;
                }
            }
        }
        productDetailsSizeAdapter = new ProductDetailsSizeAdapter(mContext, productDetailsData.sizes, this);
        colorsHorizontalAdapter = new ColorsHorizontalAdapter(mContext, productDetailsData.colors, this);
        productsHorizontalAdapter = new ProductsHorizontalAdapter(mContext, productDetailsData.relatedProducts, this);
        try {
            productDetailsData.sizes[0].isSelected = true;
            productDetailsData.colors[0].isSelected = true;
            sizeId = productDetailsData.sizes[0].sizeId;
            colorId = productDetailsData.colors[0].colorId;
        } catch (ArrayIndexOutOfBoundsException e) {

        }
        productDetailsColors = productDetailsData.colors;
        productDetailsSizes = productDetailsData.sizes;
        mView.loadData(productDetailsData.product, productDetailsData.productImages, productDetailsSizeAdapter, colorsHorizontalAdapter, productsHorizontalAdapter);
        mView.hideLoader();
    }

    @Override
    public void onProductDetailsFail(String errorMsg) {
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        mView.hideLoader();
    }

    @Override
    public void onFilterSelect(int id) {
        for (int i = 0; i < productDetailsSizes.length; i++) {
            if (productDetailsSizes[i].sizeId == id) {
                productDetailsSizes[i].isSelected = true;
                sizeId = id;
            } else {
                productDetailsSizes[i].isSelected = false;
            }
        }
        productDetailsSizeAdapter.updateDataSet(productDetailsSizes);
    }

    @Override
    public void onColorSelect(int id) {
        for (int i = 0; i < productDetailsColors.length; i++) {
            if (productDetailsColors[i].colorId == id) {
                productDetailsColors[i].isSelected = true;
                colorId = id;
            } else {
                productDetailsColors[i].isSelected = false;
            }
        }
        colorsHorizontalAdapter.updateColors(productDetailsColors);
    }

    @Override
    public void onProductClicked(int productId) {
        mView.goToProductDetails(productId);
    }

    @Override
    public void onAddToCartSuccess() {
        mView.hideLoader();
        fetchCartCount();
        Toasty.success(mContext, "Product Added SuccessFull", Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onAddToCartFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.goToLoginActivity();
        } else {
            Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFetchCartSuccess(Cart[] carts) {
        mView.loadCartCount(carts.length);
    }

    @Override
    public void onFetchCartFail(String errorMsg, int loginError) {

    }

    @Override
    public void onAddingWishListSuccess() {
        mView.hideLoader();
        mView.onAddToWishlistSuccess();
        Toast.makeText(mContext, "Added To Wishlist", Toast.LENGTH_SHORT).show();
        fetchWishlist();
    }

    @Override
    public void onAddingWishListFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.goToLoginActivity();
        } else {
            Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onWishlistFetchSuccess(WishlistProduct[] wishlistProducts) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishList(mContext, wishlistProducts);
    }

    @Override
    public void onWishListFetchFail(String errorMsg, int loginError) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishList(mContext, null);
    }

    @Override
    public void onWishlistRemovedSuccess() {
        mView.hideLoader();
        mView.onRemoveFromWishlistSuccess();
        Toast.makeText(mContext, "Remove From Wishlist", Toast.LENGTH_SHORT).show();
        fetchWishlist();
    }

    @Override
    public void onWishListRemovedFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPinAvailabilityCheckSuccess(PinData pinData) {
        mView.hideLoader();
        mView.onPinAvailable(pinData);
    }

    @Override
    public void onPinAvailabilityCheckFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.goToLoginActivity();
        } else {
            mView.onPinUnavailable();
        }
    }
}
