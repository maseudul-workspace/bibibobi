package n.webinfotech.bibibobi.presentation.presenters;

import n.webinfotech.bibibobi.presentation.ui.adapters.WishlistAdapter;

public interface WishlistPresenter {
    void fetchWishlist();
    interface View {
        void loadWishlistAdapter(WishlistAdapter wishlistAdapter);
        void goToProductDetails(int productId);
        void showLoader();
        void hideLoader();
        void hideRecyclerView();
        void goToLoginActivity();
    }
}
