package n.webinfotech.bibibobi.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import n.webinfotech.bibibobi.AndroidApplication;
import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.AddAddressInteractor;
import n.webinfotech.bibibobi.domain.interactors.FetchCitiesInteractor;
import n.webinfotech.bibibobi.domain.interactors.FetchStateListInteractor;
import n.webinfotech.bibibobi.domain.interactors.impl.AddAddressInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.FetchCitiesInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.FetchStateListInteractorImpl;
import n.webinfotech.bibibobi.domain.models.Cities;
import n.webinfotech.bibibobi.domain.models.StateList;
import n.webinfotech.bibibobi.domain.models.User;
import n.webinfotech.bibibobi.presentation.presenters.AddAddressPresenter;
import n.webinfotech.bibibobi.presentation.presenters.base.AbstractPresenter;
import n.webinfotech.bibibobi.presentation.ui.adapters.CityListDialogAdapter;
import n.webinfotech.bibibobi.presentation.ui.adapters.StateListDialogAdapter;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;


public class AddAddressPresenterImpl extends AbstractPresenter implements AddAddressPresenter,
                                                                            FetchStateListInteractor.Callback,
                                                                            StateListDialogAdapter.Callback,
                                                                            FetchCitiesInteractor.Callback,
                                                                            CityListDialogAdapter.Callback,
                                                                            AddAddressInteractor.Callback {

    Context mContext;
    AddAddressPresenter.View mView;
    FetchStateListInteractorImpl fetchStateListInteractor;
    StateListDialogAdapter stateListDialogAdapter;
    FetchCitiesInteractorImpl fetchCitiesInteractor;
    int stateId = 3;
    CityListDialogAdapter cityListDialogAdapter;
    AndroidApplication androidApplication;
    AddAddressInteractorImpl addAddressInteractor;
    int cityId = 1;

    public AddAddressPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void onAddAddressSuccess() {
        Toast.makeText(mContext, "Address Saved Successfully", Toast.LENGTH_SHORT).show();
        mView.hideLoader();
        mView.onAddAddressSuccess();
    }

    @Override
    public void onAddAddressFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.goToLoginActivity();
        } else {
            Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onGettingCitiesSuccess(Cities[] cities) {
        if (cities.length == 0) {
            mView.hideCityRecyclerView();
        } else {
            cityListDialogAdapter = new CityListDialogAdapter(mContext, cities, this::onCityClicked);
            mView.loadCitiesAdapter(cityListDialogAdapter);
        }
    }

    @Override
    public void onGettingCitiesFail() {
        mView.hideCityRecyclerView();
    }

    @Override
    public void onGettingStateListSuccess(StateList[] states) {
        if (states.length == 0) {
            mView.hideStateRecyclerView();
        } else {
            stateListDialogAdapter = new StateListDialogAdapter(mContext, states, this::onStateClicked);
            mView.loadStateAdapter(stateListDialogAdapter);
        }
    }

    @Override
    public void onGettingStateListFail() {
        mView.hideStateRecyclerView();
    }

    @Override
    public void fetchStateList() {
        fetchStateListInteractor = new FetchStateListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this);
        fetchStateListInteractor.execute();
    }

    @Override
    public void fetchCityList() {
        fetchCitiesInteractor = new FetchCitiesInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, stateId);
        fetchCitiesInteractor.execute();
    }

    @Override
    public void addAddress(String email, String mobile, String pin, String address) {
        if (stateId == 0) {
            Toast.makeText(mContext, "Please select a state", Toast.LENGTH_SHORT).show();
        } else if (cityId == 0) {
            Toast.makeText(mContext, "Please select a city", Toast.LENGTH_SHORT).show();
        } else {
            androidApplication = (AndroidApplication) mContext.getApplicationContext();
            User userInfo = androidApplication.getUserInfo(mContext);
            addAddressInteractor = new AddAddressInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId, email, mobile, stateId, cityId, pin, address);
            addAddressInteractor.execute();
        }
    }

    @Override
    public void onCityClicked(int id, String city) {
        mView.setCityName(city);
        cityId = id;
    }

    @Override
    public void onStateClicked(int id, String stateName) {
        stateId = id;
        mView.setStateName(stateName);
        fetchCityList();
    }
}
