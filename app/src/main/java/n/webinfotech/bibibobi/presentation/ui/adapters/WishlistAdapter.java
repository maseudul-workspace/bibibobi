package n.webinfotech.bibibobi.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.bibibobi.R;
import n.webinfotech.bibibobi.domain.models.WishlistProduct;
import n.webinfotech.bibibobi.util.GlideHelper;

public class WishlistAdapter extends RecyclerView.Adapter<WishlistAdapter.ViewHolder> {

    public interface Callback {
        void onProductClicked(int productId);
        void removeFromWishlist(int wishlistId);
    }

    Context mContext;
    WishlistProduct[] wishlistProducts;
    Callback mCallback;

    public WishlistAdapter(Context mContext, WishlistProduct[] wishlistProducts, Callback callback) {
        this.mContext = mContext;
        this.wishlistProducts = wishlistProducts;
        this.mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_product_list_vertical, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GlideHelper.setImageView(mContext, holder.imgViewProduct, mContext.getResources().getString(R.string.base_url) + "/images/product/thumb/" + wishlistProducts[position].mainImage);
        holder.txtViewProductTitle.setText(wishlistProducts[position].name);
        holder.txtViewSellingPrice.setText("Rs. " + wishlistProducts[position].price);
        holder.cardProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onProductClicked(wishlistProducts[position].productId);
            }
        });
        holder.cardAddCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        holder.imgViewHeartFill.setVisibility(View.VISIBLE);
        holder.imgViewHeartFill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.removeFromWishlist(wishlistProducts[position].id);
            }
        });

    }

    @Override
    public int getItemCount() {
        return wishlistProducts.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_product)
        ImageView imgViewProduct;
        @BindView(R.id.txt_view_product_title)
        TextView txtViewProductTitle;
        @BindView(R.id.txt_view_selling_price)
        TextView txtViewSellingPrice;
        @BindView(R.id.card_product)
        View cardProduct;
        @BindView(R.id.card_add_cart)
        View cardAddCart;
        @BindView(R.id.img_view_heart_fill)
        ImageView imgViewHeartFill;
        @BindView(R.id.img_view_heart_outline)
        ImageView imgViewHeartOutline;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
