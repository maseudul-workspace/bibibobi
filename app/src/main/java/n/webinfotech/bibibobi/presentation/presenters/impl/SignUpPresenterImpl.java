package n.webinfotech.bibibobi.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.SignUpInteractor;
import n.webinfotech.bibibobi.domain.interactors.impl.SignUpInteractorImpl;
import n.webinfotech.bibibobi.domain.models.ErrorMesage;
import n.webinfotech.bibibobi.presentation.presenters.SignUpPresenter;
import n.webinfotech.bibibobi.presentation.presenters.base.AbstractPresenter;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class SignUpPresenterImpl extends AbstractPresenter implements SignUpPresenter, SignUpInteractor.Callback {

    Context mContext;
    SignUpPresenter.View mView;
    SignUpInteractorImpl signUpInteractor;

    public SignUpPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void signUp(String name, String email, String mobile, String password, String confirmPassword) {
        signUpInteractor = new SignUpInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), name, email, password, confirmPassword, mobile);
        signUpInteractor.execute();
    }

    @Override
    public void onSignUpSuccess() {
        mView.hideLoader();
        Toast.makeText(mContext, "Registered Successfully", Toast.LENGTH_SHORT).show();
        mView.goToLoginActivity();
    }

    @Override
    public void onSignUpFailed(String errorMsg, boolean errorCode, ErrorMesage errorMesage) {
        mView.hideLoader();
        if (errorCode) {
            if (errorMesage != null) {
                if (errorMesage.email != null) {
                    mView.onEmailError(errorMesage.email[0]);
                }
                if (errorMesage.mobile != null) {
                    mView.onMobileError(errorMesage.mobile[0]);
                }
            }
        } else {
            Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        }
    }
}
