package n.webinfotech.bibibobi.presentation.presenters;

import n.webinfotech.bibibobi.presentation.ui.adapters.ProductListVerticalAdapter;

public interface SearchActivityPresenter {
    void fetchProducts(String searchKey);
    void fetchWishlist();
    interface View {
        void loadAdapter(ProductListVerticalAdapter adapter);
        void showLoader();
        void hideLoader();
        void hideRecyclerView();
        void goToProductDetails(int productId);
        void goToLoginActivity();
    }
}
