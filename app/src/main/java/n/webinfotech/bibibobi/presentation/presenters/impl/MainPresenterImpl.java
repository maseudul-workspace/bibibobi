package n.webinfotech.bibibobi.presentation.presenters.impl;

import android.content.Context;

import n.webinfotech.bibibobi.AndroidApplication;
import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.FetchCartInteractor;
import n.webinfotech.bibibobi.domain.interactors.FetchWishListInteractor;
import n.webinfotech.bibibobi.domain.interactors.GetMainDataInteractor;
import n.webinfotech.bibibobi.domain.interactors.impl.FetchCartInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.FetchWishListInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.GetMainDataInteractorImpl;
import n.webinfotech.bibibobi.domain.models.Cart;
import n.webinfotech.bibibobi.domain.models.MainData;
import n.webinfotech.bibibobi.domain.models.User;
import n.webinfotech.bibibobi.domain.models.WishlistProduct;
import n.webinfotech.bibibobi.presentation.presenters.MainPresenter;
import n.webinfotech.bibibobi.presentation.presenters.base.AbstractPresenter;
import n.webinfotech.bibibobi.presentation.ui.adapters.ProductListGridAdapter;
import n.webinfotech.bibibobi.presentation.ui.adapters.ProductsHorizontalAdapter;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class MainPresenterImpl extends AbstractPresenter implements MainPresenter,
                                                                    GetMainDataInteractor.Callback,
                                                                    ProductsHorizontalAdapter.Callback,
                                                                    ProductListGridAdapter.Callback,
                                                                    FetchWishListInteractor.Callback,
                                                                    FetchCartInteractor.Callback
{

    Context mContext;
    MainPresenter.View mView;
    GetMainDataInteractorImpl getMainDataInteractor;
    FetchWishListInteractorImpl fetchWishListInteractor;
    FetchCartInteractorImpl fetchCartInteractor;

    public MainPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void loadData() {
        getMainDataInteractor = new GetMainDataInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this);
        getMainDataInteractor.execute();
    }

    @Override
    public void fetchWishList() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        if (user != null) {
            fetchWishListInteractor = new FetchWishListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId);
            fetchWishListInteractor.execute();
        }
    }

    @Override
    public void fetchCartCount() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        if (user != null) {
            fetchCartInteractor = new FetchCartInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId);
            fetchCartInteractor.execute();
        }
    }

    @Override
    public void onGettingMainDataSuccess(MainData mainData) {
        ProductsHorizontalAdapter womenTraditionalAdapter = new ProductsHorizontalAdapter(mContext, mainData.womenTraditionalProducts, this);
        ProductsHorizontalAdapter menTraditionalAdapter = new ProductsHorizontalAdapter(mContext, mainData.menTraditionalProducts, this);
        ProductsHorizontalAdapter specialProductsAdapter = new ProductsHorizontalAdapter(mContext, mainData.specialProducts, this);
        ProductListGridAdapter randomProductsAdapter = new ProductListGridAdapter(mContext, mainData.randomProducts, this);
        mView.setData(mainData.productImages, womenTraditionalAdapter, menTraditionalAdapter, specialProductsAdapter, randomProductsAdapter, mainData.promotionImage1.image, mainData.promotionImage2.image);
    }

    @Override
    public void onGettingMainDataFail() {

    }

    @Override
    public void onProductClicked(int productId) {
        mView.goToProdyctDetails(productId);
    }

    @Override
    public void onWishlistFetchSuccess(WishlistProduct[] wishlistProducts) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishList(mContext, wishlistProducts);
    }

    @Override
    public void onWishListFetchFail(String errorMsg, int loginError) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishList(mContext, null);
    }

    @Override
    public void onFetchCartSuccess(Cart[] carts) {
        mView.loadCartCount(carts.length);
    }

    @Override
    public void onFetchCartFail(String errorMsg, int loginError) {
        mView.loadCartCount(0);
    }
}
