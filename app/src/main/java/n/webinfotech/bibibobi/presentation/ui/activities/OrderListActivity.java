package n.webinfotech.bibibobi.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.bibibobi.R;
import n.webinfotech.bibibobi.domain.executors.impl.ThreadExecutor;
import n.webinfotech.bibibobi.presentation.presenters.OrderListPresenter;
import n.webinfotech.bibibobi.presentation.presenters.impl.OrderListPresenterImpl;
import n.webinfotech.bibibobi.presentation.ui.adapters.OrderListAdapter;
import n.webinfotech.bibibobi.threading.MainThreadImpl;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

public class OrderListActivity extends AppCompatActivity implements OrderListPresenter.View {

    @BindView(R.id.recycler_view_order_list)
    RecyclerView recyclerView;
    ProgressDialog progressDialog;
    OrderListPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);
        ButterKnife.bind(this);
        initialisePresenter();
        getSupportActionBar().setTitle("Order History");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setProgressDialog();
        mPresenter.fetchOrdersList();
        showLoader();
    }

    public void initialisePresenter() {
        mPresenter = new OrderListPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadAdapter(OrderListAdapter orderListAdapter) {
        recyclerView.setAdapter(orderListAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void goToLoginActivity() {
        Toast.makeText(this, "Your session expired !! Please login again", Toast.LENGTH_LONG).show();
        Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(loginIntent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
