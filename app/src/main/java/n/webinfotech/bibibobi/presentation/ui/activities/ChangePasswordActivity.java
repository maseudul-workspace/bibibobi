package n.webinfotech.bibibobi.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import n.webinfotech.bibibobi.R;
import n.webinfotech.bibibobi.domain.executors.impl.ThreadExecutor;
import n.webinfotech.bibibobi.presentation.presenters.ChangePasswordPresenter;
import n.webinfotech.bibibobi.presentation.presenters.impl.ChangePasswordPresenterImpl;
import n.webinfotech.bibibobi.threading.MainThreadImpl;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

public class ChangePasswordActivity extends AppCompatActivity implements ChangePasswordPresenter.View {

    @BindView(R.id.txt_input_current_password_layout)
    TextInputLayout txtInputCurrentPasswordLayout;
    @BindView(R.id.txt_input_new_password_layout)
    TextInputLayout txtInputNewPasswordLayout;
    @BindView(R.id.txt_input_confirm_password_layout)
    TextInputLayout txtInputConfirmPasswordLayout;
    @BindView(R.id.edit_text_current_password)
    EditText editTextCurrentPassword;
    @BindView(R.id.edit_text_new_password)
    EditText editTextNewPassword;
    @BindView(R.id.edit_text_confirm_password)
    EditText editTextConfirmPassword;
    ProgressDialog progressDialog;
    ChangePasswordPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Change Password");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setUpProgressDialog();
        initialisePresenter();
    }

    public void initialisePresenter() {
        mPresenter = new ChangePasswordPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @OnClick(R.id.btn_change_password) void onChangePasswordClicked() {
        if (editTextCurrentPassword.getText().toString().trim().isEmpty()) {
            txtInputCurrentPasswordLayout.setError("*Current Password Required");
            txtInputNewPasswordLayout.setError("");
            txtInputConfirmPasswordLayout.setError("");
        } else if (editTextNewPassword.getText().toString().trim().isEmpty()) {
            txtInputNewPasswordLayout.setError("*New Password Required");
            txtInputCurrentPasswordLayout.setError("");
            txtInputConfirmPasswordLayout.setError("");
        } else if (editTextConfirmPassword.getText().toString().trim().isEmpty()) {
            txtInputConfirmPasswordLayout.setError("*Repeat Password");
            txtInputNewPasswordLayout.setError("");
            txtInputCurrentPasswordLayout.setError("");
        } else if (!editTextNewPassword.getText().toString().equals(editTextConfirmPassword.getText().toString())) {
            txtInputConfirmPasswordLayout.setError("*Password Mismatched");
            txtInputNewPasswordLayout.setError("");
            txtInputCurrentPasswordLayout.setError("");
        } else {
            mPresenter.changePassword(editTextCurrentPassword.getText().toString(), editTextNewPassword.getText().toString());
            showLoader();
        }
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void onChangePasswordSuccess() {
        finish();
    }

    @Override
    public void goToLoginActivity() {
        Toast.makeText(this, "Your session expired !! Please login again", Toast.LENGTH_LONG).show();
        Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(loginIntent);
    }
}
