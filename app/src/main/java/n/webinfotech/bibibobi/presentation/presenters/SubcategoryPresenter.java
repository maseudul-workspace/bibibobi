package n.webinfotech.bibibobi.presentation.presenters;

import n.webinfotech.bibibobi.presentation.ui.adapters.FirstCategoryAdapter;

public interface SubcategoryPresenter {
    void fetchSubcategories(int mainCatId);
    void fetchTraditionalCategories(int traditionaType);
    interface View {
        void loadAdapter(FirstCategoryAdapter adapter);
        void showLoader();
        void hideLoader();
        void goToProductListActivity(int id);
    }
}
