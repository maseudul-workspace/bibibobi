package n.webinfotech.bibibobi.presentation.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.bibibobi.R;
import n.webinfotech.bibibobi.domain.models.Size;

/**
 * Created by Raj on 02-09-2019.
 */

public class SizesFilterAdapter extends RecyclerView.Adapter<SizesFilterAdapter.ViewHolder> {

    public interface Callback {
        void onSizeSelect(int sellerId);
        void onSizeNotSelect(int sellerId);
    }

    Context mContext;
    Size[] sizes;
    Callback mCallback;

    public SizesFilterAdapter(Context mContext, Size[] sizes, Callback callback) {
        this.mContext = mContext;
        this.sizes = sizes;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_sizes_filter, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.checkBox.setText(sizes[i].sizeName);
        viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mCallback.onSizeSelect(sizes[i].sizeId);
                } else {
                    mCallback.onSizeNotSelect(sizes[i].sizeId);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return sizes.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.recycler_view_size_filter_by_checkbox)
        CheckBox checkBox;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
