package n.webinfotech.bibibobi.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.view.MenuItem;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.bibibobi.R;
import n.webinfotech.bibibobi.domain.executors.impl.ThreadExecutor;
import n.webinfotech.bibibobi.presentation.presenters.SubcategoryPresenter;
import n.webinfotech.bibibobi.presentation.presenters.impl.SubcategoryPresenterImpl;
import n.webinfotech.bibibobi.presentation.ui.adapters.FirstCategoryAdapter;
import n.webinfotech.bibibobi.threading.MainThreadImpl;
import n.webinfotech.bibibobi.util.GlideHelper;

public class SubCategoryActivity extends AppCompatActivity implements SubcategoryPresenter.View {

    @BindView(R.id.recycler_view_subcategory)
    RecyclerView recyclerViewSubcategory;
    @BindView(R.id.img_view_main_category)
    ImageView imgViewMainCategory;
    String image;
    String title;
    SubcategoryPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    boolean isTraditional;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category);
        ButterKnife.bind(this);
        image = getIntent().getStringExtra("image");
        title = getIntent().getStringExtra("title");
        id = getIntent().getIntExtra("id", 0);
        isTraditional = getIntent().getBooleanExtra("isTraditional", false);
        getSupportActionBar().setTitle((Html.fromHtml("<font color=\"#00000\"> " + title +" </font>")));
        GlideHelper.setImageViewCustomRoundedCorners(this, imgViewMainCategory, image, 10);
        initialisePresenter();
        setUpProgressDialog();
        if (isTraditional) {
            mPresenter.fetchTraditionalCategories(id);
        } else {
            mPresenter.fetchSubcategories(id);
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void initialisePresenter() {
        mPresenter = new SubcategoryPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadAdapter(FirstCategoryAdapter adapter) {
        recyclerViewSubcategory.setNestedScrollingEnabled(false);
        recyclerViewSubcategory.setAdapter(adapter);
        recyclerViewSubcategory.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void showLoader() {
    }

    @Override
    public void hideLoader() {
    }

    @Override
    public void goToProductListActivity(int id) {
        Intent intent = new Intent(this, ProductListActivity.class);
        intent.putExtra("categoryId", id);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
