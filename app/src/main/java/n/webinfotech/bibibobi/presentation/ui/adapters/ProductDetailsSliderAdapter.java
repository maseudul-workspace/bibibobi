package n.webinfotech.bibibobi.presentation.ui.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import n.webinfotech.bibibobi.R;
import n.webinfotech.bibibobi.domain.models.ProductImage;
import n.webinfotech.bibibobi.util.GlideHelper;

public class ProductDetailsSliderAdapter extends PagerAdapter {

    Context mContext;
    ProductImage[] productImages;

    public ProductDetailsSliderAdapter(Context mContext, ProductImage[] productImages) {
        this.mContext = mContext;
        this.productImages = productImages;
    }

    @Override
    public int getCount() {
        return productImages.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ImageView imageView = new ImageView(mContext);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        GlideHelper.setImageView(mContext, imageView, mContext.getResources().getString(R.string.base_url) + "/images/product/" + productImages[position].image);
        container.addView(imageView);
        return imageView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

}
