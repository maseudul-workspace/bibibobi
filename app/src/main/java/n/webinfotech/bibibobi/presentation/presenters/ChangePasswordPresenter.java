package n.webinfotech.bibibobi.presentation.presenters;

public interface ChangePasswordPresenter {
    void changePassword(String currentPswd, String newPswd);
    interface View {
        void showLoader();
        void hideLoader();
        void onChangePasswordSuccess();
        void goToLoginActivity();

    }
}
