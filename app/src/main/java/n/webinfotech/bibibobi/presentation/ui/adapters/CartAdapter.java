package n.webinfotech.bibibobi.presentation.ui.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.bibibobi.R;
import n.webinfotech.bibibobi.domain.models.Cart;
import n.webinfotech.bibibobi.util.GlideHelper;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {

    public interface Callback {
        void onDeleteClicked(int cartId);
        void onUpdateClicked(int cartId, int qty);
    }

    Context mContext;
    Cart[] carts;
    Callback mCallback;

    public CartAdapter(Context mContext, Cart[] carts, Callback callback) {
        this.mContext = mContext;
        this.carts = carts;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_cart, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewProductTitle.setText(carts[position].productName);
        holder.txtViewQty.setText(Integer.toString(carts[position].quantity));
        GlideHelper.setImageView(mContext, holder.imgViewProduct, mContext.getResources().getString(R.string.base_url) + "/images/product/thumb/" + carts[position].productImage);
        holder.imgViewCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Are You Sure ?");
                builder.setMessage("You are about to delete a remove an item from your cart. Do you really want to proceed ?");
                builder.setCancelable(false);
                builder.setPositiveButton(Html.fromHtml("<font color='#f4454c'>Yes</font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCallback.onDeleteClicked(carts[position].cartId);
                    }
                });

                builder.setNegativeButton(Html.fromHtml("<font color='#f4454c'>No</font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                builder.show();
            }
        });
        holder.imgViewPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                carts[position].quantity = carts[position].quantity + 1;
                holder.txtViewQty.setText(Integer.toString(carts[position].quantity));
            }
        });
        holder.imgViewMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (carts[position].quantity > 1) {
                    carts[position].quantity = carts[position].quantity - 1;
                    holder.txtViewQty.setText(Integer.toString(carts[position].quantity));
                }
            }
        });
        holder.txtViewPrice.setText("Rs." + Double.toString(carts[position].productMRP * carts[position].quantity));
        holder.txtViewMrp.setText("Rs. " + Double.toString(carts[position].productPrice * carts[position].quantity));
        holder.txtViewPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        holder.btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onUpdateClicked(carts[position].cartId, carts[position].quantity);
            }
        });
        holder.txtViewSize.setText(carts[position].sizeName);
        holder.layoutColor.setBackgroundColor(Color.parseColor(carts[position].colorValue));
        if (carts[position].stock == 0) {
            holder.txtViewCartStatus.setText("Out Of Stock");
        } else if (carts[position].quantity > carts[position].stock) {
            holder.txtViewCartStatus.setText("Low Your Quantity");
        } else {
            holder.txtViewCartStatus.setText("");
        }
    }

    @Override
    public int getItemCount() {
        return carts.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_cross)
        ImageView imgViewCross;
        @BindView(R.id.img_view_product)
        ImageView imgViewProduct;
        @BindView(R.id.txt_view_product_title)
        TextView txtViewProductTitle;
        @BindView(R.id.txt_view_qty)
        TextView txtViewQty;
        @BindView(R.id.img_view_plus)
        ImageView imgViewPlus;
        @BindView(R.id.img_view_minus)
        ImageView imgViewMinus;
        @BindView(R.id.txt_view_price)
        TextView txtViewPrice;
        @BindView(R.id.txt_view_mrp)
        TextView txtViewMrp;
        @BindView(R.id.btn_update)
        Button btnUpdate;
        @BindView(R.id.layout_color)
        View layoutColor;
        @BindView(R.id.txt_view_size)
        TextView txtViewSize;
        @BindView(R.id.txt_view_cart_status)
        TextView txtViewCartStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
