package n.webinfotech.bibibobi.presentation.presenters;

public interface PaymentActivityPresenter {
    void getAccessToken();
    void initiatePayment(String accessToken, String amount, String transactionId, String env);
    void requestPayment(String accessToken, String id);
    void placeOrder(int addressId);
    void setTransactionId(int orderId, String transactionId);
    void updateTransactionId(int orderId, String transactionId, String paymentId);
    interface View {
        void showLoader();
        void hideLoader();
        void setAccessToken(String accessToken);
        void setPaymentId(String id);
        void setOrderId(String id);
        void setProductOrderId(int id);
        void onSetTransactionIdSuccess();
        void goToOrderPlacedResponseActivity();
    }
}
