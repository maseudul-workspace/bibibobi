package n.webinfotech.bibibobi.presentation.presenters;

import n.webinfotech.bibibobi.presentation.ui.adapters.CityListDialogAdapter;
import n.webinfotech.bibibobi.presentation.ui.adapters.StateListDialogAdapter;

public interface AddAddressPresenter {
    void fetchStateList();
    void fetchCityList();
    void addAddress(String email,
                    String mobile,
                    String pin,
                    String address);

    interface View {
        void loadStateAdapter(StateListDialogAdapter stateListDialogAdapter);
        void loadCitiesAdapter(CityListDialogAdapter cityListDialogAdapter);
        void showLoader();
        void hideLoader();
        void setStateName(String stateName);
        void setCityName(String cityName);
        void hideStateRecyclerView();
        void hideCityRecyclerView();
        void onAddAddressSuccess();
        void goToLoginActivity();
    }

}
