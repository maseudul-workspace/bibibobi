package n.webinfotech.bibibobi.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import n.webinfotech.bibibobi.AndroidApplication;
import n.webinfotech.bibibobi.R;
import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.GetAccessTokenInteractor;
import n.webinfotech.bibibobi.domain.interactors.InitiatePaymentInteractor;
import n.webinfotech.bibibobi.domain.interactors.PlaceOrderInteractor;
import n.webinfotech.bibibobi.domain.interactors.RequestPaymentInteractor;
import n.webinfotech.bibibobi.domain.interactors.SetTransactionIdInteractor;
import n.webinfotech.bibibobi.domain.interactors.UpdateTransactionIdInteractor;
import n.webinfotech.bibibobi.domain.interactors.impl.GetAccessTokenInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.InitiatePaymentInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.PlaceOrderInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.RequestPaymentInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.SetTransactionIdInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.UpdateTransactionIdInteractorImpl;
import n.webinfotech.bibibobi.domain.models.GatewayOrder;
import n.webinfotech.bibibobi.domain.models.OrderPlaceData;
import n.webinfotech.bibibobi.domain.models.PaymentRequestResponse;
import n.webinfotech.bibibobi.domain.models.User;
import n.webinfotech.bibibobi.presentation.presenters.PaymentActivityPresenter;
import n.webinfotech.bibibobi.presentation.presenters.base.AbstractPresenter;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;
import n.webinfotech.bibibobi.repository.PaymentRepositoryImpl;


public class PaymentActivityPresenterImpl extends AbstractPresenter implements PaymentActivityPresenter,
                                                                                GetAccessTokenInteractor.Callback,
                                                                                InitiatePaymentInteractor.Callback,
                                                                                RequestPaymentInteractor.Callback,
                                                                                PlaceOrderInteractor.Callback,
                                                                                SetTransactionIdInteractor.Callback,
                                                                                UpdateTransactionIdInteractor.Callback
{

    Context mContext;
    PaymentActivityPresenter.View mView;
    GetAccessTokenInteractorImpl getAccessTokenInteractor;
    InitiatePaymentInteractorImpl initiatePaymentInteractor;
    RequestPaymentInteractorImpl requestPaymentInteractor;
    AndroidApplication androidApplication;
    PlaceOrderInteractorImpl placeOrderInteractor;
    SetTransactionIdInteractorImpl setTransactionIdInteractor;
    UpdateTransactionIdInteractorImpl updateTransactionIdInteractor;

    public PaymentActivityPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void getAccessToken() {
        getAccessTokenInteractor = new GetAccessTokenInteractorImpl(mExecutor, mMainThread, new PaymentRepositoryImpl(), this, mContext.getResources().getString(R.string.Client_Id), mContext.getResources().getString(R.string.Client_Secret), "client_credentials");
        getAccessTokenInteractor.execute();
    }

    @Override
    public void initiatePayment(String accessToken, String amount, String transactionId, String env) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        initiatePaymentInteractor = new InitiatePaymentInteractorImpl(mExecutor, mMainThread, new PaymentRepositoryImpl(), this, accessToken, user.name, "razthemonster@gmail.com", user.mobile, amount, "", "INR", transactionId, "https://test.instamojo.com/integrations/android/redirect/", env);
        initiatePaymentInteractor.execute();
    }

    @Override
    public void requestPayment(String accessToken, String id) {
        requestPaymentInteractor = new RequestPaymentInteractorImpl(mExecutor, mMainThread, new PaymentRepositoryImpl(), this, accessToken, id);
        requestPaymentInteractor.execute();
    }

    @Override
    public void placeOrder(int addressId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        placeOrderInteractor = new PlaceOrderInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, 2, addressId);
        placeOrderInteractor.execute();
    }

    @Override
    public void setTransactionId(int orderId, String transactionId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        setTransactionIdInteractor = new SetTransactionIdInteractorImpl(mExecutor,mMainThread, new AppRepositoryImpl(), this, user.apiToken, orderId, transactionId);
        setTransactionIdInteractor.execute();
    }

    @Override
    public void updateTransactionId(int orderId, String transactionId, String paymentId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        updateTransactionIdInteractor = new UpdateTransactionIdInteractorImpl(mExecutor,  mMainThread, new AppRepositoryImpl(), this, user.apiToken, orderId, transactionId, paymentId);
        updateTransactionIdInteractor.execute();
    }

    @Override
    public void onGettingAccessTokenSuccess(String accessToken) {
        mView.setAccessToken(accessToken);
    }

    @Override
    public void onGettingAccessTokenFail(String errorMsg) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onInitiatePaymentSuccess(GatewayOrder gatewayOrder) {
        try {
            mView.setPaymentId(gatewayOrder.id);
        } catch (Exception e) {
            Toast.makeText(mContext, "Something went wrong", Toast.LENGTH_SHORT).show();
            mView.hideLoader();
        }
    }

    @Override
    public void onInitiatePaymentFail(String errorMsg) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPaymentSuccess(PaymentRequestResponse paymentRequestResponse) {
        mView.setOrderId(paymentRequestResponse.orderId);
    }

    @Override
    public void onRequestPaymentFail(String errorMsg) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPlaceOrderSuccess(OrderPlaceData orderPlaceData) {
        mView.setProductOrderId(orderPlaceData.orderId);
    }

    @Override
    public void onPlaceOrderFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSetTransactionIdSuccess() {
        mView.onSetTransactionIdSuccess();
    }

    @Override
    public void onSetTransactionIdFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUpdateTransactionIdSuccess() {
        mView.goToOrderPlacedResponseActivity();
    }

    @Override
    public void onUpdateTransactionIdFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
