package n.webinfotech.bibibobi.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.bibibobi.R;
import n.webinfotech.bibibobi.domain.models.Product;
import n.webinfotech.bibibobi.util.GlideHelper;

public class ProductListVerticalAdapter extends RecyclerView.Adapter<ProductListVerticalAdapter.ViewHolder> {

    public interface Callback {
        void onProductClicked(int productId);
        void onAddToCartClicked(int productId);
        void addToWishlist(int productId, int position);
        void removeFromWishList(int productId, int position);
    }

    Context mContext;
    Product[] products;
    Callback mCallback;

    public ProductListVerticalAdapter(Context mContext, Product[] products, Callback callback) {
        this.mContext = mContext;
        this.products = products;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_product_list_vertical, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GlideHelper.setImageView(mContext, holder.imgViewProduct, mContext.getResources().getString(R.string.base_url) + "/images/product/thumb/" + products[position].mainImage);
        holder.txtViewProductTitle.setText(products[position].name);
        holder.txtViewSellingPrice.setText("Rs. " + products[position].minPrice);
        holder.cardProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onProductClicked(products[position].id);
            }
        });
        holder.cardAddCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onAddToCartClicked(products[position].id);
            }
        });
        if (products[position].isWishListPresent) {
            holder.imgViewHeartOutline.setVisibility(View.GONE);
            holder.imgViewHeartFill.setVisibility(View.VISIBLE);
        } else {
            holder.imgViewHeartOutline.setVisibility(View.VISIBLE);
            holder.imgViewHeartFill.setVisibility(View.GONE);
        }
        holder.imgViewHeartFill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.removeFromWishList(products[position].id, position);
            }
        });
        holder.imgViewHeartOutline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.addToWishlist(products[position].id, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return products.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_product)
        ImageView imgViewProduct;
        @BindView(R.id.txt_view_product_title)
        TextView txtViewProductTitle;
        @BindView(R.id.txt_view_selling_price)
        TextView txtViewSellingPrice;
        @BindView(R.id.card_product)
        View cardProduct;
        @BindView(R.id.card_add_cart)
        View cardAddCart;
        @BindView(R.id.img_view_heart_fill)
        ImageView imgViewHeartFill;
        @BindView(R.id.img_view_heart_outline)
        ImageView imgViewHeartOutline;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateData(Product[] products) {
        this.products = products;
    }

    public void onAddToWishlistSuccess(int position) {
        this.products[position].isWishListPresent = true;
        notifyItemChanged(position);
    }

    public void onRemoveFromWishlistSuccess(int position) {
        this.products[position].isWishListPresent = false;
        notifyItemChanged(position);
    }

}
