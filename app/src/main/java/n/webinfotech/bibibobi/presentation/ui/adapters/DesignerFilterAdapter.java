package n.webinfotech.bibibobi.presentation.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.bibibobi.R;
import n.webinfotech.bibibobi.domain.models.Designer;

/**
 * Created by Raj on 02-09-2019.
 */

public class DesignerFilterAdapter extends RecyclerView.Adapter<DesignerFilterAdapter.ViewHolder> {

    public interface Callback {
        void onDesignerSelect(int brandId);
        void onDesignerNotSelect(int brandId);
    }

    Context mContext;
    Designer[] designers;
    Callback mCallback;

    public DesignerFilterAdapter(Context mContext, Designer[] designers, Callback callback) {
        this.mContext = mContext;
        this.designers = designers;
        this.mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_designer_filter, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.checkBox.setText(designers[i].designerName);
        viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mCallback.onDesignerSelect(designers[i].designerId);
                } else {
                    mCallback.onDesignerNotSelect(designers[i].designerId);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return designers.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.recycler_view_designer_by_checkbox)
        CheckBox checkBox;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
