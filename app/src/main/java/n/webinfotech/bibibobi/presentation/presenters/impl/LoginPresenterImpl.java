package n.webinfotech.bibibobi.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import n.webinfotech.bibibobi.AndroidApplication;
import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.CheckLoginInteractor;
import n.webinfotech.bibibobi.domain.interactors.impl.CheckLoginInteractorImpl;
import n.webinfotech.bibibobi.domain.models.User;
import n.webinfotech.bibibobi.presentation.presenters.LoginPresenter;
import n.webinfotech.bibibobi.presentation.presenters.base.AbstractPresenter;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class LoginPresenterImpl extends AbstractPresenter implements LoginPresenter, CheckLoginInteractor.Callback {

    Context mContext;
    LoginPresenter.View mView;
    CheckLoginInteractorImpl checkLoginInteractor;
    AndroidApplication androidApplication;

    public LoginPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void checkLogin(String email, String password) {
        checkLoginInteractor = new CheckLoginInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, email, password);
        checkLoginInteractor.execute();
    }

    @Override
    public void onCheckLoginSuccess(User user) {
        mView.hideLoader();
        Toast.makeText(mContext, "Login Successfull", Toast.LENGTH_SHORT).show();
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setUserInfo(mContext, user);
        mView.goToMainActivity();
    }

    @Override
    public void onCheckLoginFail(String errorMsg) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
