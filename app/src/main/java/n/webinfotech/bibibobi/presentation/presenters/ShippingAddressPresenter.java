package n.webinfotech.bibibobi.presentation.presenters;

import n.webinfotech.bibibobi.presentation.ui.adapters.ShippingAddressAdapter;

public interface ShippingAddressPresenter {
    void fetchShippingAddress();
    interface View {
        void loadAdapter(ShippingAddressAdapter adapter);
        void showLoader();
        void hideLoader();
        void hideAddressRecyclerView();
        void goToAddressEditActivity(int id);
        void goToLoginActivity();
    }
}
