package n.webinfotech.bibibobi.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import n.webinfotech.bibibobi.R;
import n.webinfotech.bibibobi.domain.executors.impl.ThreadExecutor;
import n.webinfotech.bibibobi.presentation.presenters.CartsPresenter;
import n.webinfotech.bibibobi.presentation.presenters.impl.CartsPresenterImpl;
import n.webinfotech.bibibobi.presentation.ui.adapters.CartAdapter;
import n.webinfotech.bibibobi.threading.MainThreadImpl;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class CartActivity extends AppCompatActivity implements CartsPresenter.View {

    @BindView(R.id.recycler_view_cart)
    RecyclerView recyclerViewCart;
    @BindView(R.id.txt_view_cart_total)
    TextView txtViewCartTotal;
    @BindView(R.id.bottom_layout)
    View bottomLayout;
    CartsPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    String amount;
    @BindView(R.id.txt_cart_count)
    TextView txtViewCartCount;
    @BindView(R.id.btn_select_address)
    Button btnSelectAddress;
    @BindView(R.id.txt_view_cart_total_price)
    TextView txtViewCartTotalPrice;
    @BindView(R.id.txt_view_total_mrp)
    TextView txtViewTotalMrp;
    @BindView(R.id.txt_view_total_selling_price)
    TextView txtViewTotalSellingPrice;
    @BindView(R.id.txt_view_total_shipping_charges)
    TextView txtViewTotalShippingCharges;
    @BindView(R.id.layout_cart_summary)
    View layoutCartSummary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
        getSupportActionBar().setTitle("My Cart");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void initialisePresenter() {
        mPresenter = new CartsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.hide();
    }

    @Override
    public void loadData(CartAdapter adapter, double cartTotal, int cartItem) {
        recyclerViewCart.setVisibility(View.VISIBLE);
        bottomLayout.setVisibility(View.VISIBLE);
        txtViewCartTotal.setText("Rs. " + Double.toString(cartTotal));
        recyclerViewCart.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewCart.setAdapter(adapter);
        amount = Double.toString(cartTotal);
        txtViewCartCount.setText("Total Item: " + cartItem);
    }

    @Override
    public void hideCartRecyclerView() {
        recyclerViewCart.setVisibility(View.GONE);
        bottomLayout.setVisibility(View.GONE);
        txtViewCartCount.setText("");
        layoutCartSummary.setVisibility(View.GONE);
    }

    @Override
    public void goToLoginActivity() {
        Toast.makeText(this, "Your session expired !! Please login again", Toast.LENGTH_LONG).show();
        Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(loginIntent);
    }

    @Override
    public void disableCartButton(boolean isOutOfStock) {
        btnSelectAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOutOfStock) {
                    Toasty.warning(getApplicationContext(), "Please Update Your Cart", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(getApplicationContext(), DeliveryAddressActivity.class);
                    intent.putExtra("amount", amount);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void setCartSummary(int totalItems, double totalPrice, double sellingPrice, int totalShippingCharges, double total) {
        layoutCartSummary.setVisibility(View.VISIBLE);
        txtViewCartCount.setText(Integer.toString(totalItems));
        txtViewTotalMrp.setText("₹" + totalPrice);
        txtViewTotalSellingPrice.setText("₹" + sellingPrice);
        txtViewTotalShippingCharges.setText("₹" + totalShippingCharges);
        txtViewCartTotalPrice.setText("₹" + total);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchCartProducts();
        showLoader();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
