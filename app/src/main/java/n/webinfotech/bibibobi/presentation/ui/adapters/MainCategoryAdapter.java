package n.webinfotech.bibibobi.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.bibibobi.R;
import n.webinfotech.bibibobi.domain.models.MainCategory;
import n.webinfotech.bibibobi.util.GlideHelper;

public class MainCategoryAdapter extends RecyclerView.Adapter<MainCategoryAdapter.ViewHolder>{

    public interface Callback {
        void onMainCategoryClicked(int id, String image, String title, ImageView imageView, boolean isTraditional);
    }

    Context mContext;
    ArrayList<MainCategory> mainCategories;
    Callback mCallback;

    public MainCategoryAdapter(Context mContext, ArrayList<MainCategory> mainCategories, Callback mCallback) {
        this.mContext = mContext;
        this.mainCategories = mainCategories;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_main_category, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewMainCategory, mainCategories.get(position).categoryImage, 30);
        holder.txtViewMainCategory.setText(mainCategories.get(position).categoryName);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onMainCategoryClicked(mainCategories.get(position).categoryId, mainCategories.get(position).categoryImage, mainCategories.get(position).categoryName, holder.imgViewMainCategory, mainCategories.get(position).isTraditional);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mainCategories.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_main_category)
        ImageView imgViewMainCategory;
        @BindView(R.id.txt_view_main_category)
        TextView txtViewMainCategory;
        @BindView(R.id.main_layout)
        View mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
