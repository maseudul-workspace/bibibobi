package n.webinfotech.bibibobi.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import n.webinfotech.bibibobi.AndroidApplication;
import n.webinfotech.bibibobi.R;
import n.webinfotech.bibibobi.domain.executors.impl.ThreadExecutor;
import n.webinfotech.bibibobi.domain.models.Colors;
import n.webinfotech.bibibobi.domain.models.Designer;
import n.webinfotech.bibibobi.domain.models.SecondCategory;
import n.webinfotech.bibibobi.domain.models.Size;
import n.webinfotech.bibibobi.domain.models.User;
import n.webinfotech.bibibobi.presentation.presenters.ProductListPresenter;
import n.webinfotech.bibibobi.presentation.presenters.impl.ProductListPresenterImpl;
import n.webinfotech.bibibobi.presentation.ui.adapters.ProductListVerticalAdapter;
import n.webinfotech.bibibobi.presentation.ui.dialogs.FilterByDialog;
import n.webinfotech.bibibobi.presentation.ui.dialogs.SortByDialog;
import n.webinfotech.bibibobi.threading.MainThreadImpl;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ProductListActivity extends AppCompatActivity implements SortByDialog.Callback, ProductListPresenter.View, FilterByDialog.Callback {

    @BindView(R.id.recycler_view_product_list_vertical)
    RecyclerView recyclerViewProductList;
    @BindView(R.id.pagination_progress)
    View paginationProgressBar;
    SortByDialog sortByDialog;
    ProductListPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int totalPage = 1;
    int pageNo = 1;
    int categoryId;
    LinearLayoutManager layoutManager;
    boolean isFirstLoad = true;
    FilterByDialog filterByDialog;
    ArrayList<Integer> sizeIds;
    ArrayList<Integer> designerIds;
    ArrayList<Integer> colorIds;
    int minPrice = 0;
    int maxPrice = 0;
    int sortBy = 1;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txt_cart_count)
    TextView txtViewCartCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        ButterKnife.bind(this);
        sizeIds = new ArrayList<>();
        designerIds = new ArrayList<>();
        colorIds = new ArrayList<>();
        categoryId = getIntent().getIntExtra("categoryId", 0);
        setUpSortByDialog();
        initialisePresenter();
        setUpProgressDialog();
        setFilterDialog();
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Product List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void initialisePresenter() {
        mPresenter = new ProductListPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setFilterDialog() {
        filterByDialog = new FilterByDialog(this, this, this);
        filterByDialog.setUpDialogView();
    }

    public void setUpSortByDialog() {
        sortByDialog = new SortByDialog(this, this, this);
        sortByDialog.setUpDialogView();
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.layout_sort_by) void onSortByLayoutClicked() {
        sortByDialog.showDialog();
    }

    @Override
    public void onSortApplyBtnClicked(int sort) {
        this.sortBy = sort;
        mPresenter.fetchProductListWithoutFilters(categoryId, designerIds, sizeIds, colorIds, minPrice, maxPrice, sortBy, pageNo, "refresh");
        showLoader();
    }

    @Override
    public void loadProducts(ProductListVerticalAdapter adapter, int totalPage) {
        this.totalPage = totalPage;
        recyclerViewProductList.setVisibility(View.VISIBLE);
        layoutManager = new GridLayoutManager(this, 2);
        recyclerViewProductList.setLayoutManager(layoutManager);
        recyclerViewProductList.setAdapter(adapter);
        recyclerViewProductList.setNestedScrollingEnabled(false);
        recyclerViewProductList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        showPaginationProgressbar();
                        if(isFirstLoad){
                            mPresenter.fetchProductListWithFilters(categoryId, pageNo, "");
                        }
                        else{
                            mPresenter.fetchProductListWithoutFilters(categoryId, designerIds, sizeIds, colorIds, minPrice, maxPrice, sortBy, pageNo, "");
                        }
                    }
                }
            }
        });
    }

    @Override
    public void loadFilters(SecondCategory[] secondCategories, Colors[] colors, Size[] sizes, Designer[] designers) {
        filterByDialog.setData(secondCategories, colors, sizes, designers);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void showPaginationProgressbar() {
        paginationProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hidePaginationProgressbar() {
        paginationProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void hideProductsRecyclerView() {
        recyclerViewProductList.setVisibility(View.GONE);
    }

    @Override
    public void goToProductDetails(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", productId);
        startActivity(intent);
    }

    @Override
    public void loadCartCount(int cartCount) {
        if (cartCount == 0) {
            txtViewCartCount.setText("");
        } else {
            txtViewCartCount.setText(Integer.toString(cartCount));
        }
    }

    @Override
    public void goToLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void onFiltersApplied(ArrayList<Integer> sizeIds, ArrayList<Integer> designerIds, ArrayList<Integer> colorIds, int minPrice, int maxPrice) {
        isFirstLoad = false;
        this.sizeIds = sizeIds;
        this.designerIds = designerIds;
        this.colorIds = colorIds;
        this.maxPrice = maxPrice;
        this.minPrice = minPrice;
        mPresenter.fetchProductListWithoutFilters(categoryId, designerIds, sizeIds, colorIds, minPrice, maxPrice, sortBy, pageNo, "refresh");
        showLoader();
    }

    @Override
    public void onCategoryFilterClicked(int categoryId) {
        Intent intent = new Intent(this, ProductListActivity.class);
        intent.putExtra("categoryId", categoryId);
        startActivity(intent);
    }

    @OnClick(R.id.layout_filter_by) void onLayoutFilterByClicked() {
        filterByDialog.showDialog();
    }

    @Override
    protected void onResume() {
        super.onResume();
        isScrolling = false;
        totalPage = 1;
        pageNo = 1;
        mPresenter.fetchCartCount();
        showLoader();
        mPresenter.fetchProductListWithFilters(categoryId, pageNo, "refresh");
    }

    @OnClick(R.id.layout_cart) void onCartClicked() {
        if (checkLogin()) {
            AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
            User user = androidApplication.getUserInfo(this);
            if (user != null) {
                Intent cartIntent = new Intent(this, CartActivity.class);
                startActivity(cartIntent);
            }
        } else {
            Toast.makeText(this, "Please Login", Toast.LENGTH_SHORT).show();
            goToLogin();
        }

    }

    public boolean checkLogin() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        User user = androidApplication.getUserInfo(this);
        if (user == null) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}