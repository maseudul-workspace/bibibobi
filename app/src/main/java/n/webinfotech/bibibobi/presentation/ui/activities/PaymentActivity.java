package n.webinfotech.bibibobi.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import n.webinfotech.bibibobi.R;
import n.webinfotech.bibibobi.domain.executors.impl.ThreadExecutor;
import n.webinfotech.bibibobi.presentation.presenters.PaymentActivityPresenter;
import n.webinfotech.bibibobi.presentation.presenters.impl.PaymentActivityPresenterImpl;
import n.webinfotech.bibibobi.threading.MainThreadImpl;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import com.instamojo.android.Instamojo;

import java.util.HashMap;
import java.util.UUID;

public class PaymentActivity extends AppCompatActivity implements Instamojo.InstamojoPaymentCallback, PaymentActivityPresenter.View {

    private static final HashMap<Instamojo.Environment, String> env_options = new HashMap<>();

    static {
        env_options.put(Instamojo.Environment.TEST, "https://test.instamojo.com/");
        env_options.put(Instamojo.Environment.PRODUCTION, "https://api.instamojo.com/");
    }

    private Instamojo.Environment mCurrentEnv = Instamojo.Environment.PRODUCTION;
    public ProgressDialog progressDialog;
    public PaymentActivityPresenterImpl mPresenter;
    String amount;
    int addressId;
    String transactionId;
    String accessToken;
    int productOrderId;
    String gatewayOrderId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        amount = getIntent().getStringExtra("amount");
        addressId = getIntent().getIntExtra("addressId", 0);
        Instamojo.getInstance().initialize(PaymentActivity.this, mCurrentEnv);
        setProgressDialog();
        initialisePresenter();
        mPresenter.placeOrder(addressId);
        showLoader();
    }

    public void initialisePresenter() {
        mPresenter = new PaymentActivityPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void onInstamojoPaymentComplete(String s, String s1, String s2, String s3) {
        mPresenter.updateTransactionId(productOrderId, transactionId, s2);
    }

    @Override
    public void onPaymentCancelled() {
        Intent intent = new Intent(this, OrderPlaceResponseActivity.class);
        intent.putExtra("orderId", productOrderId);
        intent.putExtra("orderStatus", 2);
        intent.putExtra("transactionId", transactionId);
        intent.putExtra("paymentType", 1);
        intent.putExtra("amount", amount);
        startActivity(intent);
        finish();
    }

    @Override
    public void onInitiatePaymentFailure(String s) {
        Intent intent = new Intent(this, OrderPlaceResponseActivity.class);
        intent.putExtra("orderId", productOrderId);
        intent.putExtra("orderStatus", 2);
        intent.putExtra("transactionId", transactionId);
        intent.putExtra("paymentType", 1);
        intent.putExtra("amount", amount);
        startActivity(intent);
        finish();
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        transactionId = UUID.randomUUID().toString();
        mPresenter.initiatePayment(accessToken, amount, transactionId, mCurrentEnv.name());
    }

    @Override
    public void setPaymentId(String id) {
        gatewayOrderId = id;
        mPresenter.setTransactionId(productOrderId, transactionId);
    }

    @Override
    public void setOrderId(String id) {
        hideLoader();
        Instamojo.getInstance().initiatePayment(this, id, this);
    }

    @Override
    public void setProductOrderId(int id) {
        productOrderId = id;
        mPresenter.getAccessToken();
    }

    @Override
    public void onSetTransactionIdSuccess() {
        mPresenter.requestPayment(accessToken, gatewayOrderId);
    }

    @Override
    public void goToOrderPlacedResponseActivity() {
        Intent intent = new Intent(this, OrderPlaceResponseActivity.class);
        intent.putExtra("orderId", productOrderId);
        intent.putExtra("orderStatus", 1);
        intent.putExtra("transactionId", transactionId);
        intent.putExtra("paymentType", 1);
        intent.putExtra("amount", amount);
        startActivity(intent);
        finish();
    }

}
