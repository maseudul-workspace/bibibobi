package n.webinfotech.bibibobi.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import n.webinfotech.bibibobi.AndroidApplication;
import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.AddToCartWithoutFilterInteractor;
import n.webinfotech.bibibobi.domain.interactors.AddToWishListInteractor;
import n.webinfotech.bibibobi.domain.interactors.FetchCartInteractor;
import n.webinfotech.bibibobi.domain.interactors.FetchWishListInteractor;
import n.webinfotech.bibibobi.domain.interactors.GetProductListWithFilterInteractor;
import n.webinfotech.bibibobi.domain.interactors.GetProductListWithoutFilterInteractor;
import n.webinfotech.bibibobi.domain.interactors.RemoveFromWishlistInteractor;
import n.webinfotech.bibibobi.domain.interactors.impl.AddToCartWithoutFilterInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.AddToWishListInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.FetchCartInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.FetchWishListInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.GetProductListWithFilterInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.GetProductListWithoutFilterInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.RemoveFromWishListInteractorImpl;
import n.webinfotech.bibibobi.domain.models.Cart;
import n.webinfotech.bibibobi.domain.models.Product;
import n.webinfotech.bibibobi.domain.models.ProductListData;
import n.webinfotech.bibibobi.domain.models.User;
import n.webinfotech.bibibobi.domain.models.WishlistProduct;
import n.webinfotech.bibibobi.presentation.presenters.ProductListPresenter;
import n.webinfotech.bibibobi.presentation.presenters.base.AbstractPresenter;
import n.webinfotech.bibibobi.presentation.ui.adapters.ProductListVerticalAdapter;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class ProductListPresenterImpl extends AbstractPresenter implements  ProductListPresenter,
                                                                            ProductListVerticalAdapter.Callback,
                                                                            GetProductListWithFilterInteractor.Callback,
                                                                            GetProductListWithoutFilterInteractor.Callback,
                                                                            FetchCartInteractor.Callback,
                                                                            AddToWishListInteractor.Callback,
                                                                            FetchWishListInteractor.Callback,
                                                                            RemoveFromWishlistInteractor.Callback,
                                                                            AddToCartWithoutFilterInteractor.Callback{

    Context mContext;
    ProductListPresenter.View mView;
    Product[] newProductLists;
    GetProductListWithFilterInteractorImpl getProductListWithFilterInteractor;
    ProductListVerticalAdapter adapter;
    GetProductListWithoutFilterInteractorImpl getProductListWithoutFilterInteractor;
    FetchCartInteractorImpl fetchCartInteractor;
    AddToWishListInteractorImpl addToWishListInteractor;
    int position;
    RemoveFromWishListInteractorImpl removeFromWishListInteractor;
    FetchWishListInteractorImpl fetchWishListInteractor;
    AddToCartWithoutFilterInteractorImpl addToCartWithoutFilterInteractor;

    public ProductListPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchProductListWithFilters(int categoryId, int page, String type) {
        if (type.equals("refresh")) {
            newProductLists = null;
        }
        getProductListWithFilterInteractor = new GetProductListWithFilterInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, categoryId, page);
        getProductListWithFilterInteractor.execute();
    }

    @Override
    public void fetchProductListWithoutFilters(int secondCategory, ArrayList<Integer> designerIds, ArrayList<Integer> sizeIds, ArrayList<Integer> colorIds, int priceFrom, int priceTo, int sortBy, int pageNo, String type) {
        if (type.equals("refresh")) {
            newProductLists = null;
        }
        getProductListWithoutFilterInteractor = new GetProductListWithoutFilterInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, secondCategory, designerIds, sizeIds, colorIds, priceFrom, priceTo, sortBy, pageNo);
        getProductListWithoutFilterInteractor.execute();
    }

    @Override
    public void fetchCartCount() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        if (user != null) {
            fetchCartInteractor = new FetchCartInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId);
            fetchCartInteractor.execute();
        }
    }

    @Override
    public void fetchWishlist() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        if (user != null) {
            fetchWishListInteractor = new FetchWishListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId);
            fetchWishListInteractor.execute();
        }
    }

    @Override
    public void onProductClicked(int productId) {
        mView.goToProductDetails(productId);
    }

    @Override
    public void onAddToCartClicked(int productId) {
        if (checkLogin()) {
            AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
            User user = androidApplication.getUserInfo(mContext);
            addToCartWithoutFilterInteractor = new AddToCartWithoutFilterInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, productId);
            addToCartWithoutFilterInteractor.execute();
            mView.showLoader();
        } else {
            Toast.makeText(mContext, "PLease LOgin", Toast.LENGTH_SHORT).show();
//            mView.goToLogin();
        }
    }

    @Override
    public void addToWishlist(int productId, int position) {
        mView.showLoader();
        this.position = position;
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        if (user != null) {
            addToWishListInteractor = new AddToWishListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(),  this, user.apiToken, user.userId, productId);
            addToWishListInteractor.execute();
        }
    }

    @Override
    public void removeFromWishList(int productId, int position) {
        mView.showLoader();
        this.position = position;
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        WishlistProduct[] wishlistProducts = androidApplication.getWishList(mContext);
        User user = androidApplication.getUserInfo(mContext);
        int wishListId = 0;
        for (int i = 0; i < wishlistProducts.length; i++) {
            if (wishlistProducts[i].productId == productId) {
                wishListId = wishlistProducts[i].id;
                break;
            }
        }
        removeFromWishListInteractor = new RemoveFromWishListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, wishListId);
        removeFromWishListInteractor.execute();
    }

    @Override
    public void onGettingProductListWithFiltersSuccess(ProductListData productListData, int totalPage) {
        mView.hideLoader();
        mView.loadFilters(productListData.secondCategories, productListData.colors, productListData.sizes, productListData.designers);
        setProductList(productListData.products, totalPage);
    }

    public void setProductList(Product[] products, int totalPage) {
        if(products.length > 0){
            AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
            User user = androidApplication.getUserInfo(mContext);
            WishlistProduct[] wishlistProducts = androidApplication.getWishList(mContext);
            if (user != null && wishlistProducts != null && wishlistProducts.length != 0) {
                for (int i = 0; i < wishlistProducts.length; i++) {
                    for (int j = 0; j < products.length; j++) {
                        if (wishlistProducts[i].productId == products[j].id) {
                            products[j].isWishListPresent = true;
                            break;
                        }
                    }
                }
            }
            Product[] tempProductLists;
            Product[] productLists = products;
            tempProductLists = newProductLists;
            try {
                int len1 = tempProductLists.length;
                int len2 = productLists.length;
                newProductLists = new Product[len1 + len2];
                System.arraycopy(tempProductLists, 0, newProductLists, 0, len1);
                System.arraycopy(productLists, 0, newProductLists, len1, len2);
                adapter.updateData(newProductLists);
                adapter.notifyDataSetChanged();
                mView.hidePaginationProgressbar();
            } catch (NullPointerException e){
                newProductLists = productLists;
                adapter = new ProductListVerticalAdapter(mContext, productLists, this);
                mView.loadProducts(adapter, totalPage);
            }
        } else {
            Toast.makeText(mContext, "No products found", Toast.LENGTH_SHORT).show();
            mView.hideProductsRecyclerView();
        }
    }

    @Override
    public void onGettingProductListWithFiltersFail(String errorMsg) {
        mView.hideLoader();
        mView.hidePaginationProgressbar();
        mView.hideProductsRecyclerView();
    }

    @Override
    public void onGettingProductListWithoutFilterSuccess(Product[] products, int totalPage) {
        mView.hideLoader();
        setProductList(products, totalPage);
    }

    @Override
    public void onGettingProductListWithoutFilterFail(String errorMsg) {
        mView.hideLoader();
        mView.hidePaginationProgressbar();
        mView.hideProductsRecyclerView();
    }

    @Override
    public void onAddToCartSuccess() {
        mView.hideLoader();
        fetchCartCount();
        Toasty.success(mContext, "Added To Cart Successfully", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAddToCartFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.goToLogin();
        } else {
            Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFetchCartSuccess(Cart[] carts) {
        mView.loadCartCount(carts.length);
    }

    @Override
    public void onFetchCartFail(String errorMsg, int loginError) {

    }

    public boolean checkLogin() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onAddingWishListSuccess() {
        mView.hideLoader();
        fetchWishlist();
        Toast.makeText(mContext, "Added To Wishlist", Toast.LENGTH_SHORT).show();
        adapter.onAddToWishlistSuccess(position);
    }

    @Override
    public void onAddingWishListFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.goToLogin();
        } else {
            Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onWishlistFetchSuccess(WishlistProduct[] wishlistProducts) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishList(mContext, wishlistProducts);
    }

    @Override
    public void onWishListFetchFail(String errorMsg, int loginError) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishList(mContext, null);
    }

    @Override
    public void onWishlistRemovedSuccess() {
        mView.hideLoader();
        fetchWishlist();
        Toast.makeText(mContext, "Removed From Wishlist", Toast.LENGTH_SHORT).show();
        adapter.onRemoveFromWishlistSuccess(position);
    }

    @Override
    public void onWishListRemovedFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.goToLogin();
        } else {
            Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        }
    }

}
