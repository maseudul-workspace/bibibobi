package n.webinfotech.bibibobi.presentation.ui.activities;

import android.app.ActivityOptions;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Pair;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import n.webinfotech.bibibobi.AndroidApplication;
import n.webinfotech.bibibobi.domain.executors.impl.ThreadExecutor;
import n.webinfotech.bibibobi.domain.models.MainCategory;
import n.webinfotech.bibibobi.domain.models.ProductImage;
import n.webinfotech.bibibobi.domain.models.User;
import n.webinfotech.bibibobi.presentation.presenters.MainPresenter;
import n.webinfotech.bibibobi.presentation.presenters.impl.MainPresenterImpl;
import n.webinfotech.bibibobi.presentation.ui.adapters.MainCategoryAdapter;
import n.webinfotech.bibibobi.presentation.ui.adapters.ProductListGridAdapter;
import n.webinfotech.bibibobi.presentation.ui.adapters.ProductsHorizontalAdapter;
import n.webinfotech.bibibobi.R;
import n.webinfotech.bibibobi.presentation.ui.adapters.SlidingImagesAdapter;
import n.webinfotech.bibibobi.domain.models.Images;
import n.webinfotech.bibibobi.threading.MainThreadImpl;
import n.webinfotech.bibibobi.util.GlideHelper;

public class MainActivity extends AppCompatActivity implements MainCategoryAdapter.Callback, MainPresenter.View {

    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.dots_indicator_layout)
    LinearLayout dotsLayout;
    @BindView(R.id.recycler_view_men_traditional)
    RecyclerView recyclerViewMenTraditional;
    @BindView(R.id.recycler_view_women_traditional)
    RecyclerView recyclerViewWomenTraditional;
    @BindView(R.id.recycler_view_special_products)
    RecyclerView recyclerViewSpecialProducts;
    ArrayList<Images> images;
    @BindView(R.id.navigation)
    BottomNavigationView bottomNavigationView;
    @BindView(R.id.img_view_promotion_image_1)
    ImageView imgViewPromotion1;
    @BindView(R.id.img_view_promotion_image_2)
    ImageView imgViewPromotion2;
    @BindView(R.id.recycler_view_main_category)
    RecyclerView recyclerViewMainCategory;
    @BindView(R.id.recycler_view_random_products)
    RecyclerView recyclerViewRandomProducts;
    MainPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        getUserInfo();
        setBottomNavigationView();
        setRecyclerViewMainCategory();
        initialisePresenter();
        mPresenter.loadData();
    }

    public void initialisePresenter() {
        mPresenter = new MainPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void getUserInfo() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        User user = androidApplication.getUserInfo(this);
        if (user != null) {
//            Log.e("LogMsg", "API Token: " + user.apiToken);
//            Log.e("LogMsg", "User Id:" + user.userId);
        }
    }

    public void setBottomNavigationView() {
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.action_cart:
                        if (checkLogin()) {
                            Intent intent1 = new Intent(getApplicationContext(), CartActivity.class);
                            startActivity(intent1);
                        } else {
                            Toast.makeText(getApplicationContext(), "Please Login", Toast.LENGTH_SHORT).show();
                            goToLoginActivity();
                        }
                        break;
                    case R.id.action_user:
                        if (checkLogin()) {
                            Intent userIntent = new Intent(getApplicationContext(), UserActivity.class);
                            startActivity(userIntent);
                        } else {
                            Toast.makeText(getApplicationContext(), "Please Login", Toast.LENGTH_SHORT).show();
                            goToLoginActivity();
                        }
                        break;
                    case R.id.action_about:
                        Intent aboutIntent = new Intent(getApplicationContext(), AboutActivity.class);
                        startActivity(aboutIntent);
                        break;
                }
                return false;
            }
        });

    }

    public void setRecyclerViewMainCategory() {
        ArrayList<MainCategory> mainCategories = new ArrayList<>();
        MainCategory menCategory = new MainCategory(1, "Men", "https://assetscdn1.paytm.com/images/catalog/view_item/487684/1575630609905.jpg?imwidth=1600&impolicy=hq", false);
        MainCategory womenCategory = new MainCategory(2, "Women", "https://blog.grabon.in/wp-content/uploads/2017/06/womens-fashion.jpg", false);
        MainCategory womenTraditional = new MainCategory(3, "Women\nTraditional", "https://cdn0.weddingwire.in/articles/images/9/1/3/5/img_55319/assameseweddingdress-kunalkaushikofficial-giveyouroutfitamoderntouch.jpg", true);
        MainCategory menTraditional = new MainCategory(2, "Men\nTraditional", "https://cdn0.weddingwire.in/articles/images/9/0/3/5/img_55309/assameseweddingdress-camaraderiebondsphotography-strikeaposewithyourpartnerinthisoutfit.jpg", true);
        MainCategory giftItemTraditional = new MainCategory(5, "Gift\nItems", "http://www.giftitems.in/wp-content/uploads/2012/03/Corporate-Gifts-2.jpg", false);
        MainCategory handicraftCategory = new MainCategory(6, "Handicraft", "https://i1.wp.com/www.dfordelhi.in/wp-content/uploads/2016/07/handicraft.jpg?fit=1040%2C585&ssl=1", false);
        mainCategories.add(womenTraditional);
        mainCategories.add(menTraditional);
        mainCategories.add(menCategory);
        mainCategories.add(womenCategory);
        mainCategories.add(giftItemTraditional);
        mainCategories.add(handicraftCategory);
        MainCategoryAdapter mainCategoryAdapter = new MainCategoryAdapter(this, mainCategories, this::onMainCategoryClicked);
        recyclerViewMainCategory.setAdapter(mainCategoryAdapter);
        recyclerViewMainCategory.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
    }

    public void prepareDotsIndicator(int sliderPosition, int length){
        if(dotsLayout.getChildCount() > 0){
            dotsLayout.removeAllViews();
        }

        ImageView dots[] = new ImageView[length];
        for(int i = 0; i < length; i++){
            dots[i] = new ImageView(this);
            if(i == sliderPosition){
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.active_dot);
            }else{
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.inactive_dot);
            }

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(20, 20);
            layoutParams.setMargins(7, 0, 7, 0);
            dotsLayout.addView(dots[i], layoutParams);

        }
    }

    @Override
    public void onMainCategoryClicked(int id, String image, String title, ImageView imageView, boolean isTraditional) {
        Intent secondCategoryIntent = new Intent(this, SubCategoryActivity.class);
        secondCategoryIntent.putExtra("title", title);
        secondCategoryIntent.putExtra("id", id);
        secondCategoryIntent.putExtra("image", image);
        secondCategoryIntent.putExtra("isTraditional", isTraditional);
        Pair[] pairs = new Pair[1];
        pairs[0] = new Pair<View, String> (imageView, "imageTransition");
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this, pairs);
            startActivity(secondCategoryIntent, options.toBundle());
        } else {
            startActivity(secondCategoryIntent);
        }
    }

    @OnClick(R.id.search_layout) void onSearchLayoutClicked() {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);
    }

    @Override
    public void setData(ProductImage[] productImages, ProductsHorizontalAdapter womenTraditionalAdapter, ProductsHorizontalAdapter menTraditionalAdapter, ProductsHorizontalAdapter specialProductsAdapter, ProductListGridAdapter randomProducts, String promotionImage1, String promotionImage2) {
        recyclerViewMenTraditional.setAdapter(menTraditionalAdapter);
        recyclerViewMenTraditional.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        recyclerViewWomenTraditional.setAdapter(womenTraditionalAdapter);
        recyclerViewWomenTraditional.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        recyclerViewSpecialProducts.setAdapter(specialProductsAdapter);
        recyclerViewSpecialProducts.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        SlidingImagesAdapter slidingImagesAdapter = new SlidingImagesAdapter(this, productImages);
        viewPager.setAdapter(slidingImagesAdapter);
        prepareDotsIndicator(0, productImages.length);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                prepareDotsIndicator(position, productImages.length);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        recyclerViewRandomProducts.setAdapter(randomProducts);
        recyclerViewRandomProducts.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerViewRandomProducts.setNestedScrollingEnabled(false);
        GlideHelper.setImageView(this, imgViewPromotion1, getString(R.string.base_url) + "/images/slider/" + promotionImage1);
        GlideHelper.setImageView(this, imgViewPromotion2, getString(R.string.base_url) + "/images/slider/" + promotionImage2);
    }

    @Override
    public void goToProdyctDetails(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", productId);
        startActivity(intent);
    }

    @Override
    public void loadCartCount(int totalCount) {
        BadgeDrawable badge = bottomNavigationView.getOrCreateBadge(R.id.action_cart);
        badge.setNumber(totalCount);
        badge.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        badge.setBadgeTextColor(getResources().getColor(R.color.white));    }

    public boolean checkLogin() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        User user = androidApplication.getUserInfo(this);
        if (user == null) {
            return false;
        } else {
            return true;
        }
    }

    void goToLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchWishList();
        mPresenter.fetchCartCount();
    }

}
