package n.webinfotech.bibibobi.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.ButterKnife;
import butterknife.OnClick;
import n.webinfotech.bibibobi.R;

import android.content.Intent;
import android.os.Bundle;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Return Policy");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.layout_privacy_policy) void onPrivacyPolicyClicked() {
        Intent intent = new Intent(this, PrivacyPolicyActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.layout_return_policy) void onReturnPolicyActivity() {
        Intent intent = new Intent(this, ReturnPolicyActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.layout_about_us) void onAboutUsClicked() {
        Intent intent = new Intent(this, AboutAppActivity.class);
        startActivity(intent);
    }

}
