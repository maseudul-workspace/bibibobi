package n.webinfotech.bibibobi.presentation.presenters.impl;

import android.content.Context;

import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.GetSubcategoriesInteractor;
import n.webinfotech.bibibobi.domain.interactors.GetTraditionalSubcategoriesInteractor;
import n.webinfotech.bibibobi.domain.interactors.impl.GetSubcategoriesInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.GetTraditionalSubcategoriesInteractorImpl;
import n.webinfotech.bibibobi.domain.models.FirstCategory;
import n.webinfotech.bibibobi.presentation.presenters.SubcategoryPresenter;
import n.webinfotech.bibibobi.presentation.presenters.base.AbstractPresenter;
import n.webinfotech.bibibobi.presentation.ui.adapters.FirstCategoryAdapter;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class SubcategoryPresenterImpl extends AbstractPresenter implements SubcategoryPresenter, GetSubcategoriesInteractor.Callback, GetTraditionalSubcategoriesInteractor.Callback, FirstCategoryAdapter.Callback {

    Context mContext;
    SubcategoryPresenter.View mView;
    GetTraditionalSubcategoriesInteractorImpl getTraditionalSubcategoriesInteractor;
    GetSubcategoriesInteractorImpl getSubcategoriesInteractor;

    public SubcategoryPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchSubcategories(int mainCatId) {
        getSubcategoriesInteractor = new GetSubcategoriesInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, mainCatId);
        getSubcategoriesInteractor.execute();
    }

    @Override
    public void fetchTraditionalCategories(int traditionaType) {
        getTraditionalSubcategoriesInteractor = new GetTraditionalSubcategoriesInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, traditionaType);
        getTraditionalSubcategoriesInteractor.execute();
    }

    @Override
    public void onGettingSubcategoriesSuccess(FirstCategory[] firstCategories) {
        FirstCategoryAdapter firstCategoryAdapter = new FirstCategoryAdapter(mContext, firstCategories, this);
        mView.loadAdapter(firstCategoryAdapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingSubcategoriesFail(String errorMsg) {
        mView.hideLoader();
    }

    @Override
    public void onGettingTraditionalSubcategoriesSuccess(FirstCategory[] firstCategories) {
        FirstCategoryAdapter firstCategoryAdapter = new FirstCategoryAdapter(mContext, firstCategories, this);
        mView.loadAdapter(firstCategoryAdapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingTraditionaSubcategoriesFail(String errorMsg) {
        mView.hideLoader();
    }

    @Override
    public void onSubcategoryClicked(int id) {
        mView.goToProductListActivity(id);
    }
}
