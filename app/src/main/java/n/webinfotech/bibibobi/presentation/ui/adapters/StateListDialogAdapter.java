package n.webinfotech.bibibobi.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.bibibobi.R;
import n.webinfotech.bibibobi.domain.models.StateList;

public class StateListDialogAdapter extends RecyclerView.Adapter<StateListDialogAdapter.ViewHolder> {

    public interface Callback {
        void onStateClicked(int id, String state);
    }

    Context mContext;
    StateList[] states;
    Callback mCallback;

    public StateListDialogAdapter(Context mContext, StateList[] states, Callback mCallback) {
        this.mContext = mContext;
        this.states = states;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_state_list_dialog, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewState.setText(states[position].name);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onStateClicked(states[position].id, states[position].name);
            }
        });
    }

    @Override
    public int getItemCount() {
        return states.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_state)
        TextView txtViewState;
        @BindView(R.id.main_layout)
        View mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
