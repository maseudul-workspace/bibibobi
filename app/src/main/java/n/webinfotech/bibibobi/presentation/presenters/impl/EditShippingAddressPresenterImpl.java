package n.webinfotech.bibibobi.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import n.webinfotech.bibibobi.AndroidApplication;
import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.FetchCitiesInteractor;
import n.webinfotech.bibibobi.domain.interactors.FetchStateListInteractor;
import n.webinfotech.bibibobi.domain.interactors.GetAddressDetailsInteractor;
import n.webinfotech.bibibobi.domain.interactors.UpdateAddressInteractor;
import n.webinfotech.bibibobi.domain.interactors.impl.FetchCitiesInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.FetchStateListInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.GetAddressDetailsInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.UpdateAddressInteractorImpl;
import n.webinfotech.bibibobi.domain.models.Address;
import n.webinfotech.bibibobi.domain.models.Cities;
import n.webinfotech.bibibobi.domain.models.StateList;
import n.webinfotech.bibibobi.domain.models.User;
import n.webinfotech.bibibobi.presentation.presenters.EditShippingAddressPresenter;
import n.webinfotech.bibibobi.presentation.presenters.base.AbstractPresenter;
import n.webinfotech.bibibobi.presentation.ui.adapters.CityListDialogAdapter;
import n.webinfotech.bibibobi.presentation.ui.adapters.StateListDialogAdapter;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class EditShippingAddressPresenterImpl extends AbstractPresenter implements  EditShippingAddressPresenter,
                                                                                    GetAddressDetailsInteractor.Callback,
                                                                                    FetchStateListInteractor.Callback,
                                                                                    FetchCitiesInteractor.Callback,
                                                                                    StateListDialogAdapter.Callback,
                                                                                    CityListDialogAdapter.Callback,
                                                                                    UpdateAddressInteractor.Callback
{

    Context mContext;
    EditShippingAddressPresenter.View mView;
    GetAddressDetailsInteractorImpl getAddressDetailsInteractor;
    AndroidApplication androidApplication;
    int stateId = 3;
    int cityId = 1;
    FetchStateListInteractorImpl fetchStateListInteractor;
    StateListDialogAdapter stateListDialogAdapter;
    FetchCitiesInteractorImpl fetchCitiesInteractor;
    CityListDialogAdapter cityListDialogAdapter;
    UpdateAddressInteractorImpl updateAddressInteractor;
    int addressId;

    public EditShippingAddressPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void getShippingAddressDetails(int addressId) {
        this.addressId = addressId;
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User userInfo = androidApplication.getUserInfo(mContext);
        getAddressDetailsInteractor = new GetAddressDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId, addressId);
        getAddressDetailsInteractor.execute();
    }

    @Override
    public void updateAddress(String email, String mobile, String pin, String address) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User userInfo = androidApplication.getUserInfo(mContext);
        updateAddressInteractor = new UpdateAddressInteractorImpl(
                mExecutor,
                mMainThread,
                new AppRepositoryImpl(),
                this,
                userInfo.apiToken,
                userInfo.userId,
                addressId,
                email,
                mobile,
                stateId,
                cityId,
                pin,
                address
        );
        updateAddressInteractor.execute();
    }

    @Override
    public void fetchStateList() {
        fetchStateListInteractor = new FetchStateListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this);
        fetchStateListInteractor.execute();
    }

    @Override
    public void fetchCityList() {
        fetchCitiesInteractor = new FetchCitiesInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, stateId);
        fetchCitiesInteractor.execute();
    }

    @Override
    public void onGettingAddressDetailsSuccess(Address address) {
        stateId = address.stateId;
        cityId = address.cityId;
        fetchCityList();
        mView.setData(address);
        mView.hideLoader();
    }

    @Override
    public void onGettingAddressDetailsFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.goToLoginActivity();
        } else {
            Toast.makeText(mContext, errorMsg, loginError);
        }
    }

    @Override
    public void onGettingCitiesSuccess(Cities[] cities) {
        if (cities.length == 0) {
            mView.hideCityRecyclerView();
        } else {
            cityListDialogAdapter = new CityListDialogAdapter(mContext, cities, this::onCityClicked);
            mView.loadCitiesAdapter(cityListDialogAdapter);
        }
    }

    @Override
    public void onGettingCitiesFail() {
        mView.hideCityRecyclerView();
    }

    @Override
    public void onGettingStateListSuccess(StateList[] states) {
        if (states.length == 0) {
            mView.hideStateRecyclerView();
        } else {
            stateListDialogAdapter = new StateListDialogAdapter(mContext, states, this::onStateClicked);
            mView.loadStateAdapter(stateListDialogAdapter);
        }
    }

    @Override
    public void onGettingStateListFail() {
        mView.hideStateRecyclerView();
    }

    @Override
    public void onCityClicked(int id, String city) {
        mView.setCityName(city);
        cityId = id;
    }

    @Override
    public void onStateClicked(int id, String state) {
        stateId = id;
        mView.setStateName(state);
        fetchCityList();
    }

    @Override
    public void onUpdateAddressSuccess() {
        Toast.makeText(mContext, "Address Updated Successfully", Toast.LENGTH_SHORT).show();
        mView.hideLoader();
        mView.onUpdateAddressSuccess();
    }

    @Override
    public void onUpdateAddressFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.goToLoginActivity();
        } else {
            Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        }
    }
}
