package n.webinfotech.bibibobi.presentation.presenters;

import n.webinfotech.bibibobi.domain.models.ProductImage;
import n.webinfotech.bibibobi.presentation.ui.adapters.ProductListGridAdapter;
import n.webinfotech.bibibobi.presentation.ui.adapters.ProductsHorizontalAdapter;

public interface MainPresenter {
    void loadData();
    void fetchWishList();
    void fetchCartCount();
    interface View {
        void setData(ProductImage[] productImages, ProductsHorizontalAdapter womenTraditionalAdapter, ProductsHorizontalAdapter menTraditionalAdapter, ProductsHorizontalAdapter specialProductsAdapter, ProductListGridAdapter randomProducts, String promotionImage1, String promotionImage2);
        void goToProdyctDetails(int productId);
        void loadCartCount(int totalCount);
    }
}
