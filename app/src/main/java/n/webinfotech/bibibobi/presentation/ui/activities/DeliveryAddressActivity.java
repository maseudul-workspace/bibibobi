package n.webinfotech.bibibobi.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import n.webinfotech.bibibobi.R;
import n.webinfotech.bibibobi.domain.executors.impl.ThreadExecutor;
import n.webinfotech.bibibobi.presentation.presenters.DeliveryAddressPresenter;
import n.webinfotech.bibibobi.presentation.presenters.impl.DeliveryAddressPresenterImpl;
import n.webinfotech.bibibobi.presentation.ui.adapters.DeliveryAddressAdapter;
import n.webinfotech.bibibobi.presentation.ui.dialogs.PaymentMethodDialog;
import n.webinfotech.bibibobi.threading.MainThreadImpl;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class DeliveryAddressActivity extends AppCompatActivity implements DeliveryAddressPresenter.View, PaymentMethodDialog.Callback {

    @BindView(R.id.recycler_view_shipping_address)
    RecyclerView recyclerViewAddress;
    DeliveryAddressPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    @BindView(R.id.bottom_layout)
    View bottomLayout;
    PaymentMethodDialog paymentMethodDialog;
    String amount;
    int addressId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_address);
        ButterKnife.bind(this);
        amount = getIntent().getStringExtra("amount");
        setUpProgressDialog();
        initialisePresenter();
        setPaymentMethodDialog();
        getSupportActionBar().setTitle("Select Delivery Address");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void initialisePresenter() {
        mPresenter = new DeliveryAddressPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setPaymentMethodDialog() {
        paymentMethodDialog = new PaymentMethodDialog(this, this, this);
        paymentMethodDialog.setUpDialogView();
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadAdapter(DeliveryAddressAdapter adapter) {
        recyclerViewAddress.setVisibility(View.VISIBLE);
        bottomLayout.setVisibility(View.VISIBLE);
        recyclerViewAddress.setAdapter(adapter);
        recyclerViewAddress.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void hideAddressRecyclerView() {
        recyclerViewAddress.setVisibility(View.GONE);
        bottomLayout.setVisibility(View.GONE);
    }

    @Override
    public void goToAddressEditActivity(int id) {
        Intent intent = new Intent(this, EditAddressActivity.class);
        intent.putExtra("addressId", id);
        startActivity(intent);
    }

    @Override
    public void onAddressSelected(int id) {
        addressId = id;
    }

    @Override
    public void onOrderPlaceSuccess(int orderId) {
        Intent intent = new Intent(this, OrderPlaceResponseActivity.class);
        intent.putExtra("orderId", orderId);
        intent.putExtra("orderStatus", 1);
        intent.putExtra("transactionId", "");
        intent.putExtra("paymentType", 2);
        intent.putExtra("amount", amount);
        startActivity(intent);
        finish();
    }

    @Override
    public void goToLoginActivity() {
        Toast.makeText(this, "Your session expired !! Please login again", Toast.LENGTH_LONG).show();
        Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(loginIntent);
    }


    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchShippingAddress();
        showLoader();
    }

    @OnClick(R.id.layout_add_address) void onAddAddressClicked() {
        Intent intent = new Intent(this, AddShippingAddressActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.bottom_layout) void onBottomLayoutClicked() {
        paymentMethodDialog.showDialog();
    }

    @Override
    public void onPaymentMethodSelect(int id) {
        if (id == 2) {
            if (Double.valueOf(amount) > 200000) {
                Toast.makeText(this, "Amount should be less than Rs 200000 for online payment", Toast.LENGTH_LONG).show();
            } else {
                paymentMethodDialog.hideDialog();
                hideLoader();
                Intent intent = new Intent(this, PaymentActivity.class);
                intent.putExtra("amount", amount);
                intent.putExtra("addressId", addressId);
                startActivity(intent);
                finish();
            }
        } else {
            mPresenter.placeOrder(addressId);
            showLoader();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
