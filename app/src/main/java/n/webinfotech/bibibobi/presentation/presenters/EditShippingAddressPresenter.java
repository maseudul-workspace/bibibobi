package n.webinfotech.bibibobi.presentation.presenters;


import n.webinfotech.bibibobi.domain.models.Address;
import n.webinfotech.bibibobi.presentation.ui.adapters.CityListDialogAdapter;
import n.webinfotech.bibibobi.presentation.ui.adapters.StateListDialogAdapter;

public interface EditShippingAddressPresenter {
    void getShippingAddressDetails(int addressId);
    void updateAddress(String email,
                       String mobile,
                       String pin,
                       String address);
    void fetchStateList();
    void fetchCityList();
    interface View {
        void showLoader();
        void hideLoader();
        void setData(Address address);
        void loadStateAdapter(StateListDialogAdapter stateListDialogAdapter);
        void loadCitiesAdapter(CityListDialogAdapter cityListDialogAdapter);
        void setStateName(String stateName);
        void setCityName(String cityName);
        void hideStateRecyclerView();
        void hideCityRecyclerView();
        void onUpdateAddressSuccess();
        void goToLoginActivity();
    }
}
