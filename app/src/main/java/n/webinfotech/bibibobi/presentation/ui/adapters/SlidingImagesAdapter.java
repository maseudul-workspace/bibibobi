package n.webinfotech.bibibobi.presentation.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import n.webinfotech.bibibobi.R;
import n.webinfotech.bibibobi.domain.models.ProductImage;
import n.webinfotech.bibibobi.util.GlideHelper;

/**
 * Created by Raj on 25-04-2019.
 */

public class SlidingImagesAdapter extends PagerAdapter {


    Context mContext;
    ProductImage[] images;
    int customPosition = 0;

    public SlidingImagesAdapter(Context mContext, ProductImage[] images) {
        this.mContext = mContext;
        this.images = images;
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ImageView imageView = new ImageView(mContext);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, imageView, mContext.getResources().getString(R.string.base_url) + "/images/slider/" + images[position].image, 50);
        container.addView(imageView);
        return imageView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
