package n.webinfotech.bibibobi.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import n.webinfotech.bibibobi.AndroidApplication;
import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.ChangePasswordInteractor;
import n.webinfotech.bibibobi.domain.interactors.impl.ChangePasswordInteractorImpl;
import n.webinfotech.bibibobi.domain.models.User;
import n.webinfotech.bibibobi.presentation.presenters.ChangePasswordPresenter;
import n.webinfotech.bibibobi.presentation.presenters.base.AbstractPresenter;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class ChangePasswordPresenterImpl extends AbstractPresenter implements ChangePasswordPresenter, ChangePasswordInteractor.Callback {

    Context mContext;
    ChangePasswordPresenter.View mView;
    ChangePasswordInteractorImpl changePasswordInteractor;
    AndroidApplication androidApplication;

    public ChangePasswordPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void changePassword(String currentPswd, String newPswd) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        changePasswordInteractor = new ChangePasswordInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.userId, user.apiToken, currentPswd, newPswd);
        changePasswordInteractor.execute();
    }

    @Override
    public void onChangePasswordSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "Password Changed Successfully", Toast.LENGTH_SHORT).show();
        mView.onChangePasswordSuccess();
    }

    @Override
    public void onChangePasswordFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.goToLoginActivity();
        } else {
            Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        }
    }
}
