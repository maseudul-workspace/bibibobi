package n.webinfotech.bibibobi.presentation.presenters;


import n.webinfotech.bibibobi.presentation.ui.adapters.OrderListAdapter;

public interface OrderListPresenter {
    void fetchOrdersList();
    interface View {
        void loadAdapter(OrderListAdapter orderListAdapter);
        void showLoader();
        void hideLoader();
        void goToLoginActivity();
    }
}
