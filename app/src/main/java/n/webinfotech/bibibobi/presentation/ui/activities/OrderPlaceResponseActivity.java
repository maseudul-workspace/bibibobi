package n.webinfotech.bibibobi.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.bibibobi.R;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class OrderPlaceResponseActivity extends AppCompatActivity {

    @BindView(R.id.layout_success)
    View layoutSuccess;
    @BindView(R.id.layout_failed)
    View layoutFailed;
    @BindView(R.id.txt_view_order_id)
    TextView txtViewOrderId;
    @BindView(R.id.txt_view_failed_order_id)
    TextView txtViewFailedOrderId;
    @BindView(R.id.txt_view_order_date)
    TextView txtViewOrderDate;
    @BindView(R.id.txt_view_failed_order_date)
    TextView txtViewFailedOrderDate;
    @BindView(R.id.txt_view_payment_type)
    TextView txtViewPaymentType;
    @BindView(R.id.txt_view_failed_payment_type)
    TextView txtViewFailedPaymentType;
    @BindView(R.id.txt_view_transaction_id)
    TextView txtViewTranactionId;
    @BindView(R.id.layout_transaction_id)
    View layoutTransactionId;
    @BindView(R.id.txt_view_amount)
    TextView txtViewAmount;
    @BindView(R.id.txt_view_failed_amount)
    TextView txtViewFailedAmount;
    int orderId;
    int orderStatus;
    String transactionId;
    int paymentType;
    String amount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_place_response);
        ButterKnife.bind(this);
        orderId = getIntent().getIntExtra("orderId", 0);
        orderStatus = getIntent().getIntExtra("orderStatus", 0);
        transactionId = getIntent().getStringExtra("transactionId");
        paymentType = getIntent().getIntExtra("paymentType", 0);
        amount = getIntent().getStringExtra("amount");
        setUpUI();
    }

    public void setUpUI() {
        if (orderStatus == 1) {
            layoutSuccess.setVisibility(View.VISIBLE);
            txtViewOrderId.setText(Integer.toString(orderId));
            txtViewOrderDate.setText(getDate());
            if (paymentType == 1) {
                txtViewTranactionId.setText(transactionId);
                txtViewPaymentType.setText("Online Payment");
            } else {
                layoutTransactionId.setVisibility(View.GONE);
                txtViewPaymentType.setText("Cash On Delivery");
            }
            txtViewAmount.setText("Rs. " + amount);
        } else {
            layoutFailed.setVisibility(View.VISIBLE);
            txtViewFailedOrderId.setText(Integer.toString(orderId));
            txtViewFailedOrderDate.setText(getDate());
            txtViewFailedAmount.setText("Rs. " + amount);
            if (paymentType == 1) {
                txtViewFailedPaymentType.setText("Online Payment");
            } else {
                txtViewFailedPaymentType.setText("Cash On Delivery");
            }
        }
    }

    public static String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "MMMM dd, yyyy", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

}
