package n.webinfotech.bibibobi.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import n.webinfotech.bibibobi.AndroidApplication;
import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.FetchWishListInteractor;
import n.webinfotech.bibibobi.domain.interactors.RemoveFromWishlistInteractor;
import n.webinfotech.bibibobi.domain.interactors.impl.FetchWishListInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.RemoveFromWishListInteractorImpl;
import n.webinfotech.bibibobi.domain.models.User;
import n.webinfotech.bibibobi.domain.models.WishlistProduct;
import n.webinfotech.bibibobi.presentation.presenters.WishlistPresenter;
import n.webinfotech.bibibobi.presentation.presenters.base.AbstractPresenter;
import n.webinfotech.bibibobi.presentation.ui.adapters.WishlistAdapter;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class WishlistPresenterImpl extends AbstractPresenter implements WishlistPresenter,
                                                                        FetchWishListInteractor.Callback,
                                                                        RemoveFromWishlistInteractor.Callback,
                                                                        WishlistAdapter.Callback
{

    Context mContext;
    WishlistPresenter.View mView;
    FetchWishListInteractorImpl fetchWishListInteractor;
    RemoveFromWishListInteractorImpl removeFromWishListInteractor;

    public WishlistPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchWishlist() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        if (user != null) {
            fetchWishListInteractor = new FetchWishListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId);
            fetchWishListInteractor.execute();
        }
    }

    @Override
    public void onWishlistFetchSuccess(WishlistProduct[] wishlistProducts) {
        WishlistAdapter wishlistAdapter = new WishlistAdapter(mContext, wishlistProducts, this);
        mView.loadWishlistAdapter(wishlistAdapter);
        mView.hideLoader();
    }

    @Override
    public void onWishListFetchFail(String errorMsg, int loginError) {
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        mView.hideLoader();
        mView.hideRecyclerView();
    }

    @Override
    public void onWishlistRemovedSuccess() {
        mView.hideLoader();
        fetchWishlist();
        Toast.makeText(mContext, "Removed From Wishlist", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onWishListRemovedFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.goToLoginActivity();
        } else {
            Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onProductClicked(int productId) {
        mView.goToProductDetails(productId);
    }

    @Override
    public void removeFromWishlist(int wishListId) {
        mView.showLoader();
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        removeFromWishListInteractor = new RemoveFromWishListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, wishListId);
        removeFromWishListInteractor.execute();
    }
}
