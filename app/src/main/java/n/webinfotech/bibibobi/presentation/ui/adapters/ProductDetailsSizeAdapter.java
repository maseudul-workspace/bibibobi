package n.webinfotech.bibibobi.presentation.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.bibibobi.R;
import n.webinfotech.bibibobi.domain.models.ProductDetailsSize;

/**
 * Created by Raj on 11-09-2019.
 */

public class ProductDetailsSizeAdapter extends RecyclerView.Adapter<ProductDetailsSizeAdapter.ViewHolder> {

    public interface Callback {
        void onFilterSelect(int id);
    }

    Context mContext;
    ProductDetailsSize[] sizes;
    Callback mCallback;

    public ProductDetailsSizeAdapter(Context mContext, ProductDetailsSize[] sizes, Callback mCallback) {
        this.mContext = mContext;
        this.sizes = sizes;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_product_details_filter_size, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.txtViewFilterValue.setText(sizes[i].sizeName);
        if (sizes[i].isSelected) {
            viewHolder.mainLayout.setBackground(mContext.getResources().getDrawable(R.drawable.rectangle_filter_select_background));
        } else {
            viewHolder.mainLayout.setBackground(mContext.getResources().getDrawable(R.drawable.rectangle_grey_stroke));
        }
        viewHolder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onFilterSelect(sizes[i].sizeId);
            }
        });
    }

    @Override
    public int getItemCount() {
        return sizes.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.main_layout)
        View mainLayout;
        @BindView(R.id.txt_view_filter_value)
        TextView txtViewFilterValue;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateDataSet(ProductDetailsSize[] sizes) {
        this.sizes = sizes;
        notifyDataSetChanged();
    }

}
