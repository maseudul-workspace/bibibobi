package n.webinfotech.bibibobi.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AlertDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import n.webinfotech.bibibobi.R;


public class PaymentMethodDialog {

    public interface Callback {
        void onPaymentMethodSelect(int id);
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    @BindView(R.id.img_view_card_check)
    ImageView imgViewCardCheck;
    @BindView(R.id.img_view_cash_check)
    ImageView imgViewCashCheck;
    Callback mCallback;
    int flag = 1;

    public PaymentMethodDialog(Context mContext, Activity mActivity, Callback mCallback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = mCallback;
    }

    public void setUpDialogView() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_payment_method_dialog, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
    }

    @OnClick(R.id.layout_card) void onCardClicked() {
        imgViewCardCheck.setVisibility(View.VISIBLE);
        imgViewCashCheck.setVisibility(View.GONE);
        flag = 2;
    }

    @OnClick(R.id.layout_cash) void onCashClicked() {
        imgViewCashCheck.setVisibility(View.VISIBLE);
        imgViewCardCheck.setVisibility(View.GONE);
        flag = 1;
    }

    public void showDialog() {
        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }

    @OnClick(R.id.btn_next_step) void onNextStepClicked() {
        hideDialog();
        mCallback.onPaymentMethodSelect(flag);
    }

}
