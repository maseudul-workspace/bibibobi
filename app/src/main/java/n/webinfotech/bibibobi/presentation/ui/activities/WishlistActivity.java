package n.webinfotech.bibibobi.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.bibibobi.R;
import n.webinfotech.bibibobi.domain.executors.impl.ThreadExecutor;
import n.webinfotech.bibibobi.presentation.presenters.WishlistPresenter;
import n.webinfotech.bibibobi.presentation.presenters.impl.WishlistPresenterImpl;
import n.webinfotech.bibibobi.presentation.ui.adapters.WishlistAdapter;
import n.webinfotech.bibibobi.threading.MainThreadImpl;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

public class WishlistActivity extends AppCompatActivity implements WishlistPresenter.View {

    @BindView(R.id.recycler_view_wishlist)
    RecyclerView recyclerViewWishlist;
    ProgressDialog progressDialog;
    WishlistPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wishlist);
        ButterKnife.bind(this);
        setUpProgressDialog();
        getSupportActionBar().setTitle("My Wishlist");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initialisePresenter();
    }

    public void initialisePresenter() {
       mPresenter = new WishlistPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadWishlistAdapter(WishlistAdapter wishlistAdapter) {
        recyclerViewWishlist.setVisibility(View.VISIBLE);
        recyclerViewWishlist.setAdapter(wishlistAdapter);
        recyclerViewWishlist.setLayoutManager(new GridLayoutManager(this, 2));

    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void hideRecyclerView() {
        recyclerViewWishlist.setVisibility(View.GONE);
    }

    @Override
    public void goToLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchWishlist();
        showLoader();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void goToProductDetails(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", productId);
        startActivity(intent);
    }

}
