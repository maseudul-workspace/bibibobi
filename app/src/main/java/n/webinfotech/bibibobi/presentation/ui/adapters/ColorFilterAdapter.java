package n.webinfotech.bibibobi.presentation.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.bibibobi.R;
import n.webinfotech.bibibobi.domain.models.Colors;

/**
 * Created by Raj on 30-08-2019.
 */

public class ColorFilterAdapter extends RecyclerView.Adapter<ColorFilterAdapter.ViewHolder> {

    public interface Callback {
        void onColorSelect(int colorId);
        void onColorNotSelect(int colorId);
    }

    Context mContext;
    Colors[] colors;
    Callback mCallback;

    public ColorFilterAdapter(Context mContext, Colors[] colors, Callback callback) {
        this.mContext = mContext;
        this.colors = colors;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_colors, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.layoutColor.setBackgroundColor(Color.parseColor(colors[i].colorValue));
        viewHolder.txtViewColorName.setText(colors[i].colorName);
        viewHolder.colorCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mCallback.onColorSelect(colors[i].colorId);
                } else {
                    mCallback.onColorNotSelect(colors[i].colorId);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return colors.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.color_check_box)
        CheckBox colorCheckBox;
        @BindView(R.id.txt_view_color_name)
        TextView txtViewColorName;
        @BindView(R.id.layout_color)
        View layoutColor;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
