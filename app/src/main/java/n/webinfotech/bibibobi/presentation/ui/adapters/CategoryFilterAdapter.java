package n.webinfotech.bibibobi.presentation.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.bibibobi.R;
import n.webinfotech.bibibobi.domain.models.SecondCategory;
import n.webinfotech.bibibobi.util.GlideHelper;

/**
 * Created by Raj on 02-09-2019.
 */

public class CategoryFilterAdapter extends RecyclerView.Adapter<CategoryFilterAdapter.ViewHolder> {

    public interface Callback {
        void onCategoryClicked(int categoryId);
    }

    Context mContext;
    SecondCategory[] categories;
    Callback mCallback;

    public CategoryFilterAdapter(Context mContext, SecondCategory[] categories, Callback callback) {
        this.mContext = mContext;
        this.categories = categories;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_category_filter, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        GlideHelper.setImageViewCustomRoundedCorners(mContext, viewHolder.imgViewCategoryFilter, mContext.getResources().getString(R.string.base_url) + "/images/category/second_category/thumb/" + categories[i].image, 50);
        viewHolder.txtViewCategoryFilter.setText(categories[i].name);
        viewHolder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onCategoryClicked(categories[i].id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categories.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_category_filter)
        ImageView imgViewCategoryFilter;
        @BindView(R.id.txt_view_category_filter)
        TextView txtViewCategoryFilter;
        @BindView(R.id.main_layout)
        View mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
