package n.webinfotech.bibibobi.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.bibibobi.R;
import n.webinfotech.bibibobi.domain.models.Colors;
import n.webinfotech.bibibobi.domain.models.Designer;
import n.webinfotech.bibibobi.domain.models.SecondCategory;
import n.webinfotech.bibibobi.domain.models.Size;
import n.webinfotech.bibibobi.presentation.ui.adapters.CategoryFilterAdapter;
import n.webinfotech.bibibobi.presentation.ui.adapters.ColorFilterAdapter;
import n.webinfotech.bibibobi.presentation.ui.adapters.DesignerFilterAdapter;
import n.webinfotech.bibibobi.presentation.ui.adapters.SizesFilterAdapter;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Raj on 29-04-2019.
 */

public class FilterByDialog implements DesignerFilterAdapter.Callback,  CategoryFilterAdapter.Callback, ColorFilterAdapter.Callback, SizesFilterAdapter.Callback {

    @Override
    public void onDesignerSelect(int designerId) {
        designerIds.add(designerId);
    }

    @Override
    public void onDesignerNotSelect(int designerId) {
        for (int i = 0; i < designerIds.size(); i++) {
            if (designerIds.get(i) == designerId) {
                designerIds.remove(i);
                break;
            }
        }
    }

    @Override
    public void onSizeSelect(int sizeId) {
        sizeIds.add(sizeId);
    }

    @Override
    public void onSizeNotSelect(int sizeId) {
        for (int i = 0; i < sizeIds.size(); i++) {
            if (sizeIds.get(i) == sizeId) {
                sizeIds.remove(i);
                break;
            }
        }
    }

    public interface Callback {
        void onFiltersApplied(ArrayList<Integer> sizeIds, ArrayList<Integer> designerIds, ArrayList<Integer> colorIds, int minPrice, int maxPrice);
        void onCategoryFilterClicked(int categoryId);
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    @BindView(R.id.btn_apply_filter)
    Button applyFilterBtn;
    @BindView(R.id.recycler_view_categories_filter)
    RecyclerView recyclerViewCategoryFilter;
    @BindView(R.id.recycler_view_sizes_filter)
    RecyclerView recyclerViewSizesFilter;
    @BindView(R.id.recycler_view_designers_filter)
    RecyclerView recyclerViewDesignersFilter;
    @BindView(R.id.recycler_view_colors_filters)
    RecyclerView recyclerViewColorsFilter;
    DesignerFilterAdapter designerFilterAdapter;
    CategoryFilterAdapter categoryFilterAdapter;
    ColorFilterAdapter colorFilterAdapter;
    SizesFilterAdapter sizesFilterAdapter;
    @BindView(R.id.edit_text_price_form)
    EditText editTextPriceFrom;
    @BindView(R.id.edit_text_price_to)
    EditText editTextPriceTo;
    ArrayList<Integer> sizeIds;
    ArrayList<Integer> designerIds;
    ArrayList<Integer> colorIds;
    int minPrice = 0;
    int maxPrice = 0;
    Callback mCallback;

    public FilterByDialog(Context mContext, Activity mActivity, Callback callback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        mCallback = callback;
    }

    public void setUpDialogView(){
        sizeIds = new ArrayList<>();
        designerIds = new ArrayList<>();
        colorIds = new ArrayList<>();
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.filter_by_dialog_layout, null);
        ButterKnife.bind(this, dialogContainer);
        applyFilterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    minPrice = Integer.parseInt(editTextPriceFrom.getText().toString());
                } catch (NumberFormatException e) {
                    minPrice = 0;
                }

                try {
                    maxPrice = Integer.parseInt(editTextPriceTo.getText().toString());
                } catch (NumberFormatException e) {
                    maxPrice = 0;
                }
                if (minPrice > maxPrice) {
                    Toast.makeText(mContext, "Minimun price should be less than maximun price", Toast.LENGTH_SHORT).show();
                } else {
                    mCallback.onFiltersApplied(sizeIds, designerIds, colorIds, minPrice, maxPrice);
                    dialog.dismiss();
                }
            }
        });

        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog = builder.create();
    }

    public void setData(SecondCategory[] secondCategories, Colors[] colors, Size[] sizes, Designer[] designers) {


        designerFilterAdapter = new DesignerFilterAdapter(mContext, designers, this);
        categoryFilterAdapter = new CategoryFilterAdapter(mContext, secondCategories, this);
        colorFilterAdapter = new ColorFilterAdapter(mContext, colors, this);
        sizesFilterAdapter = new SizesFilterAdapter(mContext, sizes, this);

        recyclerViewSizesFilter.setAdapter(sizesFilterAdapter);
        recyclerViewSizesFilter.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerViewSizesFilter.setNestedScrollingEnabled(false);

        recyclerViewDesignersFilter.setAdapter(designerFilterAdapter);
        recyclerViewDesignersFilter.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerViewDesignersFilter.setNestedScrollingEnabled(false);

        recyclerViewColorsFilter.setAdapter(colorFilterAdapter);
        recyclerViewColorsFilter.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerViewColorsFilter.setNestedScrollingEnabled(false);

        recyclerViewCategoryFilter.setAdapter(categoryFilterAdapter);
        recyclerViewCategoryFilter.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerViewCategoryFilter.setNestedScrollingEnabled(false);
    }

    public void showDialog(){
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.show();
    }

    @Override
    public void onCategoryClicked(int categoryId) {
        mCallback.onCategoryFilterClicked(categoryId);
    }

    @Override
    public void onColorSelect(int colorId) {
        colorIds.add(colorId);
    }

    @Override
    public void onColorNotSelect(int colorId) {
        for (int i = 0; i < colorIds.size(); i++) {
            if (colorIds.get(i) == colorId) {
                colorIds.remove(i);
                break;
            }
        }
    }

}
