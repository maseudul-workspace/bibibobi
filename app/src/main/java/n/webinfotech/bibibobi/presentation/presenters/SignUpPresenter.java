package n.webinfotech.bibibobi.presentation.presenters;

public interface SignUpPresenter {
    void signUp(String name, String email, String mobile, String password, String confirmPassword);
    interface View {
        void showLoader();
        void hideLoader();
        void goToLoginActivity();
        void onEmailError(String errorMsg);
        void onMobileError(String errorMsg);
    }
}
