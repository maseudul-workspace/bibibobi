package n.webinfotech.bibibobi.presentation.presenters;

import java.util.ArrayList;

import n.webinfotech.bibibobi.domain.models.Colors;
import n.webinfotech.bibibobi.domain.models.Designer;
import n.webinfotech.bibibobi.domain.models.SecondCategory;
import n.webinfotech.bibibobi.domain.models.Size;
import n.webinfotech.bibibobi.presentation.ui.adapters.ProductListVerticalAdapter;

public interface ProductListPresenter {
    void fetchProductListWithFilters(int categoryId, int page, String type);
    void fetchProductListWithoutFilters(int secondCategory,
                                        ArrayList<Integer> designerIds,
                                        ArrayList<Integer> sizeIds,
                                        ArrayList<Integer> colorIds,
                                        int priceFrom,
                                        int priceTo,
                                        int sortBy,
                                        int pageNo,
                                        String type
                                        );
    void fetchCartCount();
    void fetchWishlist();
    interface View {
        void loadProducts(ProductListVerticalAdapter adapter, int totalPage);
        void loadFilters(SecondCategory[] secondCategories, Colors[] colors, Size[] sizes, Designer[] designers);
        void showLoader();
        void hideLoader();
        void showPaginationProgressbar();
        void hidePaginationProgressbar();
        void hideProductsRecyclerView();
        void goToProductDetails(int productId);
        void loadCartCount(int cartCount);
        void goToLogin();
    }
}
