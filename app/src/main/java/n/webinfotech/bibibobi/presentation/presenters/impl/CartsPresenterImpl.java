package n.webinfotech.bibibobi.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import n.webinfotech.bibibobi.AndroidApplication;
import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.DeleteItemFromCartInteractor;
import n.webinfotech.bibibobi.domain.interactors.FetchCartInteractor;
import n.webinfotech.bibibobi.domain.interactors.UpdateCartInteractor;
import n.webinfotech.bibibobi.domain.interactors.impl.DeleteItemFromCartInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.FetchCartInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.UpdateCartInteractorImpl;
import n.webinfotech.bibibobi.domain.models.Cart;
import n.webinfotech.bibibobi.domain.models.User;
import n.webinfotech.bibibobi.presentation.presenters.CartsPresenter;
import n.webinfotech.bibibobi.presentation.presenters.base.AbstractPresenter;
import n.webinfotech.bibibobi.presentation.ui.adapters.CartAdapter;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;


public class CartsPresenterImpl extends AbstractPresenter implements CartsPresenter, FetchCartInteractor.Callback, CartAdapter.Callback, DeleteItemFromCartInteractor.Callback, UpdateCartInteractor.Callback {

    Context mContext;
    CartsPresenter.View mView;
    FetchCartInteractorImpl fetchCartInteractor;
    AndroidApplication androidApplication;
    DeleteItemFromCartInteractorImpl deleteItemFromCartInteractor;
    UpdateCartInteractorImpl updateCartInteractor;

    public CartsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchCartProducts() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        fetchCartInteractor = new FetchCartInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId);
        fetchCartInteractor.execute();
    }

    @Override
    public void onDeleteClicked(int cartId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        deleteItemFromCartInteractor = new DeleteItemFromCartInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, cartId);
        deleteItemFromCartInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onUpdateClicked(int cartId, int qty) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        updateCartInteractor = new UpdateCartInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, qty, cartId);
        updateCartInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onCartItemDeleteSuccess() {
        Toast.makeText(mContext, "Deleted Successfully", Toast.LENGTH_SHORT).show();
        fetchCartProducts();
        mView.hideCartRecyclerView();
    }

    @Override
    public void onCartItemDeleteFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.goToLoginActivity();
        } else {
            Toast.makeText(mContext, "Unable to delete", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onUpdateCartSuccess() {
        fetchCartProducts();
        Toast.makeText(mContext, "Successfully updated", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUpdateCartFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.goToLoginActivity();
        } else {
            Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFetchCartSuccess(Cart[] carts) {
        if (carts.length > 0) {
            boolean isOutOfStock = false;
            double cartTotal = 0;
            double totalMrp = 0;
            double totalDiscountPrice = 0;
            for (int i = 0; i < carts.length; i++) {
                cartTotal = cartTotal + carts[i].productPrice*carts[i].quantity;
                totalMrp = totalMrp + carts[i].productMRP*carts[i].quantity;
                totalDiscountPrice = totalDiscountPrice + (carts[i].productMRP - carts[i].productPrice)*carts[i].quantity;
                if (carts[i].stock == 0) {
                    isOutOfStock = true;
                } else if (carts[i].quantity > carts[i].stock) {
                    isOutOfStock = true;
                }
            }
            CartAdapter cartAdapter = new CartAdapter(mContext, carts, this);
            mView.loadData(cartAdapter, cartTotal + 50*carts.length, carts.length);
            mView.disableCartButton(isOutOfStock);
            mView.setCartSummary(carts.length, totalMrp, totalDiscountPrice, 50 * carts.length, cartTotal + 50 * carts.length );
        } else {
            mView.hideCartRecyclerView();
        }
        mView.hideLoader();
    }

    @Override
    public void onFetchCartFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.goToLoginActivity();
        } else {
            mView.hideCartRecyclerView();
            Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        }
    }
}
