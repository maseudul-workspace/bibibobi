package n.webinfotech.bibibobi.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import n.webinfotech.bibibobi.AndroidApplication;
import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.FetchCitiesInteractor;
import n.webinfotech.bibibobi.domain.interactors.FetchStateListInteractor;
import n.webinfotech.bibibobi.domain.interactors.FetchUserInfoInteractor;
import n.webinfotech.bibibobi.domain.interactors.UpdateUserProfileInteractor;
import n.webinfotech.bibibobi.domain.interactors.impl.FetchCitiesInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.FetchStateListInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.FetchUserInfoInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.UpdateUserProfileInteractorImpl;
import n.webinfotech.bibibobi.domain.models.Cities;
import n.webinfotech.bibibobi.domain.models.StateList;
import n.webinfotech.bibibobi.domain.models.User;
import n.webinfotech.bibibobi.domain.models.UserDetails;
import n.webinfotech.bibibobi.presentation.presenters.UserProfilePresenter;
import n.webinfotech.bibibobi.presentation.presenters.base.AbstractPresenter;
import n.webinfotech.bibibobi.presentation.ui.adapters.CityListDialogAdapter;
import n.webinfotech.bibibobi.presentation.ui.adapters.StateListDialogAdapter;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class UserProfilePresenterImpl extends AbstractPresenter implements UserProfilePresenter,
                                                                            FetchUserInfoInteractor.Callback,
                                                                            FetchStateListInteractor.Callback,
                                                                            StateListDialogAdapter.Callback,
                                                                            FetchCitiesInteractor.Callback,
                                                                            CityListDialogAdapter.Callback,
                                                                            UpdateUserProfileInteractor.Callback
{

    Context mContext;
    UserProfilePresenter.View mView;
    FetchUserInfoInteractorImpl fetchUserInfoInteractor;
    AndroidApplication androidApplication;
    FetchStateListInteractorImpl fetchStateListInteractor;
    StateListDialogAdapter stateListDialogAdapter;
    FetchCitiesInteractorImpl fetchCitiesInteractor;
    int stateId = 3;
    CityListDialogAdapter cityListDialogAdapter;
    int cityId = 1;
    UpdateUserProfileInteractorImpl updateUserProfileInteractor;

    public UserProfilePresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchStateList() {
        fetchStateListInteractor = new FetchStateListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this);
        fetchStateListInteractor.execute();
    }

    @Override
    public void fetchCityList() {
        fetchCitiesInteractor = new FetchCitiesInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, stateId);
        fetchCitiesInteractor.execute();
    }

    @Override
    public void fetchUserProfile() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        fetchUserInfoInteractor = new FetchUserInfoInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.userId, user.apiToken);
        fetchUserInfoInteractor.execute();
    }

    @Override
    public void updateUserProfile(String gender, String name, String pin, String dob) {
//        if (cityId == 0 || stateId == 0) {
//            Toast.makeText(mContext, "State or City is missing", Toast.LENGTH_SHORT).show();
//        } else {
            androidApplication = (AndroidApplication) mContext.getApplicationContext();
            User user = androidApplication.getUserInfo(mContext);
            updateUserProfileInteractor = new UpdateUserProfileInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, gender, name, stateId, cityId, pin, dob);
            updateUserProfileInteractor.execute();
            mView.showLoader();
//        }
    }

    @Override
    public void onGettingUserInfoSuccess(UserDetails user) {
        stateId = user.userProfile.stateId;
        cityId = user.userProfile.cityId;
        fetchCityList();
        mView.loadData(user);
        mView.hideLoader();
    }

    @Override
    public void onGettingUserInfoFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.goToLoginActivity();
        } else {
            Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void onGettingCitiesSuccess(Cities[] cities) {
        if (cities.length == 0) {
            mView.hideCityRecyclerView();
        } else {
            cityListDialogAdapter = new CityListDialogAdapter(mContext, cities, this::onCityClicked);
            mView.loadCitiesAdapter(cityListDialogAdapter);
        }
    }

    @Override
    public void onGettingCitiesFail() {
        mView.hideCityRecyclerView();
    }

    @Override
    public void onGettingStateListSuccess(StateList[] states) {
        if (states.length == 0) {
            mView.hideStateRecyclerView();
        } else {
            stateListDialogAdapter = new StateListDialogAdapter(mContext, states, this::onStateClicked);
            mView.loadStateAdapter(stateListDialogAdapter);
        }
    }

    @Override
    public void onGettingStateListFail() {
        mView.hideStateRecyclerView();
    }

    @Override
    public void onCityClicked(int id, String city) {
        mView.setCityName(city);
        cityId = id;
    }

    @Override
    public void onStateClicked(int id, String state) {
        stateId = id;
        mView.setStateName(state);
        fetchCityList();
    }

    @Override
    public void onUserProfileUpdateSuccess() {
        Toast.makeText(mContext, "Profile Updated Successfully", Toast.LENGTH_SHORT).show();
        mView.hideLoader();
    }

    @Override
    public void onUserProfileUpdateFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.goToLoginActivity();
        } else {
            Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        }
    }
}
