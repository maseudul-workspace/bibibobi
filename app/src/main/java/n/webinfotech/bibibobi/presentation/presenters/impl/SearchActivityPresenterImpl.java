package n.webinfotech.bibibobi.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import n.webinfotech.bibibobi.AndroidApplication;
import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.AddToCartWithoutFilterInteractor;
import n.webinfotech.bibibobi.domain.interactors.AddToWishListInteractor;
import n.webinfotech.bibibobi.domain.interactors.FetchWishListInteractor;
import n.webinfotech.bibibobi.domain.interactors.GetProductSearchListInteractor;
import n.webinfotech.bibibobi.domain.interactors.RemoveFromWishlistInteractor;
import n.webinfotech.bibibobi.domain.interactors.impl.AddToCartWithoutFilterInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.AddToWishListInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.FetchWishListInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.GetProductSearchListInteractorImpl;
import n.webinfotech.bibibobi.domain.interactors.impl.RemoveFromWishListInteractorImpl;
import n.webinfotech.bibibobi.domain.models.Product;
import n.webinfotech.bibibobi.domain.models.User;
import n.webinfotech.bibibobi.domain.models.WishlistProduct;
import n.webinfotech.bibibobi.presentation.presenters.SearchActivityPresenter;
import n.webinfotech.bibibobi.presentation.presenters.base.AbstractPresenter;
import n.webinfotech.bibibobi.presentation.ui.adapters.ProductListVerticalAdapter;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;

public class SearchActivityPresenterImpl extends AbstractPresenter implements   SearchActivityPresenter,
                                                                                GetProductSearchListInteractor.Callback,
                                                                                ProductListVerticalAdapter.Callback,
                                                                                AddToCartWithoutFilterInteractor.Callback,
                                                                                AddToWishListInteractor.Callback,
                                                                                FetchWishListInteractor.Callback,
                                                                                RemoveFromWishlistInteractor.Callback {

    Context mContext;
    SearchActivityPresenter.View mView;
    GetProductSearchListInteractorImpl getProductSearchListInteractor;
    ProductListVerticalAdapter adapter;
    String searchKey;
    AddToCartWithoutFilterInteractorImpl addToCartWithoutFilterInteractor;
    AddToWishListInteractorImpl addToWishListInteractor;
    int position;
    RemoveFromWishListInteractorImpl removeFromWishListInteractor;
    FetchWishListInteractorImpl fetchWishListInteractor;

    public SearchActivityPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchProducts(String searchKey) {
        this.searchKey = searchKey;
        getProductSearchListInteractor = new GetProductSearchListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, searchKey);
        getProductSearchListInteractor.execute();
    }

    @Override
    public void fetchWishlist() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        if (user != null) {
            fetchWishListInteractor = new FetchWishListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId);
            fetchWishListInteractor.execute();
        }
    }

    @Override
    public void onGettingProductListSuccess(Product[] products, String searchKey) {
        mView.hideLoader();
        if (products.length > 0) {
            if (this.searchKey.equals(searchKey)) {
                AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
                User user = androidApplication.getUserInfo(mContext);
                WishlistProduct[] wishlistProducts = androidApplication.getWishList(mContext);
                if (user != null && wishlistProducts != null && wishlistProducts.length != 0) {
                    for (int i = 0; i < wishlistProducts.length; i++) {
                        for (int j = 0; j < products.length; j++) {
                            if (wishlistProducts[i].productId == products[j].id) {
                                products[j].isWishListPresent = true;
                                break;
                            }
                        }
                    }
                }
                adapter = new ProductListVerticalAdapter(mContext, products, this);
                mView.loadAdapter(adapter);
            }
        } else {
            Toast.makeText(mContext, "No products found", Toast.LENGTH_SHORT).show();
            mView.hideRecyclerView();
        }
    }

    @Override
    public void onGettingProductListFail(String errorMsg) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        mView.hideRecyclerView();
    }

    @Override
    public void onProductClicked(int productId) {
        mView.goToProductDetails(productId);
    }

    @Override
    public void onAddToCartClicked(int productId) {
        if (checkLogin()) {
            AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
            User user = androidApplication.getUserInfo(mContext);
            addToCartWithoutFilterInteractor = new AddToCartWithoutFilterInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, productId);
            addToCartWithoutFilterInteractor.execute();
            mView.showLoader();
        } else {
            Toast.makeText(mContext, "PLease Login", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void addToWishlist(int productId, int position) {
        mView.showLoader();
        this.position = position;
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        if (user != null) {
            addToWishListInteractor = new AddToWishListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(),  this, user.apiToken, user.userId, productId);
            addToWishListInteractor.execute();
        }
    }

    @Override
    public void removeFromWishList(int productId, int position) {
        mView.showLoader();
        this.position = position;
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        WishlistProduct[] wishlistProducts = androidApplication.getWishList(mContext);
        User user = androidApplication.getUserInfo(mContext);
        int wishListId = 0;
        for (int i = 0; i < wishlistProducts.length; i++) {
            if (wishlistProducts[i].productId == productId) {
                wishListId = wishlistProducts[i].id;
                break;
            }
        }
        removeFromWishListInteractor = new RemoveFromWishListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, wishListId);
        removeFromWishListInteractor.execute();
    }

    @Override
    public void onAddToCartSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "Added To Cart Successfully", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAddToCartFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    public boolean checkLogin() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onAddingWishListSuccess() {
        mView.hideLoader();
        fetchWishlist();
        Toast.makeText(mContext, "Added To Wishlist", Toast.LENGTH_SHORT).show();
        adapter.onAddToWishlistSuccess(position);
    }

    @Override
    public void onAddingWishListFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.goToLoginActivity();
        } else {
            Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onWishlistFetchSuccess(WishlistProduct[] wishlistProducts) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishList(mContext, wishlistProducts);
    }

    @Override
    public void onWishListFetchFail(String errorMsg, int loginError) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishList(mContext, null);
    }

    @Override
    public void onWishlistRemovedSuccess() {
        mView.hideLoader();
        fetchWishlist();
        Toast.makeText(mContext, "Removed From Wishlist", Toast.LENGTH_SHORT).show();
        adapter.onRemoveFromWishlistSuccess(position);
    }

    @Override
    public void onWishListRemovedFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.goToLoginActivity();
        } else {
            Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        }
    }
}
