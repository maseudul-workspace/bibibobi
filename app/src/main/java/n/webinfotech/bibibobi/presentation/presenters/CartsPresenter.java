package n.webinfotech.bibibobi.presentation.presenters;

import n.webinfotech.bibibobi.presentation.ui.adapters.CartAdapter;

public interface CartsPresenter {
    void fetchCartProducts();
    interface View {
        void showLoader();
        void hideLoader();
        void loadData(CartAdapter adapter, double cartTotal, int cartItem);
        void hideCartRecyclerView();
        void goToLoginActivity();
        void disableCartButton(boolean isOutOfStock);
        void setCartSummary(int totalItems, double totalPrice, double sellingPrice, int totalShippingCharges, double total);
    }
}
