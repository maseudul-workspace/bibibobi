package n.webinfotech.bibibobi.presentation.presenters;

import n.webinfotech.bibibobi.domain.models.PinData;
import n.webinfotech.bibibobi.domain.models.Product;
import n.webinfotech.bibibobi.domain.models.ProductImage;
import n.webinfotech.bibibobi.presentation.ui.adapters.ColorsHorizontalAdapter;
import n.webinfotech.bibibobi.presentation.ui.adapters.ProductDetailsSizeAdapter;
import n.webinfotech.bibibobi.presentation.ui.adapters.ProductsHorizontalAdapter;

public interface ProductDetailsPresenter {

    void getProductDetails(int productId);
    void addToCart(int productId);
    void fetchCartCount();
    void fetchWishlist();
    void addToWishList(int productId);
    void removeFromWishlist(int productId);
    void checkPin(String pin);
    interface View {
        void loadData(Product product, ProductImage[] productImages, ProductDetailsSizeAdapter productDetailsSizeAdapter, ColorsHorizontalAdapter colorsHorizontalAdapter, ProductsHorizontalAdapter productsHorizontalAdapter);
        void showLoader();
        void hideLoader();
        void goToProductDetails(int productId);
        void loadCartCount(int cartCount);
        void onAddToWishlistSuccess();
        void onRemoveFromWishlistSuccess();
        void onPinAvailable(PinData pinData);
        void onPinUnavailable();
        void goToLoginActivity();
    }

}
