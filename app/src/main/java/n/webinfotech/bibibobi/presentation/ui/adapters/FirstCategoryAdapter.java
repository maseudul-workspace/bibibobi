package n.webinfotech.bibibobi.presentation.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.bibibobi.R;
import n.webinfotech.bibibobi.domain.models.FirstCategory;

/**
 * Created by Raj on 26-09-2019.
 */

public class FirstCategoryAdapter extends RecyclerView.Adapter<FirstCategoryAdapter.ViewHolder> implements SecondCategoryAdapter.Callback {

    @Override
    public void onSecondCategoryClicked(int id) {
        mCallback.onSubcategoryClicked(id);
    }

    public interface Callback {
        void onSubcategoryClicked(int id);
    }

    Context mContext;
    FirstCategory[] firstCategories;
    Callback mCallback;

    public FirstCategoryAdapter(Context mContext, FirstCategory[] firstCategories, Callback callback) {
        this.mContext = mContext;
        this.firstCategories = firstCategories;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_firstcategory, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.txtViewFirstCategory.setText(firstCategories[i].name);
        SecondCategoryAdapter secondCategoryAdapter = new SecondCategoryAdapter(mContext, firstCategories[i].secondCategories, this);
        viewHolder.recyclerViewSecondCategory.setAdapter(secondCategoryAdapter);
        viewHolder.recyclerViewSecondCategory.setLayoutManager(new LinearLayoutManager(mContext));
        viewHolder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewHolder.imgViewArrowUp.getVisibility() == View.GONE){
                    viewHolder.imgViewArrowUp.setVisibility(View.VISIBLE);
                    viewHolder.imgViewArrowDown.setVisibility(View.GONE);
                    viewHolder.recyclerViewSecondCategory.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.imgViewArrowUp.setVisibility(View.GONE);
                    viewHolder.imgViewArrowDown.setVisibility(View.VISIBLE);
                    viewHolder.recyclerViewSecondCategory.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return firstCategories.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_arrow_down)
        ImageView imgViewArrowDown;
        @BindView(R.id.img_view_arrow_up)
        ImageView imgViewArrowUp;
        @BindView(R.id.txt_view_first_category)
        TextView txtViewFirstCategory;
        @BindView(R.id.recycler_view_second_category)
        RecyclerView recyclerViewSecondCategory;
        @BindView(R.id.main_layout)
        View mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
