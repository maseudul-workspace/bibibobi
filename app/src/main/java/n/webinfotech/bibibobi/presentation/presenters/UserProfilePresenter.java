package n.webinfotech.bibibobi.presentation.presenters;

import n.webinfotech.bibibobi.domain.models.UserDetails;
import n.webinfotech.bibibobi.presentation.ui.adapters.CityListDialogAdapter;
import n.webinfotech.bibibobi.presentation.ui.adapters.StateListDialogAdapter;

public interface UserProfilePresenter {
    void fetchStateList();
    void fetchCityList();
    void fetchUserProfile();
    void updateUserProfile(String gender, String name, String pin, String db);
    interface View {
        void loadData(UserDetails userDetails);
        void showLoader();
        void hideLoader();
        void hideStateRecyclerView();
        void hideCityRecyclerView();
        void loadStateAdapter(StateListDialogAdapter stateListDialogAdapter);
        void loadCitiesAdapter(CityListDialogAdapter cityListDialogAdapter);
        void setStateName(String stateName);
        void setCityName(String cityName);
        void goToLoginActivity();
    }
}
