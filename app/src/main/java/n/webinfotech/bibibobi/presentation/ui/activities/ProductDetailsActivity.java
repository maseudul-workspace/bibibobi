package n.webinfotech.bibibobi.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import n.webinfotech.bibibobi.AndroidApplication;
import n.webinfotech.bibibobi.R;
import n.webinfotech.bibibobi.domain.executors.impl.ThreadExecutor;
import n.webinfotech.bibibobi.domain.models.PinData;
import n.webinfotech.bibibobi.domain.models.Product;
import n.webinfotech.bibibobi.domain.models.ProductImage;
import n.webinfotech.bibibobi.domain.models.User;
import n.webinfotech.bibibobi.presentation.presenters.ProductDetailsPresenter;
import n.webinfotech.bibibobi.presentation.presenters.impl.ProductDetailsPresenterImpl;
import n.webinfotech.bibibobi.presentation.ui.adapters.ColorsHorizontalAdapter;
import n.webinfotech.bibibobi.presentation.ui.adapters.ProductDetailsSizeAdapter;
import n.webinfotech.bibibobi.presentation.ui.adapters.ProductDetailsSliderAdapter;
import n.webinfotech.bibibobi.presentation.ui.adapters.ProductsHorizontalAdapter;
import n.webinfotech.bibibobi.threading.MainThreadImpl;
import n.webinfotech.bibibobi.util.GlideHelper;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ProductDetailsActivity extends AppCompatActivity implements ProductDetailsPresenter.View {

    @BindView(R.id.recycler_view_similar_items)
    RecyclerView recyclerViewSimilarItems;
    int quantity = 1;
    int totalQty = 5;
    ProductDetailsPresenterImpl mPresenter;
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.dots_indicator_layout)
    LinearLayout dotsLayout;
    @BindView(R.id.recycler_view_colors_filters)
    RecyclerView recyclerViewColorsFilters;
    @BindView(R.id.recycler_view_sizes_filter)
    RecyclerView recyclerViewSizesFilter;
    @BindView(R.id.txt_view_short_desc)
    TextView txtViewShortDesc;
    @BindView(R.id.txt_view_desc)
    TextView txtViewDesc;
    @BindView(R.id.txt_view_price)
    TextView txtViewPrice;
    @BindView(R.id.txt_view_product_title)
    TextView txtViewProductTitle;
    ProductDetailsSliderAdapter productDetailsSliderAdapter;
    ProgressDialog progressDialog;
    int productId;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txt_cart_count)
    TextView txtViewCartCount;
    @BindView(R.id.img_view_heart_fill)
    ImageView imgViewHeartFill;
    @BindView(R.id.img_view_heart_outline)
    ImageView imgViewHeartOutline;
    @BindView(R.id.main_layout)
    View mainLayout;
    @BindView(R.id.btn_add_cart)
    Button btnAddToCart;
    @BindView(R.id.txt_view_mrp)
    TextView txtViewMRP;
    @BindView(R.id.txt_availability_status)
    TextView txtViewAvailabilityStatus;
    @BindView(R.id.img_view_online_delivery_check)
    ImageView imgViewOnlineDeliveryCheck;
    @BindView(R.id.txt_view_online_delivery)
    TextView txtViewOnlineDelivery;
    @BindView(R.id.img_view_cod_check)
    ImageView imgViewCODcheck;
    @BindView(R.id.txt_view_cod)
    TextView txtViewCOD;
    @BindView(R.id.edit_text_pin)
    EditText editTextPin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        productId = getIntent().getIntExtra("productId", 0);
        initialisePresenter();
        setUpProgressDialog();
        mPresenter.getProductDetails(productId);
        showLoader();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void initialisePresenter() {
        mPresenter = new ProductDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadData(Product product, ProductImage[] productImages, ProductDetailsSizeAdapter productDetailsSizeAdapter, ColorsHorizontalAdapter colorsHorizontalAdapter, ProductsHorizontalAdapter productsHorizontalAdapter) {
        mainLayout.setVisibility(View.VISIBLE);
        recyclerViewColorsFilters.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        recyclerViewSizesFilter.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        recyclerViewColorsFilters.setAdapter(colorsHorizontalAdapter);
        recyclerViewSizesFilter.setAdapter(productDetailsSizeAdapter);
        recyclerViewSimilarItems.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        recyclerViewSimilarItems.setAdapter(productsHorizontalAdapter);
        productDetailsSliderAdapter = new ProductDetailsSliderAdapter(this, productImages);
        prepareDotsIndicator(0, productImages.length);
        viewPager.setAdapter(productDetailsSliderAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                prepareDotsIndicator(position, productImages.length);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        txtViewPrice.setText("Rs. " + product.minPrice);
        if (product.minPrice == null) {
            btnAddToCart.setEnabled(false);
        }
        txtViewShortDesc.setText(product.shortDescription);
        txtViewDesc.setText(product.longDescription);
        txtViewProductTitle.setText(product.name);
        if (product.isWishListPresent) {
            imgViewHeartFill.setVisibility(View.VISIBLE);
        } else {
            imgViewHeartOutline.setVisibility(View.VISIBLE);
        }
        if (product.mrp != null) {
            txtViewMRP.setText("Rs. " + product.mrp);
            txtViewMRP.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        }
        getSupportActionBar().setTitle(product.name);
    }

    public void prepareDotsIndicator(int sliderPosition, int length){
        if(dotsLayout.getChildCount() > 0){
            dotsLayout.removeAllViews();
        }

        ImageView dots[] = new ImageView[length];
        for(int i = 0; i < length; i++){
            dots[i] = new ImageView(this);
            if(i == sliderPosition){
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.active_dot);
            }else{
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.inactive_dot);
            }

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(15, 15);
            layoutParams.setMargins(7, 0, 7, 0);
            dotsLayout.addView(dots[i], layoutParams);

        }
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void goToProductDetails(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", productId);
        startActivity(intent);
    }

    @Override
    public void loadCartCount(int cartCount) {
        txtViewCartCount.setText(Integer.toString(cartCount));
    }

    @Override
    public void onAddToWishlistSuccess() {
        imgViewHeartFill.setVisibility(View.VISIBLE);
        imgViewHeartOutline.setVisibility(View.GONE);
    }

    @Override
    public void onRemoveFromWishlistSuccess() {
        imgViewHeartFill.setVisibility(View.GONE);
        imgViewHeartOutline.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPinAvailable(PinData pinData) {
        txtViewAvailabilityStatus.setText("Delivery Available");
        txtViewAvailabilityStatus.requestFocus();
        txtViewAvailabilityStatus.setTextColor(getResources().getColor(R.color.green2));
        if (pinData.cod.equals("Y")) {
           txtViewCOD.setVisibility(View.VISIBLE);
           imgViewCODcheck.setVisibility(View.VISIBLE);
        }
        if (pinData.prePaid.equals("Y")) {
            txtViewOnlineDelivery.setVisibility(View.VISIBLE);
            imgViewOnlineDeliveryCheck.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onPinUnavailable() {
        txtViewAvailabilityStatus.setText("Delivery Not Available");
        txtViewAvailabilityStatus.requestFocus();
        txtViewAvailabilityStatus.setTextColor(getResources().getColor(R.color.red2));
        txtViewCOD.setVisibility(View.GONE);
        imgViewCODcheck.setVisibility(View.GONE);
        txtViewOnlineDelivery.setVisibility(View.GONE);
        imgViewOnlineDeliveryCheck.setVisibility(View.GONE);
    }

    @Override
    public void goToLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.btn_add_cart) void onAddToCartClicked() {
        if (checkLogin()) {
            mPresenter.addToCart(productId);
        } else {
            goToLoginActivity();
        }
    }

    @OnClick(R.id.layout_cart) void onCartClicked() {
        if (checkLogin()) {
            AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
            User user = androidApplication.getUserInfo(this);
            if (user != null) {
                Intent cartIntent = new Intent(this, CartActivity.class);
                startActivity(cartIntent);
            }
        } else {
           goToLoginActivity();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchCartCount();
    }

    public boolean checkLogin() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        User user = androidApplication.getUserInfo(this);
        if (user == null) {
            return false;
        } else {
            return true;
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.img_view_heart_fill) void onHeartFilledClicked() {
        if (checkLogin()) {
            mPresenter.removeFromWishlist(productId);
        } else {
            Toast.makeText(this, "Please Login Again", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.img_view_heart_outline) void onHeartOutlineClicked() {
        if (checkLogin()) {
            mPresenter.addToWishList(productId);
        } else {
            Toast.makeText(this, "Please Login Again", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.btn_pin_check) void onCheckPinClicked() {
        if (editTextPin.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please Insert Pincode", Toast.LENGTH_SHORT).show();
        } else {
            mPresenter.checkPin(editTextPin.getText().toString());
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(editTextPin.getWindowToken(), 0);
            showLoader();
        }
    }

    @OnClick(R.id.img_view_share) void onShareClicked() {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, "Bibibobi");
        i.putExtra(Intent.EXTRA_TEXT, "http://bibibobi.com/");
        startActivity(Intent.createChooser(i, "Share URL"));
    }

}
