package n.webinfotech.bibibobi.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import n.webinfotech.bibibobi.R;
import n.webinfotech.bibibobi.domain.executors.impl.ThreadExecutor;
import n.webinfotech.bibibobi.presentation.presenters.SignUpPresenter;
import n.webinfotech.bibibobi.presentation.presenters.impl.SignUpPresenterImpl;
import n.webinfotech.bibibobi.threading.MainThreadImpl;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

public class SignUpActivity extends AppCompatActivity implements SignUpPresenter.View {

    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_phone)
    EditText editTextMobile;
    @BindView(R.id.edit_text_password)
    EditText editTextPassword;
    @BindView(R.id.edit_text_repeat_password)
    EditText editTextRpPassword;
    @BindView(R.id.txt_input_name_layout)
    TextInputLayout txtInputNameLayout;
    @BindView(R.id.txt_input_email_layout)
    TextInputLayout txtInputEmailLayout;
    @BindView(R.id.txt_input_phone_layout)
    TextInputLayout txtInputPhoneLayout;
    @BindView(R.id.txt_input_password_layout)
    TextInputLayout txtInputPasswordLayout;
    @BindView(R.id.txt_input_repeat_layout)
    TextInputLayout txtInputRepeatayout;
    ProgressDialog progressDialog;
    SignUpPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Sign Up");
        initialisePresenter();
        setUpProgressDialog();
    }

    public void initialisePresenter() {
        mPresenter = new SignUpPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void goToLoginActivity() {
        finish();
    }

    @Override
    public void onEmailError(String errorMsg) {
        txtInputEmailLayout.setError(errorMsg);
    }

    @Override
    public void onMobileError(String errorMsg) {
        txtInputPhoneLayout.setError(errorMsg);
    }

    @OnClick(R.id.btn_submit) void onSignUpClicked() {
        if (editTextName.getText().toString().trim().isEmpty() ||
                editTextEmail.getText().toString().isEmpty() ||
                editTextMobile.getText().toString().trim().isEmpty() ||
                editTextPassword.getText().toString().trim().isEmpty() ||
                editTextRpPassword.getText().toString().trim().isEmpty()
        ) {
            Toast.makeText(this, "Please fill all the fields", Toast.LENGTH_SHORT).show();
        } else if(editTextPassword.getText().toString().length() < 8) {
            Toast.makeText(this, "Password must be minumn 8 charcters in length", Toast.LENGTH_SHORT).show();
        } else if(editTextMobile.getText().toString().length() != 10) {
            Toast.makeText(this, "Password must be 10 characters", Toast.LENGTH_SHORT).show();
        } else if (!editTextPassword.getText().toString().equals(editTextRpPassword.getText().toString())) {
            Toast.makeText(this, "Password mismatch", Toast.LENGTH_SHORT).show();
        } else {
            txtInputPhoneLayout.setError("");
            txtInputEmailLayout.setError("");
            mPresenter.signUp(
                    editTextName.getText().toString(),
                    editTextEmail.getText().toString(),
                    editTextMobile.getText().toString(),
                    editTextPassword.getText().toString(),
                    editTextRpPassword.getText().toString()
            );
            showLoader();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
