package n.webinfotech.bibibobi.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import n.webinfotech.bibibobi.AndroidApplication;
import n.webinfotech.bibibobi.R;
import n.webinfotech.bibibobi.domain.models.User;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class UserActivity extends AppCompatActivity {

    @BindView(R.id.txt_view_user)
    TextView txtViewUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        ButterKnife.bind(this);
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        User user = androidApplication.getUserInfo(this);
        txtViewUser.setText(user.name);
        getSupportActionBar().setTitle("My Activity");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.layout_orders) void onOrdersClicked() {
        Intent intent = new Intent(this, OrderListActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.layout_my_account) void onMyAccountClicked() {
        Intent intent = new Intent(this, UserProfileActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.layout_address) void onShippingAddressClicked() {
        Intent intent = new Intent(this, ShippingAddressActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.layout_cart) void onCartClicked() {
        Intent intent = new Intent(this, CartActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.layout_wishlist) void onWishlistClicked() {
        Intent intent = new Intent(this, WishlistActivity.class);
        startActivity(intent);
    }

    void goToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_logout) void onLogoutClicked() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        androidApplication.setUserInfo(this, null);
        goToMainActivity();
    }

    @OnClick(R.id.btn_change_password) void onChangePasswordClicked() {
        Intent intent = new Intent(this, ChangePasswordActivity.class);
        startActivity(intent);
    }

}
