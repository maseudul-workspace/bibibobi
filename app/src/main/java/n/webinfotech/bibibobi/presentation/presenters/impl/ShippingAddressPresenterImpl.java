package n.webinfotech.bibibobi.presentation.presenters.impl;

import android.content.Context;

import n.webinfotech.bibibobi.AndroidApplication;
import n.webinfotech.bibibobi.domain.executors.Executor;
import n.webinfotech.bibibobi.domain.executors.MainThread;
import n.webinfotech.bibibobi.domain.interactors.FetchShippingAddressListInteractor;
import n.webinfotech.bibibobi.domain.interactors.impl.FetchShippingAddressListInteractorImpl;
import n.webinfotech.bibibobi.domain.models.Address;
import n.webinfotech.bibibobi.domain.models.User;
import n.webinfotech.bibibobi.presentation.presenters.ShippingAddressPresenter;
import n.webinfotech.bibibobi.presentation.presenters.base.AbstractPresenter;
import n.webinfotech.bibibobi.presentation.ui.adapters.ShippingAddressAdapter;
import n.webinfotech.bibibobi.repository.AppRepositoryImpl;


public class ShippingAddressPresenterImpl extends AbstractPresenter implements ShippingAddressPresenter, FetchShippingAddressListInteractor.Callback, ShippingAddressAdapter.Callback{

    Context mContext;
    ShippingAddressPresenter.View mView;
    ShippingAddressAdapter shippingAddressAdapter;
    AndroidApplication androidApplication;
    FetchShippingAddressListInteractorImpl fetchShippingAddressListInteractor;
//    DeleteShippingAddressInteractorImpl deleteShippingAddressInteractor;

    public ShippingAddressPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchShippingAddress() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User userInfo = androidApplication.getUserInfo(mContext);
        fetchShippingAddressListInteractor = new FetchShippingAddressListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.userId, userInfo.apiToken);
        fetchShippingAddressListInteractor.execute();
    }

    @Override
    public void onGettingShippingAddressSuccess(Address[] addresses) {
        shippingAddressAdapter = new ShippingAddressAdapter(mContext, addresses, this);
        mView.loadAdapter(shippingAddressAdapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingShippingAddressFail(String errorMsg, int isLoginError) {
        mView.hideLoader();
        mView.hideAddressRecyclerView();
        if (isLoginError == 1) {
            mView.goToLoginActivity();
        }
    }

    @Override
    public void onRemoveClicked(int id) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User userInfo = androidApplication.getUserInfo(mContext);
//        deleteShippingAddressInteractor = new DeleteShippingAddressInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, id);
//        deleteShippingAddressInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onEditClicked(int id) {
        mView.goToAddressEditActivity(id);
    }

//    @Override
//    public void onAddressDeleteSuccess() {
//        Toast.makeText(mContext, "Address deleted successfully", Toast.LENGTH_SHORT).show();
//        fetchShippingAddress();
//        mView.hideAddressRecyclerView();
//    }

//    @Override
//    public void onAddressDeleteFail(String errorMsg, int loginError) {
//        mView.hideLoader();
//        if (loginError == 1) {
//            mView.goToLoginActivity();
//        } else{
//            Toast.makeText(mContext, "Failed to delete", Toast.LENGTH_SHORT).show();
//        }
//    }
}
