package n.webinfotech.bibibobi.presentation.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.bibibobi.R;
import n.webinfotech.bibibobi.domain.models.Product;
import n.webinfotech.bibibobi.util.GlideHelper;

/**
 * Created by Raj on 23-09-2019.
 */

public class ProductsHorizontalAdapter extends RecyclerView.Adapter<ProductsHorizontalAdapter.ViewHolder> {

    public interface Callback {
        void onProductClicked(int productId);
    }

    Context mContext;
    Product[] products;
    Callback mCallback;

    public ProductsHorizontalAdapter(Context mContext, Product[] products, Callback mCallback) {
        this.mContext = mContext;
        this.products = products;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_products_horizontal, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        GlideHelper.setImageView(mContext, viewHolder.imgViewProduct, mContext.getResources().getString(R.string.base_url) + "/images/product/thumb/" + products[i].mainImage);
        viewHolder.txtViewProductTitle.setText(products[i].name);
        viewHolder.txtViewPrice.setText("Rs. " + products[i].minPrice);
        viewHolder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onProductClicked(products[i].id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return products.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_product)
        ImageView imgViewProduct;
        @BindView(R.id.txt_view_product_title)
        TextView txtViewProductTitle;
        @BindView(R.id.txt_view_price)
        TextView txtViewPrice;
        @BindView(R.id.main_layout)
        View mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
