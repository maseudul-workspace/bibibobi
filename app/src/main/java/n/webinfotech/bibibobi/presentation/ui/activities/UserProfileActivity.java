package n.webinfotech.bibibobi.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import n.webinfotech.bibibobi.R;
import n.webinfotech.bibibobi.domain.executors.impl.ThreadExecutor;
import n.webinfotech.bibibobi.domain.models.User;
import n.webinfotech.bibibobi.domain.models.UserDetails;
import n.webinfotech.bibibobi.domain.models.UserProfile;
import n.webinfotech.bibibobi.presentation.presenters.UserProfilePresenter;
import n.webinfotech.bibibobi.presentation.presenters.impl.UserProfilePresenterImpl;
import n.webinfotech.bibibobi.presentation.ui.adapters.CityListDialogAdapter;
import n.webinfotech.bibibobi.presentation.ui.adapters.StateListDialogAdapter;
import n.webinfotech.bibibobi.presentation.ui.dialogs.SelectCityDialog;
import n.webinfotech.bibibobi.presentation.ui.dialogs.SelectStateDialog;
import n.webinfotech.bibibobi.threading.MainThreadImpl;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

import java.util.Calendar;

public class UserProfileActivity extends AppCompatActivity implements UserProfilePresenter.View {

    SelectStateDialog selectStateDialog;
    @BindView(R.id.txt_view_state)
    TextView txtViewState;
    SelectCityDialog selectCityDialog;
    @BindView(R.id.txt_view_city_name)
    TextView txtViewCityName;
    @BindView(R.id.txt_input_email_layout)
    TextInputLayout txtInputEmailLayout;
    @BindView(R.id.txt_input_phone_layout)
    TextInputLayout txtInputPhoneLayout;
    @BindView(R.id.txt_input_pincode_layout)
    TextInputLayout txtInputPincodeLayout;
    @BindView(R.id.txt_input_name_layout)
    TextInputLayout txtInputNameLayout;
    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_pincode)
    EditText editTextPincode;
    @BindView(R.id.radio_group_gender)
    RadioGroup radioGroupGender;
    @BindView(R.id.radio_btn_male)
    RadioButton radioButtonMale;
    @BindView(R.id.radio_btn_female)
    RadioButton radioButtonFemale;
    ProgressDialog progressDialog;
    UserProfilePresenterImpl mPresenter;
    String gender = "M";
    @BindView(R.id.txt_view_dob)
    TextView txtViewDOB;
    String dob;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        ButterKnife.bind(this);
        setRadioGroupGender();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("My Profile");
        setProgressDialog();
        initialisePresenter();
        setUpStateSelectDialogView();
        setSelectCityDialog();
        mPresenter.fetchStateList();
        mPresenter.fetchUserProfile();
        showLoader();
    }

    public void setRadioGroupGender() {
        radioGroupGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb=(RadioButton)findViewById(checkedId);
                if (rb.getText().toString().equals("Male")) {
                    gender = "M";
                } else {
                    gender = "F";
                }
            }
        });
    }

    public void initialisePresenter() {
        mPresenter = new UserProfilePresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    public void setUpStateSelectDialogView() {
        selectStateDialog = new SelectStateDialog(this, this);
        selectStateDialog.setUpDialogView();
    }

    public void setSelectCityDialog() {
        selectCityDialog = new SelectCityDialog(this, this);
        selectCityDialog.setUpDialogView();
    }

    @Override
    public void loadData(UserDetails userDetails) {
        User user = userDetails.user;
        UserProfile userProfile = userDetails.userProfile;
        if (user.name != null) {
            editTextName.setText(user.name);
        }
        if (user.email != null) {
            editTextEmail.setText(user.email);
        }
        if (user.mobile != null) {
            editTextPhone.setText(user.mobile);
        }
        if (userProfile.stateName != null) {
            txtViewState.setText(userProfile.stateName);
            txtViewState.setTextColor(this.getResources().getColor(R.color.black));
        }
        if (userProfile.cityName != null) {
            txtViewCityName.setText(userProfile.cityName);
            txtViewCityName.setTextColor(this.getResources().getColor(R.color.black));
        }
        if (userProfile.pin != null) {
            editTextPincode.setText(userProfile.pin);
        }

        if (userProfile.gender != null) {
            if (userProfile.gender.equals("M")) {
                gender = "M";
                radioButtonMale.setChecked(true);
                radioButtonFemale.setChecked(false);
            } else {
                gender = "F";
                radioButtonFemale.setChecked(true);
                radioButtonMale.setChecked(false);
            }
        }

        if (userProfile.dob != null) {
            dob = userProfile.dob;
            txtViewDOB.setText("DOB: " + userProfile.dob);
            txtViewDOB.setTextColor(getResources().getColor(R.color.black));
        }

    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void hideStateRecyclerView() {
        selectCityDialog.hideRecyclerView();
    }

    @Override
    public void hideCityRecyclerView() {
        selectCityDialog.hideRecyclerView();
    }

    @Override
    public void loadStateAdapter(StateListDialogAdapter stateListDialogAdapter) {
        selectStateDialog.setRecyclerView(stateListDialogAdapter);
    }

    @Override
    public void loadCitiesAdapter(CityListDialogAdapter cityListDialogAdapter) {
        selectCityDialog.setRecyclerView(cityListDialogAdapter);
    }

    @Override
    public void setStateName(String stateName) {
        txtViewState.setText(stateName);
        txtViewState.setTextColor(this.getResources().getColor(R.color.black));
        selectStateDialog.hideDialog();
    }

    @Override
    public void setCityName(String cityName) {
        txtViewCityName.setText(cityName);
        txtViewCityName.setTextColor(this.getResources().getColor(R.color.black));
        selectCityDialog.hideDialog();
    }

    @Override
    public void goToLoginActivity() {
        Toast.makeText(this, "Your session expired !! Please login again", Toast.LENGTH_LONG).show();
        Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(loginIntent);
    }

    @OnClick(R.id.state_linear_layout) void onStateClicked() {
        selectStateDialog.showDialog();
    }

    @OnClick(R.id.city_linear_layout) void onCityClicked() {
        selectCityDialog.showDialog();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.btn_update) void onUpdateClicked() {
        if (editTextPincode.getText().toString().trim().isEmpty()) {
            txtInputPincodeLayout.setError("Pincode Required");
            editTextPincode.requestFocus();
        } else if (editTextName.getText().toString().trim().isEmpty()) {
            txtInputNameLayout.setError("Name Required");
            editTextName.requestFocus();
        } else if (dob.isEmpty()) {
            Toast.makeText(this, "DOB is required", Toast.LENGTH_SHORT).show();
        } else {
            mPresenter.updateUserProfile(gender, editTextName.getText().toString(), editTextPincode.getText().toString(), dob);
        }
    }

    @OnClick(R.id.dob_linear_layout) void onDOBLayoutClicked() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        dob = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        txtViewDOB.setText("DOB: " + dob);
                        txtViewDOB.setTextColor(getResources().getColor(R.color.black));

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
        datePickerDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(Color.BLACK);
        datePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);
    }


}
