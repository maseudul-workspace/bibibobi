package n.webinfotech.bibibobi.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.bibibobi.R;
import n.webinfotech.bibibobi.domain.models.SecondCategory;
import n.webinfotech.bibibobi.util.GlideHelper;

public class SecondCategoryAdapter extends RecyclerView.Adapter<SecondCategoryAdapter.ViewHolder> {

    public interface Callback {
        void onSecondCategoryClicked(int id);
    }

    Context mContext;
    SecondCategory[] secondCategories;
    Callback mCallback;

    public SecondCategoryAdapter(Context mContext, SecondCategory[] secondCategories, Callback mCallback) {
        this.mContext = mContext;
        this.secondCategories = secondCategories;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_second_category, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewSecondCategory.setText(secondCategories[position].name);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onSecondCategoryClicked(secondCategories[position].id);
            }
        });
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewSecondCategory, mContext.getResources().getString(R.string.base_url) + "/images/category/second_category/thumb/" + secondCategories[position].image, 50);
    }

    @Override
    public int getItemCount() {
        return secondCategories.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_second_category)
        ImageView imgViewSecondCategory;
        @BindView(R.id.txt_view_second_category)
        TextView txtViewSecondCategory;
        @BindView(R.id.main_layout)
        View mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
